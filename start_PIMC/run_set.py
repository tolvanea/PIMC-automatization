import os
#from distutils.dir_util import copy_tree
import shutil
import shlex
import time
from sys import argv, path
import json
from typing import List

from start_PIMC.misc import check_that_this_is_run_in_correct_directory, add_default_values


help_text = """
This program generates multiple calculation directories and runs pimc in them.

Usage:
    Args:
    --help                  prints this text
    --start                 generates a new set and starts running (this is what 
                            you need first time running new set)
    --continue              continues existing set (does not modify any simulation files such as 
                            CONF or SBATCH.sh )
    --overwrite f1 f2 ...   continues existing set, but overwrites existing simulation files. This 
                            is almost same as --start, but it is designed for continuing existing  
                            set, and it explicitly forces to confirm every file overwrite. Otherwise  
                            that file is not touched. Here files (or directories) 'f1', 'f2' are for
                            example 'CONF', 'SBATCH.sh', 'PP'. 
    --no-run                similar to '--start' but does not run pimc3 (used for example in testing
                            and inspecting generated file structures)

Usage:
    0. Rename 'configure_instance_SAMPLE.py' to 'configure_instance.py'
    
    1. modify 'configure_instance.py' to variate some value in simulation

    2. make sure directory tree looks like following:
        my_system_XXX__variate_YYY_and_ZZZ
            sample
                configure_instance.py     <-- modify this file
                PARTICLES
                CONF
                SBATCH.sh

    3. Run in parent directory that contains subdirectory 'sample' 
       $ python3 -m start_PIMC.run_set --start
    
    4. If the calculation has been finished, and you want some more precision, you can continue with
        $ python3 -m start_PIMC.run_set --continue
        
    5. (This is example if you want to tune the calculation and continue the calculation.) 
       If you want to continue running it on csc-cluster, and you want to tune parameter `num_proc`
       in configure_instance.py (which will affect SBATCH.sh), and you want to tune block count 
       parameter on sample/CONF, then run (on csc):
       $ python3 -m start_PIMC.run_set --overwrite SBATCH.sh CONF

"""

from enum import Enum
class modes(Enum):
    init_and_run = 1
    continue_running = 2
    no_run = 3
    overwrite_and_run = 4


def start(mode=modes.init_and_run, overwrite=None):
    check_that_this_is_run_in_correct_directory(overwrite)
    path.append(os.getcwd())

    from sample import configure_instance # You must be in same folder with this py-file
    init_setup = configure_instance.InitialSetup()
    # If some options are not explicitly written, then add default values for those
    add_default_values(init_setup, [])

    if mode == modes.init_and_run:
        store_setup(init_setup)
        generate_instances_and_run(init_setup)

    elif mode == modes.continue_running:
        continue_simulation(init_setup)

    elif mode == modes.overwrite_and_run:
        store_setup(init_setup)
        init_setup.overwrite_list = overwrite
        generate_instances_and_run(init_setup, overwrite=overwrite)

    elif mode == modes.no_run:
        init_setup.no_run = True
        store_setup(init_setup)
        generate_instances_and_run(init_setup)

    else:
        raise ValueError("Keyword does not match.")


def store_setup(init_setup, path="results/metadata/initial_configuration.json"):
    """
    Stores initial setup to ../results/metadata
    :param init_setup:
    :return:
    """


    if not os.path.isdir("results"):
        os.makedirs("results")

    if not os.path.isdir("results/metadata"):
        os.makedirs("results/metadata")

    with open(path, "w") as file:
        file.write("# This is data of `initial_configuration.py` at time of last run. This file\n")
        file.write("# is not used anywhere in code, and its purpose is just to log parameters.\n'")
        class_dict = init_setup.__dict__
        json.dump(class_dict, file)


def generate_instances_and_run(init_setup, overwrite: List[str]=None):
    """
    Iterates every directory in two levels (either creates new dirs or uses
    existing ones). Copies all items in sample-dir to simulation dirs. Modifies
    simulation dir with function in configure instance. Runs simulation.

    Directory tree looks like following, when variating values X and Y:

    var_X1
        var_Y1
        var_Y2
        var_Y3
        ...
    var_X2
        var_Y1
        var_Y2
        var_Y3
        ...
    ...

    :param init_setup:  A class in 'configure_instance.py'
    :param overwrite:   Overwrite these existing files, None if not in overwrite
                        mode.
    :return:
    """
    range1 = init_setup.range1
    range2 = init_setup.range2
    dir1_labels = init_setup.dir1_labels
    dir2_labels = init_setup.dir2_labels
    if overwrite is not None:
        init_setup.overwrite_list = overwrite

    files_to_not_copy = ["configure_instance.py", "__pycache__", ".directory"]

    for i, num1 in enumerate(range1):
        dir1 = dir1_labels[i]
        if not os.path.exists(dir1):
            if overwrite is not None:
                print("Did not find directory {}, skipping it".format(dir1))
                continue
            os.makedirs(dir1)
        else:
            if overwrite is None:
                raise Exception("Directory " + str(dir1) +
                                " already exists! Use --overwrite or --continue\nOr if last run ended in crash, delete the directories '" + str(dir1) + "' and 'results'.")
        if os.path.exists(dir1 + "/sample_copy_REMOVE_THIS"):
            shutil.rmtree(dir1 + "/sample_copy_REMOVE_THIS")
        os.makedirs(dir1 + "/sample_copy_REMOVE_THIS")

        # Copy files to temporary storage (sample-dir under first dir-layer var_X)
        for file_or_dir in os.listdir("sample"):
            if (file_or_dir not in files_to_not_copy and file_or_dir[0] != "."):
                if os.path.isfile("sample/" + file_or_dir):
                    shutil.copyfile("sample/" + file_or_dir,
                                    dir1 + "/sample_copy_REMOVE_THIS/" + file_or_dir)
                else:
                    shutil.copytree("sample/" + file_or_dir,
                                    dir1 + "/sample_copy_REMOVE_THIS/" + file_or_dir)
        
        # change directory
        os.chdir(dir1)

        for j, num2 in enumerate(range2):
            dir2 = dir2_labels[j]
            if not os.path.exists(dir2):
                if overwrite is not None:
                    print("Did not find directory {}/{}, skipping it".format(dir1, dir2))
                    continue
                os.makedirs(dir2)
            else:
                if overwrite is None:
                    raise Exception("Directory " + str(
                        dir2) + " already exists! Use --overwrite or --continue")

            for file_or_dir in os.listdir("sample_copy_REMOVE_THIS"):
                if (file_or_dir not in files_to_not_copy and file_or_dir[0] != "."):
                    # Copy file
                    if os.path.isfile("sample_copy_REMOVE_THIS/" + file_or_dir):
                        # Overwrite existing files
                        if ((overwrite is not None) and
                                os.path.isfile(dir2 + "/" + file_or_dir) and
                                (file_or_dir in overwrite)):
                            os.remove(dir2 + "/" + file_or_dir)
                            shutil.copyfile("sample_copy_REMOVE_THIS/" + file_or_dir,
                                            dir2 + "/" + file_or_dir)
                        # Copy files for with start
                        elif(overwrite is None):
                            shutil.copyfile("sample_copy_REMOVE_THIS/" + file_or_dir,
                                            dir2 + "/" + file_or_dir)
                    # Copy directory
                    elif os.path.isdir("sample_copy_REMOVE_THIS/" + file_or_dir):
                        # Overwrite existing directories
                        if ((overwrite is not None) and
                                os.path.isdir(dir2 + "/" + file_or_dir) and
                                (file_or_dir in overwrite)):
                            shutil.rmtree(dir2 + "/" + file_or_dir)
                            shutil.copytree("sample_copy_REMOVE_THIS/" + file_or_dir,
                                            dir2 + "/" + file_or_dir)
                        # Copy new directories
                        elif(overwrite is None):
                            shutil.copytree("sample_copy_REMOVE_THIS/" + file_or_dir,
                                            dir2 + "/" + file_or_dir)

        
            os.chdir(dir2)

            # Do the thing (i.e. variate value by function and run pimc3)
            continuerun = (overwrite is not None)
            init_setup.set_up_simulation(i, j, continuerun)

            os.chdir("..")

        shutil.rmtree('sample_copy_REMOVE_THIS')

        os.chdir("..")
        time.sleep(2)


def continue_simulation(init_setup):
    range1 = init_setup.range1
    range2 = init_setup.range2
    dir1_labels = init_setup.dir1_labels
    dir2_labels = init_setup.dir2_labels
    init_setup.overwrite_list = []

    for i, num1 in enumerate(range1):
        dir1 = dir1_labels[i]
        if not os.path.exists(dir1):
            print("Did not find directory {}, skipping it".format(dir1))
            continue
        os.chdir(dir1)
        for j, num2 in enumerate(range2):
            dir2 = dir2_labels[j]
            if not os.path.exists(dir2):
                print("Did not find directory {}/{}, skipping it".format(dir1, dir2))
                continue
            os.chdir(dir2)

            # Do the thing (i.e. run pimc3)
            init_setup.set_up_simulation(i, j, continuerun=True)

            os.chdir("..")
        os.chdir("..")
        time.sleep(2)


"""
def run_command(command):
    # WARNING method not tested yet! Also not used anywhere
    #https://www.endpoint.com/blog/2015/01/28/getting-realtime-output-using-python
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        #if output:
        #print(output.strip())
        hs = open("stdout.txt","a")
        hs.write(output)
        hs.close()
    rc = process.poll()
    return rc
"""
        
if __name__ == "__main__":
    
    if (len(argv) < 2):
        print(help_text)
    elif (argv[1][0:2] != "--"):
        raise Exception("Use two dashes '--' as argument prefix.")
    elif (argv[1] == "--help"):
        print(help_text)
    elif (argv[1] == "--start"):
        start(mode=modes.init_and_run)
    elif (argv[1] == "--continue"):
        start(mode=modes.continue_running)
    elif (argv[1] == "--no-run" or argv[1] == "--norun"):
        start(mode=modes.no_run)
    elif (argv[1] == "--overwrite"):
        if (len(argv) < 3):
            raise Exception("Give file(s) to overwrite, e.g --overwrite SBATCH.sh CONF")

        start(mode=modes.overwrite_and_run, overwrite=argv[2:])
    else:
        valid = ["--help", "--start", "--continue", "--no-run", "--overwrite"]
        raise Exception("Unknown keyword '{}'. Valid are : \n{}"
                        .format(argv[1], "\n".join(valid)))


