This directory contains source files that starts multiple instances of pimc3. It is recommended to use latest git-commit from branch `susceptibility_with_cross_product`, that is at the moment of writing `c9053ee`.

See better documentation from comments of python source files. That is, to know in more detail how to start simulation, check the comments in the beginning file `start_PIMC.run_set.py`.

Usage:
1. Copy `CONF`, `PARTICLES` and `SBATCH.sh` under directory `sample`. The file `SBATCH.sh` is not needed if the script is not run on the CSC-cluster computer.
2. Copy `configure_instance_SAMPLE.py` in there and
rename it to `configure_instance.py`. This file defines how an array of simulations is generated. So this file dublicates files `CONF` `PARTICLES` and then and modifies them to set parameters for each simulation separately
4. Modify `configure_instance.py` to variate preferred parameter of simulation. Also add there oter required information, such as path to `pimc3` executable, if it is not in `$PATH`. By deafult, the this file modifies time-step and temperature, and if that is what you want, only the beginning of the file is required to be modified.
5. Make sure directory tree looks like following:
    * `my_system_XXX__variate_YYY_and_ZZZ`
        * `sample`
            * `configure_instance.py`
            * `PARTICLES`
            * `CONF`
            * `SBATCH.sh`
6. Run following command in parent directory that contains subdirectory `sample` :
   * `$ python3 -m start_PIMC.run_set --start`
7. If the calculation has been finished, and you want some more precision, you can continue the simulation with following command
    * On home PC (or on CSC if you do not care about old output files):
        * `$ python3 -m start_PIMC.run_set --continue`
    * On CSC so that new output is saved with a new name:
        * `$ python3 -m start_PIMC.run_set --overwrite SBATCH.sh`
8. If you want continue calculation while changing some parameters for calculations, continue with comand `--overwrite` and specify the files you want to overwrite.
    * Let's say you changed parameter `num_proc` (processor count on CSC) in configure_instance.py, and you want to continue calcualtions on CSC so that this change will affect `SBATCH.sh`. Run (on CSC)
        * `$ python3 -m start_PIMC.run_set --overwrite SBATCH.sh`
    * Let's say that you changed `NumMoves` parameter in `sample/CONF`, and you want to continue calculation so that this change will affect `CONF` files of all the calculations. (BTW, this is not recommended as sample data would be not uniform.) Also you want to save CSC outputfile with new name. You can run
        * `$ python3 -m start_PIMC.run_set --overwrite CONF SBATCH.sh`
    * **FOR MULTICORE USAGE READ THIS** Parallel multicore usage is supported on MPI environments. Multiple cores translates to multiple parallel markov chain walkers. It is recommended to first run few hour simulation with a single core and then increase the core count.
        * There are two reasons for this:
            1. About the first hour of a fresh simulation is used to pre-calculate data for particle interactions. This is saved to directory `pp`. After this is done, can the simulation begin. As far as I know, if you start a fresh simulation with multiple cores, this job is done by each thread redundantly.
            2. By letting the walker to stabilize to thermal equilibrium ("burn in"), you save in computation by calculating that only once. Then in equilibrium you can fork the walkers to multiple cores.
        * The multicore trick can be done as follows:
            1. Set `num_proc` to 1, and `timelimit` to something like 6 hours in configure_instance.py.
            2. Start a fresh simulation with
                * `$ python3 -m start_PIMC.run_set --start`
            3. Wait til the simulation ends
            4. Modify `num_proc` to something like 20, and `timelimit` to something like 72 hours.
            5. Because `timelimit` affects both pimc3 and batch-job parameters, continue the calculation with
                * `$ python3 -m start_PIMC.run_set --overwrite CONF SBATCH.sh`
