import os


def check_that_this_is_run_in_correct_directory(overwrite=None):
    """
    Checks that `start_PIMC.run_set` `plot_data.main` are ran in correct directory, which
    is of course the parent directory that contains subdirectory `sample`.
    """
    par_dir_name = os.path.basename(os.getcwd())

    help_text = """
    Your directory tree should look something like this:
    my_system_XXX__variate_YYY_and_ZZZ      <-- run it here
        sample
            configure_instance.py
            PARTICLES
            CONF
            SBATCH.sh
    """

    if not os.path.isdir("sample"):
        raise ValueError("Run this in parent directory that contains subdirectory 'sample'.\n"
                        + help_text)
    list_dir = os.listdir("sample")

    if not (("CONF" in list_dir) and ("configure_instance.py" in list_dir)
            and ("PARTICLES" in list_dir)):
        #print("os.getcwd()", os.getcwd())
        raise ValueError("Your 'sample' directory seems to be incomplete. It must contain 'CONF' \n" +
                        "and 'configure_instance.py' and 'PARTICLES'.\n" + help_text)

    if overwrite is not None:
        for file in overwrite:
            if (file not in list_dir):
                raise ValueError("File named '{}' does not exist in sample directory.".format(file))


def add_default_values(init_setup, obs_flags):
    # Here are default values for setting that can be enabled from
    # 'configure_instance.py'.

    # You can enable these setting by adding attributes to class 'InitialSetup'.
    # For example, add line 'self.extrapolate_range1 = True' to constructor.
    if not hasattr(init_setup, "extrapolate_range1"):
        # Extrapolate range1 at 0. Most likely it is time step, which
        # means that tau=0 extrapolation is added to figures and tables
        # Very useful, higly recommend to extrapolate tau=0.
        init_setup.extrapolate_range1 = False
    if not hasattr(init_setup, "plot_only_extrapolation"):
        # Do not plot finite-tau curves at all
        init_setup.plot_only_extrapolation = False
    if not hasattr(init_setup, "logarithmic_y_axis"):
        # Figures have log-y axis. Also negative values should be handled well
        init_setup.logarithmic_y_axis = False
    if not hasattr(init_setup, "logarithmic_x_axis"):
        # Figures have log-x axis. (That is range2 is spaced logarithmically)

        # Backwards compatibility to old naming
        if hasattr(init_setup, 'logarithmic_range2'):
            init_setup.logarithmic_x_axis = init_setup.logarithmic_range2
        else:
            init_setup.logarithmic_x_axis = False
    if not hasattr(init_setup, "reference_values"):
        # You can set reference values to be plotted on figures.
        # The key in dictionary is y-label of the figure, and value is list
        # of tuples ("label", x-value, y-value). For example:
        #reference_values = {
             #"$E$ ($E_h$)" : [
                #("ref. H$_2$ AQ $0\\;$K", 0, -1.16403),
                #("ref. H$_2$ BO $0\\;$K", 0, -1.23456),
            #],
        #}
        init_setup.reference_values = {}

    if not hasattr(init_setup, "sbatch"):
        # Only for calculations on CSC cluster computer. Enable if you are
        # running on CSC.
        init_setup.sbatch = False

    if not hasattr(init_setup, "use_cache"):
        # Read data from cache if it is not changed
        init_setup.use_cache = True
    if "No cached values" in obs_flags:
        init_setup.use_cache = False

    # The next three attributes are for uniting calculations with using
    # flag "--calc_comb". This means to have a third variating value.
    # Note: 'latex_symbol3' and 'latex_unit3' must match between the calculations.
    if not hasattr(init_setup, "latex_symbol3"):
        init_setup.latex_symbol3 = None
    if not hasattr(init_setup, "range3_val"):
        # This has similar function as range1 and range2, but now you give
        # only one value for it and not a list. The sibling calculations
        # have different values for this attribute.
        init_setup.range3_val = None
    if not hasattr(init_setup, "latex_unit3"):
        init_setup.latex_unit3 = None

    if not hasattr(init_setup, "linestyles"):
        # You can set different linestyles for different curves from range1
        init_setup.linestyles = None
    if not hasattr(init_setup, "linecolors"):
        # Linecolors for curves corresponding range1
        init_setup.linecolors = None
    if not hasattr(init_setup, "ref_marker_colors"):
        # Different colors for reference value marker points
        init_setup.ref_marker_colors = None

    if not hasattr(init_setup, "r2_susceptibilities"):
        # Include susceptibilities that are not calulated with our typical
        # <A^2> method, but with Rebance's equation with distance data <r^2>
        init_setup.r2_susceptibilities = False
    if not hasattr(init_setup, "unite_A2_figure_with_r2"):
        # By default above <r^2> susceptibilities will be placed in separate
        # figures from <A^2> susceptibilities, unless you enable this
        init_setup.unite_A2_figure_with_r2 = False
    elif init_setup.unite_A2_figure_with_r2:
        init_setup.r2_susceptibilities = True
    if not hasattr(init_setup, "partial_susceptibilites"):
        # Include figures from "susceptibilities of individual atoms". That is
        # calculate <A^2> separately for each atom. Also calculates <A^2>
        # separately for positive and negative particles.
        init_setup.partial_susceptibilites = False


    # -------------------------------------------------------------------------
    # Here are other declarations, that are not needed to modify in any way in
    # configure instance. So ignore the rest, they are internals
    # -------------------------------------------------------------------------

    # Overwrite_list list is used, when you want to continue simulation, but
    # update some files. For example if you decide to change temperature for
    # all calculations, call with "--overwrite CONF" flag. If you just want to
    # continue without changing any parameters, run with "--continue".
    if not hasattr(init_setup, "overwrite_list"):
        init_setup.overwrite_list = "new_run"

    # This will be overwritten to True, if flag --no-run is passed.
    if not hasattr(init_setup, "no_run"):
        init_setup.no_run = False

    # String formatting (=constant width) of variating values. Without and
    # with base. (e.g. If range1=[1.00, ...] str_range1 = ["1.00",...] and
    # dir1_labels = ["Tau_1.00",...])
    if not (hasattr(init_setup, "str_range1")
            and hasattr(init_setup, "str_range2")
            and hasattr(init_setup, "dir1_labels")
            and hasattr(init_setup, "dir2_labels")):
        a, b, c, d = generate_directory_names(init_setup)
        if not hasattr(init_setup, "str_range1"):
            init_setup.str_range1 = a
        if not hasattr(init_setup, "str_range2"):
            init_setup.str_range2 = b
        if not hasattr(init_setup, "dir1_labels"):
            init_setup.dir1_labels = c
        if not hasattr(init_setup, "dir2_labels"):
            init_setup.dir2_labels = d
    if not hasattr(init_setup, "range1_ext"):
        init_setup.range1_ext = [0.0] + init_setup.range1
    if not hasattr(init_setup, "str_range1_ext"):
        init_setup.str_range1_ext = [0.0] + init_setup.str_range1

    # Hacky extension combining multiple calculations, contains figures and axes
    init_setup.fig_ax = None
    init_setup.range3_idx = None

    if not hasattr(init_setup, "plot_only_range1_idx"):
        # Applies only for converge-plot: Plot only one curve for given
        # range1 index. May be useful is plot is hard to read
        init_setup.plot_only_range1_idx = None



"""
This module is for formatting names for directory tree, which is two levels 
deep. I.e generate such labels as "T1000" or "tau0.01".
"""

def generate_directory_names(init_setup):
    """
    These will be directory names to calculations. New shorter implementation.
    Direcrtory names are like var_X0.0200
    """

    dir1_base_name = init_setup.latex_symbol1.replace("\\","")
    dir2_base_name = init_setup.latex_symbol2.replace("\\","")
    range1 = init_setup.range1
    range2 = init_setup.range2

    str_range = [[], []]
    dir_labels = [[], []]

    max_char_limit = 7
    # number formatting in string with constant length
    for i, (rang, base) in enumerate([(range1, dir1_base_name),
                                      (range2, dir2_base_name)]):
        for j, num in enumerate(rang):
            num_as_str = str(num)
            if len(num_as_str) > max_char_limit:
                num_as_str = num_as_str[:max_char_limit]

            str_range[i].append(num_as_str)
            dir_labels[i].append(base + num_as_str)

    return str_range[0], str_range[1], dir_labels[0], dir_labels[1]
