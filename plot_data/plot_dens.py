#!/usr/bin/python3

#import classical_energies_2
import sys
import os               #path.exists, path.isfile, getcwd

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm # colormap
from matplotlib.colors import LinearSegmentedColormap
#import time
import math
from typing import Tuple, List
import random

import result_analysis.boltz_E.plotting as boltz_E_plotting
import result_analysis.boltz_E.read_paths

help_text = """
This script plots 2d density projection of QCA calculation. It also plots the locations
of quantum dots on top of density map.

usage:
    # run in calculation folder
    python3 -m plot_dens [param]

param
    --plot              shows the figure
    --print             prints it
    --plot_and_print    both    (default)

    --path path/to/calc-folder  (default: pwd)

"""


"""
scriptfile_location = os.path.dirname(os.path.realpath(__file__))
if (os.path.realpath(scriptfile_location + "/../boltz_E")) not in sys.path:
    sys.path.append(os.path.realpath(scriptfile_location + "/../boltz_E"))
"""


if (sys.version_info.major < 3) or (sys.version_info.minor < 6):
    raise Exception("You must be running python 3.6 or newer for typing-feature")




def main_function_if_executed_directly(argv):

    path, figure_param = parse_parametes(argv)

    cells, fixed_particles = read_paths.new_read_paths(path)
    cells_no_el = remove_electrons(cells)
    
    # in boltz_E folder
    min_x, max_x, min_y, max_y = boltz_E_plotting.get_lims(cells,fixed_particles)

    Z, den_grid = load_dens_plane(path);

    plot_density(Z, den_grid)

    boltz_E_plotting.plot_permutation(cells_no_el, fixed_particles, linewidth=1)
    plt.axis((min_x-1, max_x+1, min_y-2, max_y+2))
    plt.xlabel("x (nm)")
    plt.ylabel("y (nm)")


    if ("print" in figure_param):
        current_folder = os.getcwd().split(os.sep)[-1]
        plt.savefig("dens_"+current_folder+'.png')
    if ("plot" in figure_param):
        plt.show()


def parse_parametes(argv):

    if (("--path" in argv) and (argv.index("--path")+1 < len(argv))):
        path = argv[argv.index("--path")+1]
    else:
        path = os.getcwd()

    figure_param = "not specified"
    if ("--plot" in argv):
        figure_param = "--plot"

    elif ("--print" in argv):
        figure_param = "--print"

    elif ("--plot_and_print" in argv):
        figure_param = "--plot_and_print"

    else:
        figure_param = "--plot_and_print"

    if ("--help" in argv):
        print(help_text)
        exit(0)

    return path, figure_param



def load_dens_plane(path):

    density_1 = np.loadtxt(path+'/obs001/density.1.grid');
    density_grid = np.loadtxt(path+'/obs001/density.grid.grid');
    density_grid = density_grid + (density_grid[1]-density_grid[0]) # it's off by one grid spacing

    l_dg = len(density_grid);

    D_rs = np.reshape(density_1, (l_dg, l_dg, l_dg));
    D_rs = np.rot90(D_rs, k=1, axes=(0,2))
    D_rs = np.rot90(D_rs, k=3, axes=(0,1))
    #D_rs = np.flip(D_rs, axis=3) # not sure about z-direction


    #XX,YY = np.meshgrid(density_grid, density_grid);  # not needed!
    nm = math.ceil(1/(density_grid[1]-density_grid[0])) # one_nanometer_in_idx

    Z = np.sum(D_rs[:, :, math.floor(l_dg/2-nm/3) : math.floor(l_dg/2+nm/3)], axis=2);

    return Z, density_grid


def plot_density(Z, den_grid):

    #contour_levels = np.linspace(np.min(Z.flatten()), np.max(Z.flatten()),10)

    fig1 = plt.figure(1)
    """
    ax3d = fig1.add_subplot(111, projection='3d')
    surf = ax3d.plot_surface(XX,YY,Z, cmap=cm.coolwarm, shade=True,
                             linewidth=0.1, edgecolor=(0,0,0,1), alpha=0.9)
    col_bar = fig1.colorbar(surf, shrink=0.8, aspect=10)
    #col_bar.set_label('   E (eV)', rotation=0)

    #ax3d.contour(XX, YY, Z, contour_levels, cmap=cm.coolwarm, linestyles="solid", offset=-1, 
lw=0.6)

    ax3d.set_xlabel('x')
    ax3d.set_ylabel('y')
    ax3d.set_zlabel('ρ')
    #ax3d.set_zlim((-1,2.5))
    #plt.title("")
    """

    #heatmap, xedges, yedges = np.histogram2d(x, y, bins=50)
    #extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    #lims = [XX[0,0], XX[-1,-1], YY[0,0], YY[-1,-1]]
    lims = [den_grid[0], den_grid[-1], den_grid[0], den_grid[-1]]

    plt.clf()
    plt.imshow(Z, extent=lims, origin='lower', cmap=cm.inferno)

    #plt.show()


def colormap_black_to_blue():
    # My own colormap from black to blue to half-white
    # derivate of color is normalized to 2. meaning (d(r+g+b) / dx) = 2
    
    """
    https://matplotlib.org/examples/pylab_examples/custom_cmap.html
    row i:   x  y0  y1
                   /
                  /
    row i+1: x  y0  y1

    Above is an attempt to show that for x in the range x[i] to x[i+1], the
    interpolation is between y1[i] and y0[i+1].  So, y0[0] and y1[-1] are
    never used.
    """
    cdict = {  'red':      ((0.0, 0.0, 0.0),
                            (0.2, 0.1, 0.1),
                            (0.6, 0.15, 0.15),
                            (1.0, 0.55, 0.55)),

                'green':   ((0.0, 0.0, 0.0),
                            (0.2, 0.1, 0.1),
                            (0.6, 0.15, 0.15),
                            (1.0, 0.55, 0.55)),

                'blue':    ((0.0, 0.0, 0.0),
                            (0.2, 0.3, 0.3),
                            (0.6, 1.0, 1.0),
                            (1.0, 1.0, 1.0))
                }
    
    black_blue = LinearSegmentedColormap('BlackBlue', cdict)
    #interpolation='nearest', cmap=blue_red1
    return black_blue
        
    
def plot_density_remote(ax, Z, den_grid):

    lims = [den_grid[0], den_grid[-1], den_grid[0], den_grid[-1]]
    #ax.imshow(Z, extent=lims, origin='lower', cmap=cm.inferno)
    ax.imshow(Z, extent=lims, origin='lower', interpolation='nearest',
              cmap=colormap_black_to_blue())
    

def remove_electrons(cells):
    """
    Removes electrons from cells
    """
    new_cells = []
    for cell in cells:

        new_cell = []

        for prt in cell:
            if not (prt[3] < -0.9):  # electron has charge of -1
                new_cell.append(prt)

        new_cells.append(new_cell)

    return new_cells


def plot_el_density_and_QD_circles(ax, cells, fixed_particles, path):
    """
    Args:
        cells               QD:s and electrons  (QD circles hollow, classical electrons not shown)
        fixed_particles     driver quantum dots and others (plotted filled circle)
        path                for loading density plane
    """
    
    cells_no_el = remove_electrons(cells)
    # in boltz_E folder
    min_x, max_x, min_y, max_y = boltz_E_plotting.get_lims(cells, fixed_particles)

    Z, den_grid = load_dens_plane(path)
    
    # tune linewidth (somewhat) proportionally
    diag = np.sqrt((max_x - min_x)**2 + (max_y - min_y)**2)
    linewidth = np.sqrt(9 / diag) * 1.5

    plot_density_remote(ax, Z, den_grid)

    boltz_E_plotting.plot_permutation(cells_no_el, fixed_particles, linewidth=linewidth, ax=ax)
    if ax is None:
        plt.axis((min_x-1, max_x+1, min_y-2, max_y+2))
        plt.xlabel("x (nm)")
        plt.ylabel("y (nm)")
    else:
        ax.set_xlim([min_x-1, max_x+1])
        ax.set_ylim([min_y-1, max_y+1])
        ax.set_xlabel("x (nm)")
        ax.set_ylabel("y (nm)")



if __name__ == "__main__":
    main_function_if_executed_directly(sys.argv[1:])
