import matplotlib.pyplot as plt
import read_data.read_calc_data as read_calc_data
from plot_data.utils import plot_ob_as_function_of_range2
from read_data.utilities.read_particles import particle_labels
from os import makedirs, path


def plot_energy_Vt(init_setup):

    if init_setup.fig_ax is None:
        figname = "energy_Vt"
    else:
        figname = "energy_Vt_combination"

    # Hacky part combining multiple calculations, (you can safely assume that if branch is taken)
    if (init_setup.fig_ax is None) or ("plot_energy_Vt" not in init_setup.fig_ax):
        fig, ax = plt.subplots(1, 1, num=init_setup.parent_dir_name + " " + figname, figsize=(4.5, 4))
        if init_setup.fig_ax is not None:
            init_setup.fig_ax["plot_energy_Vt"] = fig, ax
    else:
        fig, ax = init_setup.fig_ax["plot_energy_Vt"]

    if init_setup.use_cache:
        attr_dict_Vt = read_calc_data.read_cached_observables(init_setup, "/E/Vt", 'energy.h5')
    else:
        _, attr_dict_Vt = read_calc_data.read_all_observables(init_setup, "/E/Vt", 'energy.h5')


    if init_setup.extrapolate_range1:
        data_Vt = attr_dict_Vt["mean_extrapolation_at_range1=0"]
        err_Vt = attr_dict_Vt["err_extrapolation_at_range1=0"]
    else:
        data_Vt = attr_dict_Vt["mean"]
        err_Vt = attr_dict_Vt["err"]

    plot_ob_as_function_of_range2(init_setup, ax, data_Vt, err_Vt,
                                  ylabel="$E$ ($E_h$)")

    fig.tight_layout()

    fig.savefig("results/" + figname + ".pdf")


def plot_energy_components(init_setup):

    dict = observables_for_each_particle_pair(init_setup,
                                              prefix="/E", postfix="",
                                              filename="energy.h5", block_data=False)

    if init_setup.fig_ax is None:
        figname = "energy_components"
    else:
        figname = "energy_components_combination"

    # Hacky part combining multiple calculations, (you can safely assume that if branch is taken)
    if (init_setup.fig_ax is None) or ("plot_energy_components" not in init_setup.fig_ax):
        fig, ax = plt.subplots(1, 1, num=init_setup.parent_dir_name + " " + figname, figsize=(4.5, 4))
        if init_setup.fig_ax is not None:
            init_setup.fig_ax["plot_energy_components"] = fig, ax
    else:
        fig, ax = init_setup.fig_ax["plot_energy_components"]

    for label in dict:
        # sem2 value
        (_data, attr) = dict[label]
        if init_setup.extrapolate_range1:
            mean = attr["mean_extrapolation_at_range1=0"]
            err = attr["err_extrapolation_at_range1=0"]
        else:
            mean = attr["mean"]
            err = attr["err"]

        plot_ob_as_function_of_range2(init_setup, ax, mean, err,
                                      # thermal components was only implemented:
                                      ylabel="$E_{thermal}$ ($E_h$)",
                                      legend=label)

    fig.tight_layout()
    fig.savefig("results/" + figname + ".pdf")


def observables_for_each_particle_pair(init_setup, prefix="/E", postfix="",
                                      filename="energy.h5", block_data=False):
    """
    Search observables generated for every particle pair.

    :param init_setup:
    :param prefix:      e.g. "/E" or "/PC"
    :param postfix:     e.g. "" or "/Rave"
                        - prefix and postfix are combined like so with pair:
                          "/E/e_He" or "/PC/e_He/Rave"
    :param filename:    e.g. "energy.h5" or "paircorrelation.h5"
    :param block_data:  whether or not to return blockwise data "data-matrix", if false return
                        only means with "atribure-matrix", the "data-matrix" will be 'None'.

    :return: dict       Dictionary that has particle-particle-label as a key and
                        (data-matrix, atribure-matrix) -tuple as a value.
                        "Matrix" means data for every parallel simulation over
                        range1 and range2.
                        Pro-tip: iterate over dict: >>> "for key in dict:"
    """
    # generate particle pairs like: ["/E/e_p", "/E/p_p", "/E/e_e"]
    pl = init_setup.prt_labels
    pairs = []
    for i in range(len(pl)):
        for j in range(len(pl)):
            pairs.append("".join([prefix, "/", pl[i], "_", pl[j], postfix]))

    dict = {}  # all pairs and corresponding observable

    for i, pair in enumerate(pairs):
        try:
            if block_data or (not init_setup.use_cache):
                data, attrs = read_calc_data.read_all_observables(
                                    init_setup,
                                    pair,
                                    filename=filename)
            elif init_setup.use_cache:
                attrs = read_calc_data.read_cached_observables(init_setup, pair, filename)
                data = None

            dict[pair] = (data, attrs)

        except ValueError:  # pair does not exist -> do nothing
            pass

    return dict
