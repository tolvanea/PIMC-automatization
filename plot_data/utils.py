import numpy as np
import matplotlib.ticker as mticker
from matplotlib.colors import to_rgb
from miscellaneous.general_tools import linear_errorbardata_fit

def align_text(ax, x, y, text, verticalalignment="center", transAxes=False):
    """
    Writes line of text to matplolib plot that is  aligned at '&'-symbol. (Not tested)
    :param ax:      axis
    :param x:       x pos.
    :param y:       y pos.
    :param text:    You maybe guess what this field is
    :param verticalalignment: e.g. "center", "top", "bottom"
    :param transAxes:         Use coordinates 0-1, 0-1 in x and y
    :return:
    """

    pos = text.find("&")
    if pos == -1:
        raise ValueError("Amperstand '&' not found in string.")

    first_dollar = text.find("$")
    if first_dollar != -1:
        second_dollar = text.find("$", first_dollar+1, len(text))
        if second_dollar == -1:
            raise ValueError("You should have two dollars in equation")
        if first_dollar < pos and pos < second_dollar:
            insert_dollars = True
        else:
            insert_dollars = False
        if text.find("$", second_dollar+1, len(text)) != -1:
            raise ValueError("Sorry, only impleneted one equation per line. \n"
                             +"(= Total two dollar signs)")
    else:
        insert_dollars = False

    first = text[:pos]
    last = text[pos + 1:]

    if insert_dollars:
        first = first + "$"
        last = last + "$"

    if transAxes:
        ax.text(x, y, first,
                verticalalignment=verticalalignment,
                horizontalalignment='right', transform=ax.transAxes)
        ax.text(x, y, last,
                verticalalignment=verticalalignment, horizontalalignment='left',
                transform=ax.transAxes)
    else:
        ax.text(x, y, first,
                verticalalignment=verticalalignment,
                horizontalalignment='right')
        ax.text(x, y, last,
                verticalalignment=verticalalignment, horizontalalignment='left')


def plot_ob_as_function_of_range2(init_setup, ax, data, err,
                                  ylabel,
                                  legend="default",
                                  ref_vals=True):
    """Helper function to plot observables that are in matrix form (range1 by range2).

    TODO The negative value logarithm plotting makes this code ugly. It could be rewritten
         way better with some custom function/class 'errorbar_but_works_with_neg()'. """

    range1, neg_log_info, ylabel0 = some_pre_processing(init_setup, data, err, ylabel)

    for i in range(len(data)):
        if init_setup.plot_only_extrapolation and i > 0:
            break
        label = make_label(init_setup, legend, range1[i])

        # Because matplotlib can not plot logarithmic plot from negative values
        x_in = init_setup.range2
        y_in = data[i, :]
        e_in = err[i, :]
        x_out, y_out, e_out = negative_log_plot_gimmick_1(x_in, y_in, e_in, neg_log_info)

        ls = None
        c = None
        if init_setup.linestyles is not None:
            ls = init_setup.linestyles[i]
        if init_setup.linecolors is not None:
            c = init_setup.linecolors[i]

        # ----------- The line that does it ------------
        # 2-sigma errorbars!
        (line, caps, barlines) = ax.errorbar(x_out, y_out, 2*e_out, label=label, capsize=4, zorder=1,
                                             ls=ls, c=c)
        # ----------------------------------------------

        # Because matplotlib cannot output proper pdf:s (transparency does not work)
        fake_transparency_with_whiteness(line, caps, barlines)

    # Because matplotlib can not plot logarithmic plot from negative values
    negative_log_plot_gimmick_2(init_setup, ax, neg_log_info, y_out)

    if ref_vals:
        plot_reference_values(init_setup, ax, ylabel, neg_log_info)

    if not (legend is None or legend == ""):
        labelspacing = 0.4
        ax.legend(labelspacing=labelspacing)

    if init_setup.latex_unit2 == "":
        ax.set_xlabel("${}$".format(init_setup.latex_symbol2))
    else:
        ax.set_xlabel("${}$ $(\\mathrm{{{}}})$".format(init_setup.latex_symbol2,
                                           init_setup.latex_unit2))
    # TODO return this to original. Ylabels removed from figures on to make them fit in latex
    ax.set_ylabel(ylabel0, rotation='horizontal')
    ax.yaxis.set_label_coords(-0.1,1.02)

    #ax.set_title(ylabel)

def is_num(num):
    not_none = not (num is None)
    not_nan = not (num != num)
    return not_nan and not_none

def some_pre_processing(init_setup, data, err, ylabel):

    if init_setup.extrapolate_range1:  # Draw extrapolated curve at range1=0.0
        assert(data.shape[0] == len(init_setup.range1) + 1)
        range1 = init_setup.range1_ext
    else:
        assert(data.shape[0] == len(init_setup.range1))
        range1 = init_setup.range1

    for j in range(len(data[0, :])):
        x_ = init_setup.range2[j]
        y_ = data[0, j]
        e_ = err[0, j]
        if is_num(y_) and is_num(e_):
            first_valid = (x_, y_)
            break
    else:
        raise Exception("Not a single valid calculation found with {} = {}"
                        .format(init_setup.latex_unit1, init_setup.range1[0]))


    x_sign = +1
    y_sign = +1
    log_y = init_setup.logarithmic_y_axis
    log_x = init_setup.logarithmic_x_axis

    # Trickery: negate suskeptibility plot so that upwards is higher diamagnetic responce
    if ylabel == "$\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$":
        ylabel0 = "$-" + ylabel[1:]
        y_sign = -1
        susc_plot = True
    else:
        ylabel0 = ylabel
        susc_plot = False

    # Trickery to plot negative values in log-plot
    if log_y and first_valid[1] < 0:
        y_sign = -1
    if log_x and first_valid[0] < 0:
        x_sign = -1
    neg_log_info = log_x, log_y, x_sign, y_sign, susc_plot
    return range1, neg_log_info, ylabel0

def make_label(init_setup, legend, range1_val):

    def is_whole_number(n: float, tol=1e-8):
        return (n % 1.0) < n*tol

    if legend is None or legend == "":
        label = None
    else:
        if init_setup.fig_ax is None:  # Normal route
            symbol = init_setup.latex_symbol1
            unit = init_setup.latex_unit1
            val = range1_val
        else:  # Ungly hack combining multiple figures to one
            symbol = init_setup.latex_symbol3
            unit = init_setup.latex_unit3
            val = init_setup.range3_val

        # Check if init_setup.latex_symbol1 is not latex symbol. If not, do not surround
        # with '$' and do not print unit
        if len(symbol) == 0:
            label = "{} {}".format(val, unit)
        elif (len(symbol) > 2 and "\\" not in symbol):
            label = "{} {} {}".format(symbol, val, unit)
        else:
            if isinstance(val, float) and is_whole_number(val):
                pretty_val = int(round(val))
            else:
                pretty_val = val
            label = "${} = {}\\;${}".format(symbol, pretty_val, unit)
        if legend != "default":
            label = legend + " " + label
    return label


def negative_log_plot_gimmick_1(x_in, y_in, e_in, neg_log_info):
    # This following crap makes logarithmic plots possible with (all) negative values
    # by using trick to negate data and flip plot axis.

    x_out = []
    y_out = []
    e_out = []
    log_x, log_y, x_sign, y_sign, susc_plot = neg_log_info
    for j in range(len(y_in)):
        if is_num(x_in[j]) and is_num(y_in[j]) and is_num(e_in[j]):
            x_out.append(x_in[j] * x_sign)
            y_out.append(y_in[j] * y_sign)
            e_out.append(e_in[j])
            # If log is used, make sure that values are all same sign
            if ((log_x and (x_in[j] * x_sign < 0)) or (log_y and (y_in[j] * y_sign < 0))):
                raise Exception(
                    "There exists both negative and positive values in data. All should be\\n"
                    + "positive or all should be negative. This is due to the log-plotting."
                )
    return np.array(x_out), np.array(y_out), np.array(e_out)

def brighter(c, amount):
        """
        amount == -1.0: dark,
        amount ==  0.0: no change,
        amount == +1.0: white
        """
        assert amount**2 <= 1.0
        if amount < 0:
            return c + c*amount
        else:
            return c + (1-c)*amount

def brighten(color, amount):
    return tuple(map(lambda c: brighter(c, amount), color))


def change_hue(color, amount):
    s = np.sin(amount*(2*np.pi))**2  # phase
    c = np.cos(amount*(2*np.pi))**2  # phase
    return (c*color[0]+s*color[1],
            c*color[1]+s*color[2],
            c*color[2]+s*color[0])

def fake_transparency_with_whiteness(line, caps, barlines):
    color = to_rgb(line.get_color())
    #color_lighter = (color[0]*0.7, color[1]*0.7, color[2]*0.7)
    for cap in caps:
        cap.set_color(brighten(color, 0.3))
    for barline in barlines:
        barline.set_color(brighten(color, 0.3))

def negative_log_plot_gimmick_2(init_setup, ax, neg_log_info, y_out):

    def invert_neg(num, tick_idx):
        #return -num
        string = "–{:.0e}".format(num).replace("e", "×$10^{") + "}$"
        if string[1] in ["1","5"]:
            return string
        else:
            return ""

    def invert_pos(num, tick_idx):
        #return -num
        string = "{:.0e}".format(num).replace("e", "×$10^{") + "}$"
        if string[0] in ["1","5"]:
            return string
        else:
            return ""
    log_x, log_y, x_sign, y_sign, susc_plot = neg_log_info

    # HACK: Check if multiple calculation being combined, and if so, execute only to last one
    other_than_last_calc_in_range3 = ((init_setup.fig_ax is not None)
                           and init_setup.range3_idx < init_setup.fig_ax["range3_len"] - 1)
    if other_than_last_calc_in_range3:
        return

    y = y_out
    x = init_setup.range2

    if log_x:
        ax.set_xscale("log", nonpositive='clip')
        if x_sign < 0:  # Trickery to plot negative values in log-plot
            ax.invert_xaxis()
            ax.xaxis.set_major_formatter(mticker.FuncFormatter(invert_neg))
            ax.xaxis.set_minor_formatter(mticker.FuncFormatter(invert_neg))

    if log_y:
        ax.set_yscale("log", nonpositive='clip')
        y_magnitudes = np.floor(np.log10(np.max(y)*1.2)) - np.ceil(np.log10(np.min(y)*0.8))
        if y_sign < 0 and not susc_plot:  # Trickery to plot negative values in log-plot
            ax.invert_yaxis()
            ax.yaxis.set_major_formatter(mticker.FuncFormatter(invert_neg))
            ax.yaxis.set_minor_formatter(mticker.FuncFormatter(invert_neg))
        elif y_magnitudes < 1:
            ax.yaxis.set_major_formatter(mticker.FuncFormatter(invert_pos))
            ax.yaxis.set_minor_formatter(mticker.FuncFormatter(invert_pos))



def plot_reference_values(init_setup, ax, ylabel, neg_log_info):

    # UNGLY HACK ADDITION: it checks here if multiple calculation are being combined, and if so,
    # then plot reference value of only last one
    other_than_last_calc_in_range3 = ((init_setup.fig_ax is not None)
                           and init_setup.range3_idx < init_setup.fig_ax["range3_len"] - 1)

    log_x, log_y, x_sign, y_sign, susc_plot = neg_log_info

    # Plot reference values that may be given. First condition checks if reference value is given.
    # Oh yes hacky spaghetti
    if (ylabel in init_setup.reference_values and not other_than_last_calc_in_range3):

        linestyles = ['--', '-.', ':']
        # Awful code, who wrote this?? Ah I did.
        # Anyways, this is organically grown mess, only made to fix that one small thing that I wanted to figures. For example, setting marker styles and marker colors are made quite differently.
        markerstyles = ['o', '^', '*', 's', 'x']
        markersizes = [None for _ in range(5)]
        if init_setup.ref_marker_colors is not None:
            markercolors = init_setup.ref_marker_colors
        else:
            markercolors = ["C{}".format(i) for i in range(10)]
        x = [init_setup.range2[0], init_setup.range2[-1]]  # overwide x axis
        ylims = [None, None]
        xlims = [None, None]

        for i, info in enumerate(init_setup.reference_values[ylabel]):
            marker = None
            marker_size = None
            if len(info) == 2:
                lab, y_val = info
                x_val = None
            elif len(info) in [3,4,5]:
                markercolor = markercolors[i%len(markercolors)]
                lab = info[0]
                # Quick dirty hack to force ylims
                if lab == "ylims":
                    ylims[0] = info[1]
                    ylims[1] = info[2]
                    continue
                if lab == "xlims":
                    xlims[0] = info[1]
                    xlims[1] = info[2]
                    continue
                lab, x_val, y_val = info[:3]
                if len(info) < 4:
                    marker = markerstyles[i%len(markerstyles)]
                else:
                    marker = info[3]
                    # If integer is given, do not make label entry for this marker
                    if isinstance(info[3], int):
                        markercolor = markercolors[marker%len(markercolors)]
                        marker_size = markersizes[marker]
                        marker = markerstyles[marker]
                        lab = None
                    else:
                        if i < len(markerstyles):
                            markerstyles[i] = marker
                        else:
                            markerstyles.append(marker)
                if len(info) >= 5:
                    marker_size = info[4]
                    if i < len(markersizes):
                        markersizes[i] = marker_size
                    else:
                        markersizes.append(marker_size)
            else:
                raise ValueError("Invalid reference value: Must be (y-label, y) or (y-label, y, x)")
            # Because matplotlib is buggy, draw it with errorbar instad of line.
            # (Otherwise legend are in wrong order).
            if x_val is None:
                (line, caps, barlines) = ax.errorbar(x, [y_val*y_sign, y_val*y_sign], [0,0],
                                                     label=lab, ls=linestyles[i%3])
            else:
                (line, caps, barlines) = ax.errorbar(x_val*x_sign, y_val*y_sign, [0], label=lab,
                                                     ls="", marker=marker, markersize=marker_size,
                                                     color=markercolor)
            for cap in caps:
                cap.set_color((1.0, 1.0, 1.0))
            for barline in barlines:
                barline.set_color((1.0, 1.0, 1.0))
        if (ylims[0] is not None) or (ylims[1] is not None):
            if y_sign > 0:
                ax.set_ylim(ylims)
            else:
                ax.set_ylim((ylims[1], ylims[0]))
        if (xlims[0] is not None) or (xlims[1] is not None):
            if x_sign > 0:
                ax.set_xlim(xlims)
            else:
                ax.set_xlim((xlims[1], xlims[0]))
