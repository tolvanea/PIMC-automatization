import numpy as np
import matplotlib.pyplot as plt
import read_data.read_calc_data as read_calc_data
from plot_data.print_data import print_and_write_matrix
from os import makedirs, path




def plot_and_print_PC(init_setup, plot_pc_distribution=False):
    """Main function that plots everything in pair-correlation.
    (Plots distribution and mean distances)"""

    # Plot and print 'Rave' which is calculated on PIMC side and stored in hdf5
    # file. (This same thing is calculated from PC distribution also.)
    print_average_distances = True
    if print_average_distances:
        print_Rave_R2ave_1perRave(init_setup)

    # Plot pair correlation curves
    # AND
    # Plot & print most likely separation distance using PC distribution
    # (Distance is zero point of derivative in PC distribution.)
    if plot_pc_distribution:
        name1 = "paircorrelation_distributions.pdf"
        name2 = "separation_distances.pdf"
        fig1, axes1 = plt.subplots(2, 2, num=name1, figsize=(9, 8))
        fig2, ax2 = plt.subplots(1, 1, num=name2, figsize=(4.5, 4))
        plot_and_print_PC_distances(init_setup, ax2, axes1)
        fig1.tight_layout()
        fig2.tight_layout()
        fig1.savefig("results/" + name1)
        fig2.savefig("results/" + name2)


def plot_pair_correlation(ax, PC_mat, PC_err_mat, attrs, r_grid, pair, init_setup):
    nz = np.nonzero(attrs["succeed"])
    for i in range(len(nz[1])):
        # NOTICE: this is the given index, it crops out unsuccesful simulations
        idx = (nz[0][i], nz[1][i])  # index of succesfull simulation


        PC, PC_err = PC_mat[idx], PC_err_mat[idx]
        r = r_grid[idx][:]

        R_is = "{}{},${}={}{}$".format(
            init_setup.str_range1[idx[0]],
            init_setup.latex_unit1,
            init_setup.latex_symbol2,
            init_setup.str_range2[idx[1]],
            init_setup.latex_unit2
        )

        label = ", ".join([pair[3:-1], R_is])

        # 2-sigma errorbars!
        ax.errorbar(r, PC, 2*PC_err, label=label)
        ax.legend()

def PC_sem(pc_all_blocks, attrs, idx):
    """Takes mean of pair correlation distribution that is from of different
    blocks.

    Takes in data matrix of shape (var_x,var_y,blocks,1,1,r_grid) (<- OUTDATED?)
    outputs 1d-arrays."""
    import read_data.utilities.sem2 as sem2
    try:
        S = attrs["S"][idx]
        N = attrs["N"][idx]
    except KeyError:
        raise Exception("Crap")
    if S >= N:
        print("Warning (in PC_sem), S={} > N={}!  name: {}"
              .format(S, N, attrs["name"]))
        S = N//2
        #return None, None

    d = pc_all_blocks[S-1:N, :, 0]  # 0, 0,

    PC = np.zeros(d.shape[1], dtype=pc_all_blocks.dtype) + 9999
    PC_err = np.zeros(d.shape[1], dtype=pc_all_blocks.dtype) + 9999

    for i in range(d.shape[1]):
        PC[i], PC_err[i] = sem2.sem2(d[:, i])

    return PC, PC_err


def read_PC_matrices(init_setup, pair):
    """
    Read pair correlation distribution for each calculation (calculations forms matrix). The
    distribution is block mean. Also calculate errorbars for the distribution.

    :return
        (True, mean_matrix, error_matrix, r_grid) if pair was found
        (False, (), (), (), ()) if pair was not found
    """
    # This is somewhat dirty hack: Instead of figuring out what pair correlations
    # are calculated, this code just tries them all with `try`-block. It works
    # and saves a lot of code so why even fix it.
    try:
        data, attrs = read_calc_data.read_all_observables(
            init_setup,
            pair + "PC",
            filename="paircorrelation.h5")

        r_grid_mat, _ = read_calc_data.read_all_observables(
            init_setup,
            pair + "r",
            filename="paircorrelation.h5")
    except ValueError:
        return (False, (), (), (), ())  # particle pair does not exist


    PC_mat = np.full(data.shape[:2], fill_value=None, dtype=object)
    PC_err_mat = np.full(data.shape[:2], fill_value=None, dtype=object)
    for id1 in range(data.shape[0]):
        for id2 in range(data.shape[1]):
            if data[id1, id2] is None:
                continue
            PC_for_every_block = data[id1, id2]
            PC, PC_err = PC_sem(PC_for_every_block, attrs, (id1, id2))

            # Normalize
            r_grid = r_grid_mat[id1, id2]
            A = PC.sum() * (r_grid[-1]/len(r_grid))
            PC /= A
            PC_err /= A

            PC_mat[id1, id2] = PC
            PC_err_mat[id1, id2] = PC_err
    return (True, PC_mat, PC_err_mat, r_grid_mat, attrs)


def plot_mean_distances(init_setup, ax, avg_r, hwhm, pair):
    for i in range(len(init_setup.range1)):
        ax.errorbar(init_setup.range2, avg_r[i, :], hwhm[i, :],
                    label="{}, ${} = {} {}$".format(
                        pair,
                        init_setup.latex_symbol1,
                        init_setup.str_range1[i],
                        init_setup.latex_unit1),
                    capsize=4)

        if init_setup.logarithmic_x_axis:
            ax.set_xscale("log", nonposx='clip')
        ax.set_title("avg r, (errorbar is HWHM)")
        ax.legend()
        if init_setup.latex_unit2 == "":
            ax.set_xlabel("${}$".format(init_setup.latex_symbol2))
        else:
            ax.set_xlabel("${}$ $({})$".format(init_setup.latex_symbol2,
                                               init_setup.latex_unit2))
        ax.set_ylabel("R")

def print_distance_data(init_setup, write_file, pair, peak_r, avg_r, hwhm):
    with open(write_file, 'a') as f:
        line = "Pair correlation distribution data {}:".format(pair)
        f.write(line + "\n")
        print(line)

    # This first one just prints table column names
    cut_lines = [0] + list(range(-peak_r.shape[0]-1, 0, 1))
    print_and_write_matrix(init_setup, peak_r, err=None, title="asdf", write_file=write_file,
                           cut_lines=cut_lines, identation="        ")

    title1 = "    r at maximal probability:".format(pair)
    print_and_write_matrix(init_setup, peak_r, err=None, title=title1, write_file=write_file,
                           cut_lines=[1, 2, -1], identation="        ")
    title2 = "    expected value of r, (should be the same as 'Rave'):".format(pair)
    print_and_write_matrix(init_setup, avg_r, err=None, title=title2, write_file=write_file,
                           cut_lines=[1, 2, -1], identation="        ")
    title3 = "    hwhm (i.e. half width):".format(pair)
    print_and_write_matrix(init_setup, hwhm, err=None, title=title3, write_file=write_file,
                           cut_lines=[1, 2, ], identation="        ")

def plot_and_print_PC_distances(init_setup, ax, axes):
    """Plots mean distances to `ax`, plots pair correlation distribution to `axes` and print
     average data to text file."""

    # Clear the output file.
    import datetime
    import socket
    write_file = "results/print_data_distances.txt"
    with open(write_file, 'a') as f:
        date = datetime.datetime.now().replace(microsecond=0).isoformat()
        host = socket.gethostname()
        line = "{} {}\n\n\n".format(host, date)
        f.write(line)
        print("\n\n\n")

    prs = init_setup.prt_labels
    pairs = []
    for i in range(len(prs)):
        for j in range(len(prs)):
            pairs.append("".join(["PC/", prs[i], "_", prs[j], "/"]))
    # Example of pairs: `["PC/e_p/", "PC/p_p/", "PC/e_e/"]`

    titles = ["", "", "", ""]  # Four axes is hard coded
    axs = [axes[0][0], axes[0][1], axes[1][0], axes[1][1]]  # Four axes is hard coded

    for i, pair in enumerate(pairs):
        i_mod = np.mod(i, 4)  # Four axes is hard coded
        ax_mod = axs[i_mod]

        found, PC_mat, PC_err_mat, r_grid, attrs = read_PC_matrices(init_setup, pair)
        if not found:
            continue

        # Plot pair correlation distribution
        plot_pair_correlation(ax_mod, PC_mat, PC_err_mat, attrs, r_grid, pair, init_setup)
        titles[i_mod] += " " + pair
        ax_mod.set_title(titles[i_mod])

        # Plot separation distances by function of second variating value.
        # peak_r is the most probable r value, avg_r is averate value, hwhm is half width half maximum
        peak_r, avg_r, hwhm = PC_most_probable_dists(PC_mat, PC_err_mat, attrs, r_grid, pair)

        plot_mean_distances(init_setup, ax, avg_r, hwhm, pair)

        print_distance_data(init_setup, write_file, pair, peak_r, avg_r, hwhm)



def PC_most_probable_dists(PC_mat, PC_err_mat, attrs, r_grid, pair):
    """Calculate information from pair correlation distribution
        * maximum (i.e. most probable separation)
        * average (i.e. average separation)
        * half width half maximum (HWHM) (i.e deviation in separation)
    """
    from scipy import interpolate

    max_mat = np.zeros(PC_mat.shape, dtype=np.float64("nan"))
    avg_mat = np.zeros(PC_mat.shape, dtype=np.float64("nan"))
    hwhm_mat = np.zeros(PC_mat.shape, dtype=np.float64("nan"))

    for i in range(PC_mat.shape[0]):
        for j in range(PC_mat.shape[1]):
            if attrs["N"][i, j] != 9999:  # no calculation available

                r = r_grid[i, j][:]
                PC, PC_err = PC_mat[i,j], PC_err_mat[i,j]

                # Finding maximum
                PC_spline = interpolate.UnivariateSpline(r, PC, k=4, s=0.0)
                xnew = np.linspace(r[0], r[-1], 1000)
                ynew = PC_spline(xnew)
                deriv = PC_spline.derivative(n=1)
                extrema = deriv.roots()

                coarse_peak_max_x = r[np.argmax(PC)]

                if len(extrema) == 1:
                    mx = extrema[0]
                else:
                    if len(extrema) == 0:
                        continue  # max_mat[i,j] = nan
                    else:  # Pick maximal root value
                        mx = -1.0
                        my = -1.0
                        for e in extrema:
                            if my < PC_spline(e):
                                my = PC_spline(e)
                                mx = e

                    # Finding maximum fails, can be removed, TODO?
                    # This is some fallback on pathological cases
                    if np.abs(coarse_peak_max_x - mx) > 0.15*mx:
                        if np.max(PC) < 0.99:  # Not static particle-particle
                            print("coarse_peak_max_x, mx", coarse_peak_max_x, mx)
                            fail_debug_plot(xnew, ynew, r, PC, PC_err, mx, deriv, j, pair)
                            mx = coarse_peak_max_x

                # Finding average
                avg_r = (r * (PC/PC.sum())).sum()

                # Finding HWHM
                max_y = PC_spline(mx)
                PC_shifted = interpolate.UnivariateSpline(r, PC-max_y/2, k=3, s=0.0)
                halfs = PC_shifted.roots()
                if len(halfs) == 2:
                    err = (halfs[1] - halfs[0])/2
                else:
                    print("Error: 'Separation distances' are not reliable, PC-HWHM is not"
                          +" well defined.")
                    err = float("nan")

                max_mat[i, j] = mx
                avg_mat[i, j] = avg_r
                hwhm_mat[i, j] = err

    return max_mat, avg_mat, hwhm_mat


def fail_debug_plot(xnew, ynew, r, PC, PC_err, mx, deriv, j, pair):
    """Alertion when if automatic separation detection fails.
    This is just debugging information on what went wrong. (This entire function
    could be removed from code, as original code works pretty well)"""
    fig1, axes = plt.subplots(1, 2,
                              num='DEBUGGIA' + str(
                                  j) + str(
                                  np.random.random()),
                              figsize=(
                                  9, 4))  # =(4.4,3.2))

    axes[0].plot(xnew, ynew, label="spline fit")
    # 2-sigma errorbars
    axes[0].errorbar(r, PC, 2*PC_err, linewidth=2,
                     label="data", linestyle=":")
    axes[0].plot([mx, mx], [0, 0.01], c="b",
                 label="proposed maximum")

    axes[1].plot(xnew, deriv(xnew))
    axes[1].plot([r[0], r[-1]], [0, 0])

    axes[0].legend()

    axes[0].set_title("{}, Failed to find separation distance".format(pair))
    axes[1].set_title("This is the derivative, is there\n only one root (i.e maximum)?")


def print_Rave_R2ave_1perRave(init_setup):
    """
    prints PC average observables Rave (<r>), R2ave (<R^2>), 1perRave (<1/R>)
    (I wanted to print tables in inverse order, and this function is so ugly mess for that. It
    is more consistent to previous tables to print table per pair, and variate observable in row.)
    """
    from plot_data.plot_energy import observables_for_each_particle_pair
    from os.path import dirname, basename

    dict_Rave = observables_for_each_particle_pair(init_setup,
                                                   prefix="/PC", postfix="/Rave",
                                                   filename="paircorrelation.h5")
    dict_R2ave = observables_for_each_particle_pair(init_setup,
                                                   prefix="/PC", postfix="/R2ave",
                                                   filename="paircorrelation.h5")
    dict_Rave1per = observables_for_each_particle_pair(init_setup,
                                                   prefix="/PC", postfix="/Rave1per",
                                                   filename="paircorrelation.h5")
    dicts = [dict_Rave, dict_R2ave, dict_Rave1per]
    labels = ["Rave", "R2ave", "Rave1per"]
    write_file = 'results/print_data_distances.txt'
    for pair_idx, pair_and_obs0 in enumerate(dicts[0]):
        # This first one just prints table column names (so it prints no data)
        # (Pick column width from first particle pair)
        attrs0 = dicts[0][pair_and_obs0][1]
        if init_setup.extrapolate_range1:
            mean0 = attrs0["mean_extrapolation_at_range1=0"]
            err0 = attrs0["err_extrapolation_at_range1=0"]
        else:
            mean0 = attrs0["mean"]
            err0 = attrs0["err"]

        cut_lines0 = [0] + list(range(-mean0.shape[0]-1, 0, 1))

        with open(write_file, 'a') as f:
            pair0 = dirname(pair_and_obs0)
            line = "Pair {}:".format(pair0)
            f.write(line + "\n")
            print(line)

        print_and_write_matrix(init_setup, mean0, err0, title="asdf", write_file=write_file,
                               cut_lines=cut_lines0, identation="        ")
        for i_obs in range(3):
            dict = dicts[i_obs]
            pair_and_obs = list(dict.keys())[pair_idx]
            try:
                (_data, attr) = dict[pair_and_obs]
            except KeyError:
                print("Internal error, inconsistent pair correlation observables")
                print("Keys:", dict.keys(), "tried to find key:", pair_and_obs)
                continue
            # sem2 values of pair correlation observable
            if init_setup.extrapolate_range1:
                mean = attr["mean_extrapolation_at_range1=0"]
                err = attr["err_extrapolation_at_range1=0"]
            else:
                mean = attr["mean"]
                err = attr["err"]
            cut_lines = [1, 2, -1] if (i_obs != 2) else [1, 2]
            obs = basename(pair_and_obs)
            title = "    {}".format(labels[i_obs], obs)
            print_and_write_matrix(init_setup, mean, err, title=title, write_file=write_file,
                                   cut_lines=cut_lines, identation="        ")
