import os
import sys
import numpy as np
import string

import read_data.read_calc_data as read_calc_data
from uncertainties import ufloat  # pip3 install uncertainties
from miscellaneous.general_tools import to_precision_fixed

def print_energy_and_block_info(init_setup):
    """Prints energy values (Et/Vt) and also number of successful blocks"""

    # check current directory is correct
    read_calc_data.initial_error_checking()

    write_file = 'results/print_data.txt'
    if not os.path.isdir("results"):
        os.mkdir("results")

    with open(write_file, 'w') as f:
        import socket
        import datetime
        date = datetime.datetime.now().replace(microsecond=0).isoformat()
        host = socket.gethostname()
        lines = []
        lines.append("In directory {}".format(os.getcwd()))
        lines.append("{} {}\n\n\n".format(host, date))

        print_succesful_blocks(init_setup, lines)
        for line in lines:
            f.write(line + "\n")
            print(line)

    for obs_name in ["/E/Et", "/E/Vt"]:
        if init_setup.use_cache:
            attr_dict = read_calc_data.read_cached_observables(init_setup, obs_name, "energy.h5")
        else:
            _, attr_dict = read_calc_data.read_all_observables(init_setup, obs_name, "energy.h5")
        if init_setup.extrapolate_range1:
            mat = attr_dict["mean_extrapolation_at_range1=0"]
            err = attr_dict["err_extrapolation_at_range1=0"]
        else:
            mat = attr_dict["mean"]
            err = attr_dict["err"]
        print_and_write_matrix(init_setup, mat, err,
                               title="\nEnergy {}".format(obs_name),
                               write_file=write_file, identation="    ")




def print_succesful_blocks(init_setup, lines):
    """ Print for every block:
            - truth value if some obserables could be read meaningfully
            - number of blocks that were calculated.
    Also, store failed blocks in init_setup
    """
    lines.append("Simulation blocks:")

    leftmost_column_width, column_width = print_first_row(
        init_setup, lines, max_width_of_cell=13
    )


    fail_lines = []
    for i in range(len(init_setup.dir1_labels)):
        line = []
        for j in range(-1, len(init_setup.dir2_labels)):
            if j == -1:
                row_prefix = ("{:{w}}|").format(init_setup.dir1_labels[i], w=leftmost_column_width)
                line.append(row_prefix)
                continue

            S = init_setup.S_mat[i,j]
            N = init_setup.N_mat[i,j]
            # No calculation available
            if init_setup.failed_blocks_mat[i,j] is None:
                found = "x"
                fail_lines.append("{}, {}, No calculation found"
                                  .format(init_setup.dir1_labels[i], init_setup.dir2_labels[j]))
            # Success
            elif len(init_setup.failed_blocks_mat[i,j]) == 0:
                found = "s"
            else:
                found = "?"
                fail_lines.append("{}, {}, S: {}  N: {}"
                                  .format(init_setup.dir1_labels[i],
                                          init_setup.dir2_labels[j],
                                          S, N))
                fail_lines.append("- 'Et/Vt' blocks that could no be read because they are zero:")
                fail_lines.append("  {}".format(init_setup.failed_blocks_mat[i,j]))
                fail_lines.append("- All observables of this block are replaced with data of")
                fail_lines.append("  some nearby block\n")

            if found != 'x':
                cell = found + str(S) + "/" + str(N)
                cell = "{:{w}}".format(cell, w=column_width+1)
            else:
                cell = "{:{w}}".format(found, w=column_width+1)
            line.append(cell)

        lines.append("    " + "".join(line))
    lines.append("Notation above:")
    lines.append("    s<S>/<N>': all success")
    if len(fail_lines) > 0:
        lines.append("    '?<S>/<N>': errors detected in some blocks")
        lines.append("    'x': no calculation found")
    lines.append("where N is total number of blocks and S is convergence point.")
    lines.append("All observable averages are calculated from blocks S-N.")
    if len(fail_lines) > 0:
        lines.append("")
        lines.append("THERE IS SOME SORT OF ERROR IN READING CALCULATION DATA")
        lines.append("Whether it is bad or not is your decision.")
        lines.append("In above chart, erroneous calculation is indicated with prefix '?' and")
        lines.append("missing ones with 'x'. Here is some more information:")
        for fail_line in fail_lines:
            lines.append("    " + fail_line)
    lines.append("")


def print_and_write_matrix(init_setup, mat, err, title, write_file, cut_lines=None, identation="    "):
    """
    :param `cut_lines`  lines indices that are dropped out. Must be in order
                        like so: [1, 3, -2, -1]
    """

    lines = []
    print_data_matrix(init_setup, lines, mat, err, title=title, identation=identation)

    if cut_lines is not None:
        lines_new = []
        for idx, line in enumerate(lines):
            if (idx in cut_lines) or ((idx - len(lines)) in cut_lines):
                continue
            else:
                lines_new.append(line)
        lines = lines_new

    with open(write_file, 'a') as f:
        for line in lines:
            f.write(line + "\n")
            print(line)


def print_data_matrix(init_setup, lines, data, err=None, title="", identation="    "):
    """
    Writes output to parameter list 'lines' like:
    ```
    print_obs(), obsname=Et
    \tau\  T|T300.000  |T1000.0   |
    -------------------------------
    tau0.01|-1.173(12)|-1.158(9) |
    tau0.02|-1.173(12)|-1.158(9) |
    ```
    """
    lines.append(title)

    # matrix of results as formatted strings
    str_mat = [[None for j in range(data.shape[1])] for i in range(data.shape[0])]

    if data.shape[0] == len(init_setup.dir1_labels) + 1:
        sym_name = init_setup.latex_symbol1.replace("\\","")
        range1_labels = [sym_name + str(0.0)] + init_setup.dir1_labels
    else:
        range1_labels = init_setup.dir1_labels


    max_cell_width = 0  # maxiumum cell size
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if data[i, j] is None or data[i, j] == 0.0:
                cell = " " * 6
            elif err is None:
                # Assuming result is less precise than 6 digits
                cell = to_precision_fixed(data[i, j], 6)
            else:
                e = 2 * err[i, j]  # 2-sigma errorbars!
                cell = short_format(ufloat(data[i, j], e))
                # Or alternatively:
                #cell = miscellaneous.general_tools.pretty_print_errorbars(data0[i, j], e)
            str_mat[i][j] = cell
            max_cell_width = max(len(cell), max_cell_width)

    leftmost_column_width, column_width = print_first_row(
        init_setup, lines, max_width_of_cell=max_cell_width, identation=identation,
        dir1_labels=range1_labels
    )

    for i in range(data.shape[0]):
        line = []
        for j in range(-1, data.shape[1]):

            if j == -1:
                row_prefix = "{:{w}}|".format(range1_labels[i],
                                              w=leftmost_column_width)
                line.append(row_prefix)

            else:
                if data[i, j] is not None:
                    val = str_mat[i][j]
                    cell = "{:{w}}|".format(val, w=column_width)
                    line.append(cell)
                else:
                    line.append(" " * column_width + "|")

        lines.append(identation + "".join(line))

    lines.append("")


def print_first_row(init_setup, lines, max_width_of_cell=15, identation="    ", dir1_labels=None):
    """
    This is helper function for 'print_obs()' and 'print_succesful_blocks'
    :param init_setup:
    :param max_width_of_cell:
    :return:
        first column width
        non-first column width
    """
    longest_label1 = -1
    longest_label2 = -1
    if dir1_labels is None:
        dir1_labels = init_setup.dir1_labels
    for label1 in dir1_labels:
        longest_label1 = max(len(label1), longest_label1)
    for label2 in init_setup.dir2_labels:
        longest_label2 = max(len(label2), longest_label2)
    leftmost_column_width = max(longest_label1,
                                len(init_setup.latex_symbol1) + len(init_setup.latex_symbol2) + 1)
    space = leftmost_column_width - len(init_setup.latex_symbol1) - len(init_setup.latex_symbol2)
    column_width = max(longest_label2 + 1, max_width_of_cell)

    first_row = []
    for j in range(-1, len(init_setup.range2)):
        if j == -1:
            upper_corner = "{:<}{:<{w}}{:>}|".format(init_setup.latex_symbol1,
                                                     " ",
                                                     init_setup.latex_symbol2,
                                                     w=space)
            first_row.append(upper_corner)
        else:
            first_row.append(("{:" + str(column_width) + "}|").format(init_setup.dir2_labels[j]))

    line = "".join(first_row)
    lines.append(identation + line)
    lines.append(identation + "-"*len(line))

    return leftmost_column_width, column_width


class ShorthandFormatter(string.Formatter):
    """ Format uncertainties better.
    https://pythonhosted.org/uncertainties/user_guide.html
    """

    def format_field(self, value, format_spec):
        from uncertainties import UFloat
        if isinstance(value, UFloat):
            return value.format(format_spec + 'S')  # Shorthand option added
        # Special formatting for other types can be added here (floats, etc.)
        else:
            # Usual formatting:
            return super(ShorthandFormatter, self).format_field(
                value, format_spec)

def short_format(x):
    """Format uncertainties number in form of 1.23(45) instead of 1.2345+/-0.0045."""

    frmtr = ShorthandFormatter()
    return frmtr.format("{0:u}", x)
