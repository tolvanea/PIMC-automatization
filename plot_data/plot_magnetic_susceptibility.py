import numpy as np
import matplotlib.pyplot as plt
import read_data.read_calc_data as read_calc_data
from plot_data.utils import plot_ob_as_function_of_range2
from read_data.read_calc_data import extrapolate_range1_at_0
from read_data.construct_observables.read_diamagnetic_susceptibility \
    import read_susecptibility_from_r2_data

def plot_and_print_susceptibility(init_setup):

    unit = ["SI", "cgs", "au"][0]  # You can change unit by changing that hard coded index here.
    # Mgn susceptibility with A^2
    susc_by_crd, susc_err_by_crd, susc_avg, susc_err_avg \
        = read_magnetic_susceptibility(init_setup, unit)

    # Mgn susceptibility with <r^2> using Rebane's equation. (Only for two kinds of particle types.)

    if init_setup.fig_ax is None:
        # THIS IF-BRANCH IS THE DEFAULT ACTION, DO NOT CARE ABOUT THAT HACKY ELSE BRANCH
        plot_susceptibility(init_setup, susc_avg, susc_err_avg, unit)
        print_susceptibility(init_setup, susc_avg, susc_err_avg, unit, susc_by_crd, susc_err_by_crd,
                             "using <A^2>", overwrite=True)
        if init_setup.r2_susceptibilities:
            susc_r2, susc_r2_err = read_susecptibility_from_r2_data(init_setup, unit)
            if susc_r2 is not None:
                plot_susceptibility(init_setup, susc_r2, susc_r2_err, unit, figname_postfix="_r2")
                print_susceptibility(init_setup, susc_r2, susc_r2_err, unit, None, None,
                                    "using <r^2>", overwrite=False)
        # Print and plot other components of susceptibility for testing purposes
        if init_setup.partial_susceptibilites:

            names_labels_and_important = partial_susc_datasetnames(init_setup)

            for dset, label, important in names_labels_and_important:
                try:
                    by_crd, err_by_crd, avg, err_avg = read_magnetic_susceptibility(
                        init_setup, unit, base=dset
                    )
                except ValueError as e:
                    if important:
                        raise e
                    else:
                        continue
                plot_susceptibility(init_setup, avg, err_avg, unit, figname_postfix="_"+label, ref_vals=False)
                print_susceptibility(
                    init_setup, avg, err_avg, unit, by_crd, err_by_crd, label, overwrite=False
                )

    else: # Ugly hack, watch elsewhere. This combines many calculations in one.
        # Unite susc A^2, r^2 figures (valid only when calculation combination is on)
        # This else-branch sets linestyles and colors manually for each of them
        from plot_data.utils import brighten, change_hue
        from matplotlib import colors

        r1len = len(init_setup.range1)
        # Set curve legend label the ugly way
        unit_copy = init_setup.latex_unit3
        init_setup.latex_unit3 = str(unit_copy) #+ "$\\langle A^2 \\rangle$"
        # Set line color the ugly way
        lc_copy = init_setup.linecolors
        color = colors.to_rgb("C{}".format(init_setup.range3_idx))
        init_setup.linecolors = [color] * r1len
        # Disable dublicate reference value plotting (and log inverting) the "ugly hacky"-way
        ri_copy = init_setup.range3_idx
        if init_setup.unite_A2_figure_with_r2:
            init_setup.range3_idx = -1

        plot_susceptibility(init_setup, susc_avg, susc_err_avg, unit)
        print_susceptibility(init_setup, susc_avg, susc_err_avg, unit, susc_by_crd, susc_err_by_crd,
                             "using <A^2>", overwrite=True)

        if init_setup.r2_susceptibilities:
            # Disable dublicate reference value plotting (and log inverting) the "ugly hacky"-way
            init_setup.range3_idx = ri_copy
            # Set reference value marker colors the ugly hacky way
            mc_copy = init_setup.ref_marker_colors
            change = 0.06
            if mc_copy is None:
                mcs = [change_hue(colors.to_rgb("C{}".format(i)), change) for i in range(10)]
                init_setup.ref_marker_colors = mcs
            # Set curve legend label the ugly way
            init_setup.latex_unit3 = str(unit_copy) #+ " $\\langle r^2 \\rangle$"
            # Set curve linestyle the ugly way
            ls_copy = init_setup.linestyles
            init_setup.linestyles = ["--"] * r1len
            # Set line color the ugly way
            similar_color2 = brighten(change_hue(color, 2*change), -0.0)
            init_setup.linecolors = [similar_color2] * r1len

            susc_r2, susc_r2_err = read_susecptibility_from_r2_data(init_setup, unit)

            plot_susceptibility(init_setup, susc_r2, susc_r2_err, unit, figname_postfix="_r2",
                                unite=init_setup.unite_A2_figure_with_r2)
            print_susceptibility(init_setup, susc_r2, susc_r2_err, unit, None, None,
                                "using <r^2>", overwrite=False)
            init_setup.linestyles = ls_copy
            init_setup.ref_marker_colors = mc_copy

        init_setup.latex_unit3 = unit_copy
        init_setup.linecolors = lc_copy

def partial_susc_datasetnames(init_setup):
    p = "/MAGN/dia.susc._"
    names_labels_and_important = [
        (p+"(qA)^2_e_only_",        "e_only", True),
        (p+"(qA)^2_p_only_",        "p_only", True),
        (p+"cross_terms_xy-yz-zx_", "cross_terms_<r_i*r_j>", False),
        (p+"(A_e*A_p)_",            "(A_e*A_p)", False),
        (p+"(sumA)^2_",             "(sumA)^2", False),
        (p+"(q_e*A_e)*(q_p*A_p)_",  "(q_e*A_e)*(q_p*A_p)", False),
        ]
    c = 0
    for i, count in enumerate(init_setup.prt_counts):
        for _ in range(count):
            c += 1
            name = "prt" + str(c) + "_"
            names_labels_and_important.append(
                (p + "(qA)^2_" + name, name + init_setup.prt_labels[i], True)
            )

    return names_labels_and_important

def read_magnetic_susceptibility(init_setup, unit, base="/MAGN/dia.susc._(qA)^2_"):
    """ Return
        susc arr (with x, y, z)
        susc err arr (with x, y, z)
        susc mean (with averaged x, y, z)
        susc err (with averaged x, y, z)
    """
    from read_data.construct_observables.read_diamagnetic_susceptibility \
        import convert_A2_to_units_of_susceptibility
    from read_data.read_calc_data import cut_off_ridiculous_values_v3
    import read_data.auto_convergence_detection as conv_det
    import read_data.utilities.sem2 as sem2


    statinfo_mat, statinfo_dict = read_calc_data.read_all_observables(
        init_setup, "/MAGN/statinfo", "statmagn.h5")
    trotter = statinfo_dict["M"]
    tau = statinfo_dict["tau"]
    beta = trotter * tau

    # shape: variate1, variate2, dimensions
    shape = (statinfo_mat.shape[0] + 1, statinfo_mat.shape[1], 3)
    # "crd" = coordinate x,y,z
    susc_by_crd = np.zeros(shape, dtype=float)
    susc_by_crd_err = np.zeros(shape, dtype=float)
    susc = np.zeros(shape[:2], dtype=float)     # averaged over coodrinate
    susc_err = np.zeros(shape[:2], dtype=float)
    blocks_by_crd = []

    # There are two methods to average xyz, where 1. is technically correct
    #   1. first average block data array to float, then average 3 coordinate-wise floats
    #   2. first average 3 coordinate-wise block arrays to single array, then average it to float
    #      Method 2 calculates convergence point S from susceptibility, not from energy Vt
    #      TODO TEST. Difference is very small, does it really matter which one to use?
    first_mean_blocks_then_mean_xyz = True  # True is method 1.

    if init_setup.use_cache:
        try:
            attrs = read_calc_data.read_cached_observables(init_setup, base + "avg",
                                                           'statmagn.h5', fallback=False)
            susc_by_crd = attrs["susc_by_crd_ext"]
            susc_by_crd_err = attrs["susc_by_crd_err_ext"]
            susc = attrs["susc_ext"]
            susc_err = attrs["susc_err_ext"]
            if not init_setup.extrapolate_range1:
                susc_by_crd = susc_by_crd[1:,:,:]
                susc_by_crd_err = susc_by_crd_err[1:,:,:]
                susc = susc[1:,:]
                susc_err = susc_err[1:,:]
            return susc_by_crd, susc_by_crd_err, susc, susc_err
        except ValueError:  # No cached values available (yes, exceptions as flow control)
            pass # Continue without using cached values

    for crd_idx, crd in enumerate(["x", "y", "z"]):
        error = None
        try:
            # Area squared, this is used for susceptibility calculations
            A2_mat, A2_dict = read_calc_data.read_all_observables(
                init_setup,
                base + str(crd_idx+1),
                "statmagn.h5"
            )
            # try:  # Sanity check that new and old obs are not mixed
            #     _, _ = read_calc_data.read_all_observables(
            #         init_setup,
            #         "/MAGN/diamagn_susceptibility_A^2_" + str(crd_idx+1),
            #         "statmagn.h5"
            #     )
            #     assert(False) # Data contains both new and old observables! Which one to use?
            # except ValueError:
            #     pass
        except ValueError as e:
            error = e
        if error is not None:
            # Older naming convention
            if base == "/MAGN/dia.susc._(qA)^2_":
                try:
                    A2_mat, A2_dict = read_calc_data.read_all_observables(
                        init_setup,
                        "/MAGN/diamagn_susceptibility_A^2_" + str(crd_idx+1),
                        "statmagn.h5"
                    )
                except ValueError:
                    raise error
            else:
                raise error
        blocks_by_crd.append(A2_mat)

        area_squared = A2_dict["mean"]
        area_squared_err = A2_dict["err"]

        # Magnetic susceptibility is - A^2 * e^2 /(B * c^2 * h_bar^2)
        # Note that in atomic units most of natural constants are 1.0
        chi = convert_A2_to_units_of_susceptibility(area_squared, beta, unit, trotter)
        # Diamagnetic susceptibility is always negative, but error is positive, so then minus
        chi_err = -convert_A2_to_units_of_susceptibility(area_squared_err, beta, unit)

        chi_ext, chi_err_ext = extrapolate_range1_at_0(init_setup, chi, chi_err)
        susc_by_crd[:, :, crd_idx] = chi_ext
        susc_by_crd_err[:, :, crd_idx] = chi_err_ext

    if first_mean_blocks_then_mean_xyz:
        # Average susceptibility over individual axial x y z values
        susc = susc_by_crd[:,:,:].mean(axis=2)
        # Reduce errorbar accordingly
        # I checked this error summation rule on paper, though it is quite obvious. Here 3=len([x,y,z])
        susc_err = 1/3 * np.sqrt((susc_by_crd_err[:,:,:]**2).sum(axis=2))
    else:
        assert(len(blocks_by_crd) == 3)
        for i in range(blocks_by_crd[0].shape[0]):
            for j in range(blocks_by_crd[0].shape[1]):
                if blocks_by_crd[0][i,j] is None:
                    susc[i,j] = float("nan")
                    susc_err[i,j] = float("nan")
                    continue
                crd_avg = (blocks_by_crd[0][i,j] + blocks_by_crd[1][i,j] + blocks_by_crd[2][i,j]) / 3
                S = conv_det.calculate_converged_point(crd_avg)
                _, dat, drp = cut_off_ridiculous_values_v3(crd_avg[S-1:])
                if drp is not None:
                    print("Dropped out invalid magnetic blocks.", "i,j:", i , j, "Blocks:", drp)
                mean, err = sem2.sem2(dat)
                # Magnetic susceptibility is - A^2 * e^2 /(B * c^2 * h_bar^2)
                # Note that in atomic units most of natural constants are 1.0
                chi = convert_A2_to_units_of_susceptibility(mean, beta[i,j], unit, trotter[i,j])
                # Diamagnetic susceptibility is always negative, but error is positive, so minus
                chi_err = -convert_A2_to_units_of_susceptibility(err, beta[i,j], unit)
                susc[i,j] = chi
                susc_err[i,j] = chi_err

    # Save mean values to cache
    A2_dict["mean"] = "Nope, A^2 mean of x-coordinate deleted on purpose, use 'susc' to get susceptibility."
    A2_dict["err"] = "Nope, A^2 err of x-coordinate deleted on purpose, use 'susc_err' to get susceptibility error."
    A2_dict["susc_by_crd_ext"] = susc_by_crd
    A2_dict["susc_by_crd_err_ext"] = susc_by_crd_err
    A2_dict["susc_ext"] = susc
    A2_dict["susc_err_ext"] = susc_err
    read_calc_data.save_obs_to_cache(init_setup, A2_dict, "statmagn.h5", base + "avg")

    if init_setup.extrapolate_range1:
        return susc_by_crd, susc_by_crd_err, susc, susc_err
    else:
        return susc_by_crd[1:,:,:], susc_by_crd_err[1:,:,:], susc[1:,:], susc_err[1:,:]


def plot_susceptibility(init_setup, susc_avg, susc_err_avg, unit, figname_postfix="", unite=False, ref_vals=True):
    """
    :param unite:  (ugly hack) Only usable in calc_comb. Unite curves to already plotted susc figure
    """
    figname = "susceptibility"
    assert(not (unite and init_setup.fig_ax is None))
    if not unite:
        figname = figname + figname_postfix
    if init_setup.fig_ax is not None:
        figname = figname + "_combination"

    # Hacky part combining multiple calculations, (you can safely assume that if branch is taken)
    if (init_setup.fig_ax is None) or (figname not in init_setup.fig_ax):
        fig, ax = plt.subplots(1, 1, num=init_setup.parent_dir_name + " " + figname, figsize=(4.0, 3.5))
        if init_setup.fig_ax is not None:
            init_setup.fig_ax[figname] = fig, ax
    else:
        fig, ax = init_setup.fig_ax[figname]

    labels = {"SI": "$\\chi \\; (\\frac{\\mathrm{m}^3}{\\mathrm{mol}})$",
              "cgs": "$\\chi_{\\mathrm{cgs}} \\; (\\frac{\\mathrm{cm}^3}{\\mathrm{mol}})$",
              "au": "$\\chi_{\\mathrm{a.u.}} \\; (a_0^3)$"}
    if unit not in labels:
        raise ValueError("Invalid unit, it must be one of: 'SI', 'cgs', 'au'")
    ylabel = labels[unit]

    plot_ob_as_function_of_range2(init_setup, ax, susc_avg, susc_err_avg,
                                  ylabel=ylabel, ref_vals=ref_vals)

    fig.tight_layout()
    fig.savefig("results/" + figname + ".pdf")


def print_susceptibility(init_setup, susc_avg, susc_err_avg, unit,
                         susc_by_crd=None, susc_err_by_crd=None,
                         title="", overwrite=True):
    from plot_data.plot_paircorrelation import print_and_write_matrix

    def system_is_uniform(susc_mean, mgn_susc):
        c = True
        for crd_idx, crd in enumerate(["x", "y", "z"]):
            c = c and np.all(np.abs(susc_mean - mgn_susc[:,:,crd_idx]) < 2 * 2 * susc_err_by_crd[:, :, crd_idx])
        return c

    write_file = "results/magnetic_susceptibility.txt"
    if overwrite:
        open_mode = "w"
    else:
        open_mode = "a"

    # Clear output file
    with open(write_file, open_mode) as f:
        import datetime
        import socket
        date = datetime.datetime.now().replace(microsecond=0).isoformat()
        host = socket.gethostname()
        line = "{} {}\n\n\n".format(host, date)
        f.write(line)
        print("\n\n\n")

    labels = {"SI": "SI (m^3/mol)",
              "cgs": "cgs (cm^3/mol)",
              "au": "a.u. (a0^3)"}
    if unit not in labels:
        raise ValueError("Invalid unit, it must be one of: 'si', 'cgs', 'au'")
    unit_label = labels[unit]

    with open(write_file, 'a') as f:
        line = "Diamagnetic susceptibility {}, unit: {}".format(title, unit_label)
        f.write(line + "\n")
        print(line)

    if susc_by_crd is None or system_is_uniform(susc_avg, susc_by_crd):
        print_and_write_matrix(init_setup, susc_avg, susc_err_avg,
                               title="uniform mean over x,y,z",
                               write_file=write_file, identation="        ")
    else:
        with open(write_file, 'a') as f:
            line = "The system seems not to be rotationally uniform. Printing all susceptibilities componentwise."
            f.write(line + "\n")
            print(line)

        cut_lines = [0] + list(range(-susc_by_crd.shape[0]-1, 0, 1))
        # This first one just prints table column names
        print_and_write_matrix(init_setup, susc_by_crd[:, :, 0],
                               err=susc_err_by_crd[:, :, 0], title="asdf",
                               write_file=write_file, cut_lines=cut_lines,
                               identation="        ")
        # Print x y z directional susceptibility rows
        for crd_idx, crd in enumerate(["x", "y", "z"]):
            title1 = "    {}-direction:".format(crd)
            print_and_write_matrix(init_setup, susc_by_crd[:, :, crd_idx],
                                   err=susc_err_by_crd[:, :, crd_idx], title=title1,
                                   write_file=write_file, cut_lines=[1, 2, -1],
                                   identation="        ")
        title2 = "    uniform mean:"
        print_and_write_matrix(init_setup, susc_avg, susc_err_avg, title=title2,
                               write_file=write_file, cut_lines=[1, 2],
                               identation="        ")
