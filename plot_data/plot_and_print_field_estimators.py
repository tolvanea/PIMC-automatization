import datetime
import socket

import numpy as np
import matplotlib.pyplot as plt

from read_data import read_calc_data
from plot_data.print_data import print_and_write_matrix
from plot_data.utils import plot_ob_as_function_of_range2


def read_field_estimator(
    init_setup,
    unit_conversion,        # e.g. lambda tau trotter: 0.1      (function to do unit conversion)
    hdf5file,               # e.g. "statpol.h5"         (hdf5 file name)
    statinfo,               # e.g. "/POL/statinfo"      (read tau and trotter from here)
    base,                   # e.g. "/POL/mu2_",         (hdf5 dataset name without coodrinates)
    components,             # e.g. ["1", "2", "3"]      (appended after 'base'. Mean is taken from these.)
    name,                   # e.g. "pol"                (name used to store in internal cache,
    #                                                    can be whatever, BUT MAKE SURE IT'S UNIQUE!!)
):
    """ Return
        mean by crd, array with size [variate_1, variate_2, 3], where 3 is for with x, y, z components
        err by crd,  array with size [variate_1, variate_2, 3], where 3 is for with x, y, z components
        mean,        array with size [variate_1, variate_2], where averaged x, y, z
        err,         array with size [variate_1, variate_2], where averaged x, y, z
    """
    statinfo_mat, statinfo_dict = read_calc_data.read_all_observables(
        init_setup, statinfo, hdf5file)
    trotter = statinfo_dict["M"]
    tau = statinfo_dict["tau"]

    # shape: variate1, variate2, dimensions
    shape = (statinfo_mat.shape[0] + 1, statinfo_mat.shape[1], len(components))
    # "crd" = e.g. coordinates 1,2,3 (x,y,z), or 11, 22, 33 (xx, yy, zz)
    data_by_crd = np.zeros(shape, dtype=float)
    data_by_crd_err = np.zeros(shape, dtype=float)
    data = np.zeros(shape[:2], dtype=float)     # averaged over coodrinate
    data_err = np.zeros(shape[:2], dtype=float)

    if init_setup.use_cache:
        try:
            attrs = read_calc_data.read_cached_observables(
                init_setup, name, hdf5file, fallback=False
            )
            # 'ext' means extrapolation at tau=0, which is applied by default on cached data
            data_by_crd = attrs[name+"_by_crd_ext"]
            data_by_crd_err = attrs[name+"_by_crd_err_ext"]
            data = attrs[name+"_ext"]
            data_err = attrs[name+"_err_ext"]
            if not init_setup.extrapolate_range1:
                data_by_crd = data_by_crd[1:,:,:]
                data_by_crd_err = data_by_crd_err[1:,:,:]
                data = data[1:,:]
                data_err = data_err[1:,:]

            return data_by_crd, data_by_crd_err, data, data_err # Note that control flow ends here
        except ValueError:  # No cached values available (yes, exceptions as flow control)
            pass

    # Cached average data was not available, read the data from hdf5 files instead

    for crd_idx, crd in enumerate(components):
        # Read the data from hdf5
        data_mat, data_dict_crd = read_calc_data.read_all_observables(
            init_setup,
            base + crd,
            hdf5file
        )

        # Extrapolate at tau=0
        ext_mean, ext_err = read_calc_data.extrapolate_range1_at_0(
            init_setup,
            unit_conversion(tau, trotter) * data_dict_crd["mean"],
            np.abs(unit_conversion(tau, trotter)) * data_dict_crd["err"],
        )

        data_by_crd[:, :, crd_idx], data_by_crd_err[:, :, crd_idx] = ext_mean, ext_err

    # Reuse the most recent value from the loop
    data_dict = data_dict_crd


    # Average data over 'coordinates', such as x, y, z
    data = data_by_crd[:,:,:].mean(axis=2)
    # Reduce errorbar accordingly
    # I checked this error summation rule on paper, though it is quite obvious.
    N = len(components)
    data_err = 1/N * np.sqrt((data_by_crd_err[:,:,:]**2).sum(axis=2))

    # Save mean values to cache
    data_dict["mean"] = f"Deleted on purpose, use '{name}_by_crd_ext' or '{name}_ext'."
    data_dict["err"] = f"Deleted on purpose, use '{name}_by_crd_err_ext' or '{name}_err_ext'"
    data_dict[f"{name}_by_crd_ext"] = data_by_crd
    data_dict[f"{name}_by_crd_err_ext"] = data_by_crd_err
    data_dict[f"{name}_ext"] = data
    data_dict[f"{name}_err_ext"] = data_err
    read_calc_data.save_obs_to_cache(init_setup, data_dict, hdf5file, name)

    if init_setup.extrapolate_range1:  # We cached extrapolation even though it was not asked.
        return data_by_crd, data_by_crd_err, data, data_err
    else:
        return data_by_crd[1:,:,:], data_by_crd_err[1:,:,:], data[1:,:], data_err[1:,:]


def plot_field_estimator(
    init_setup,
    data_avg,
    data_err_avg,
    figname,            # e.g. "polarizability"
    symbol,             # e.g. "\\mu"                       (used in latex formatting)
    unit,               # e.g. "(\\mathrm{units here})"     (used in latex formatting)
    ref_vals=True       # Read and plot reference values from init_setup
):
    fig, ax = plt.subplots(1, 1, num=init_setup.parent_dir_name + " " + figname, figsize=(4.0, 3.5))

    ylabel = f"${symbol} \\; {unit}$"
    plot_ob_as_function_of_range2(init_setup, ax, data_avg, data_err_avg,
                                  ylabel=ylabel, ref_vals=ref_vals)

    fig.tight_layout()
    fig.savefig("results/" + figname + ".pdf")


def print_field_estimator(
    init_setup,
    data_avg,
    data_err_avg,
    data_by_crd,        # Printed in case data seem not to be uniform
    data_err_by_crd,    # Printed in case data seem not to be uniform
    components,         # e.g. ["1", "2", "3"],
    unit,               # e.g. "m/s",                           (printed in tables NOT with latex formatting)
    write_file,         # e.g. "electric_polarizability.txt"
    title,              # e.g. "Static electric polarizability", can be whatever
    overwrite           # e.g. True                             (if false, appended tables to the file)
):
    def system_is_uniform(data_mean, data_crd):
        c = True
        for crd_idx in range(len(components)):
            c = c and np.all(np.abs(data_mean - data_crd[:,:,crd_idx]) < 2 * 2 * data_err_by_crd[:, :, crd_idx])
        return c

    if overwrite:
        open_mode = "w"
    else:
        open_mode = "a"

    write_file_path = f"results/{write_file}"
    if overwrite: # Clear output file, and write date
        with open(write_file_path, open_mode) as f:
            date = datetime.datetime.now().replace(microsecond=0).isoformat()
            host = socket.gethostname()
            line = "{} {}\n\n\n".format(host, date)
            f.write(line)
            print("\n\n\n")


    with open(write_file_path, 'a') as f:
        line = "{}, unit: {}".format(title, unit)
        f.write(line + "\n")
        print(line)

    if data_by_crd is None or system_is_uniform(data_avg, data_by_crd):
        print_and_write_matrix(init_setup, data_avg, data_err_avg,
                               title=f"uniform mean over {components}",
                               write_file=write_file_path, identation="        ")
    else:
        with open(write_file_path, 'a') as f:
            line = "The system seems not to be rotationally uniform. Printing data componentwise."
            f.write(line + "\n")
            print(line)

        cut_lines = [0] + list(range(-data_by_crd.shape[0]-1, 0, 1))
        # This first one just prints table column names
        print_and_write_matrix(init_setup, data_by_crd[:, :, 0],
                               err=data_err_by_crd[:, :, 0], title="asdf",
                               write_file=write_file_path, cut_lines=cut_lines,
                               identation="        ")
        # Print x y z directional susceptibility rows
        for crd_idx, crd in enumerate(components):
            title1 = "    {}-direction:".format(crd)
            print_and_write_matrix(init_setup, data_by_crd[:, :, crd_idx],
                                   err=data_err_by_crd[:, :, crd_idx], title=title1,
                                   write_file=write_file_path, cut_lines=[1, 2, -1],
                                   identation="        ")
        title2 = "    uniform mean:"
        print_and_write_matrix(init_setup, data_avg, data_err_avg, title=title2,
                               write_file=write_file_path, cut_lines=[1, 2],
                               identation="        ")



###############
# Practical use of the functions above:
# First read data, then plot it, then print it to console/file
###############




# Read, plot and print magnetic susceptibility (i.e. dipole magnetizability)
#
# This function is not currently used, but it is rewritten version of the
# 'plot_magnetic_susceptibility.py' file, but this function utilizes the
# same functions as electric polarizability.
# TODO Enable this function, and remove the 'plot_magnetic_susceptibility.py'.
#      Maybe I want to grap some susceptibility component calculations from there?
def magnetic_susceptibility(init_setup):

    use_au_units = False

    def unit_conversion(tau, trotter):
        if use_au_units: # a.u. units
            l = 1.0  # length
            e = 1.0  # charge
            β = tau * trotter
            ℏ2 = 1.0**2  # planks constant
            c = 137.035999084 # speed_of_light
            µ = 4*np.pi / c**2  # magnetic permeability of vacuum or something
            optional_avogadro = 1.0
        else: # SI units
            l = 5.29177210903e-11  # length
            e = 1.602176634e-19   # charge
            hartree_energy = 4.3597447222071e-18
            β = tau * trotter / hartree_energy
            ℏ2 = 1.054571800e-34**2
            µ = 4*np.pi * 1e-7 # magnetic permeability of vacuum or something
            optional_avogadro = 6.02214076e23
        return - µ * (e**2) * ((l**4) / (β * ℏ2)) * optional_avogadro

    if use_au_units:
        unit = r"(a_0^3)"
    else:
        unit = "\\frac{\\mathrm{m}^3}{\\mathrm{mol}}"

    symbol = r"\chi"
    components = ["1", "2", "3"]
    title = "magnetic_susceptibility"

    data_by_crd, data_err_by_crd, data_avg, data_err_avg = read_field_estimator(
        init_setup,
        unit_conversion,
        hdf5file = "statmagn.h5",
        statinfo = "/MAGN/statinfo",
        base = "/MAGN/dia.susc._(qA)^2_",
        components = components,
        name = "magnetic_susceptibility",
    )
    plot_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        figname = title,
        symbol = symbol,
        unit = unit,
        ref_vals=True,
    )
    print_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        data_by_crd,
        data_err_by_crd,
        components = components,
        unit = unit,
        write_file = title + ".txt",
        title = title,
        overwrite=True,
    )

# Read, plot and print electric dipole polarizability (i.e. mu_xx)
def electric_dipole_polarizability(init_setup, overwrite=True):

    def unit_conversion(tau, trotter):
        beta = tau * trotter
        return beta

    symbol = r"\alpha_{\alpha\alpha}"
    unit = r"(\mathrm{e^2 a_0^2 E_h^{-1}})"
    components = ["11", "22", "33"]
    title = "electric dipole polarizability"

    data_by_crd, data_err_by_crd, data_avg, data_err_avg = read_field_estimator(
        init_setup,
        unit_conversion,
        hdf5file = "statpol.h5",
        statinfo = "/POL/statinfo",
        base = "/POL/mu2_",             # Not to be confused with 4chan /pol/
        components = components,
        name = title.replace(" ", "_"),
    )
    plot_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        figname = title,
        symbol = symbol,
        unit = unit,
        ref_vals=True,
    )
    print_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        data_by_crd,
        data_err_by_crd,
        components = components,
        unit = unit,
        write_file = "polarizability.txt",
        title = title,
        overwrite=overwrite,
    )


# Read, plot and print electric polarizability cross terms (i.e. mu_xy)
def electric_dipole_polarizability_cross_terms(init_setup, overwrite=False):

    def unit_conversion(tau, trotter):
        beta = tau * trotter
        return beta

    symbol = r"\alpha_{\alpha\beta}"
    unit = r"(\mathrm{e^2 a_0^2 E_h^{-1}})"
    components = ["12", "13", "23"]
    title = "electric dipole polarizability cross term"

    data_by_crd, data_err_by_crd, data_avg, data_err_avg = read_field_estimator(
        init_setup,
        unit_conversion,
        hdf5file = "statpol.h5",
        statinfo = "/POL/statinfo",
        base = "/POL/mu2_",
        components = components,
        name = title.replace(" ", "_"),
    )
    plot_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        figname = title,
        symbol = symbol,
        unit = unit,
        ref_vals=True,
    )
    print_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        data_by_crd,
        data_err_by_crd,
        components = components,
        unit = unit,
        write_file = "polarizability.txt",
        title = title,
        overwrite=overwrite,
    )

# Read, plot and print electric first hyper polarizability (i.e. mu_xxx)
def electric_first_hyper_polarizability(init_setup, overwrite=False):

    def unit_conversion(tau, trotter):
        beta = tau * trotter
        return beta

    symbol = r"\beta_{\alpha\alpha\alpha}"
    unit = r"(\mathrm{e^3 a_0^3 E_h^{-2}})"
    components = ["111", "222", "333"]
    title = "electric 1st hyper polarizability"

    data_by_crd, data_err_by_crd, data_avg, data_err_avg = read_field_estimator(
        init_setup,
        unit_conversion,
        hdf5file = "statpol.h5",
        statinfo = "/POL/statinfo",
        base = "/POL/mu3_",
        components = components,
        name = title.replace(" ", "_"),
    )
    plot_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        figname = title,
        symbol = symbol,
        unit = unit,
        ref_vals=True,
    )
    print_field_estimator(
        init_setup,
        data_avg,
        data_err_avg,
        data_by_crd,
        data_err_by_crd,
        components = components,
        unit = unit,
        write_file = "polarizability.txt",
        title = title,
        overwrite=overwrite,
    )
