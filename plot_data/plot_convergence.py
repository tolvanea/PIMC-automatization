import os
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt


#from . import plot_dens
#import plotting_1
import read_data.read_calc_data as read_calc_data
import read_data.auto_convergence_detection as conv_det
from read_data.read_calc_data import cut_off_ridiculous_values_v3



def plot_convergence_for_obs(init_setup, obs_list, filename=None, average_obs_list=False):
    """Plots multiple figures about observables in different blocks.

    :param obs_list:            e.g. ["/E/Et", "/E/Vt"], must contain two values
    :param filename:            For example "energy.h5" or "statmagn.h5". (Default "energy.h5")
    :param average_obs_list:    Calculate average, for example if obs_list=[O_x, O_y, O_z] -> [<O>]
    """
    # if init_setup.use_cache:
    #     raise ValueError("Cached values does not support convergence. Leave out either\n"
    #                      "flag '--susc_conv' or flag '--cache'")
    # HACK: Do not plot convernce if combination of many different calculations,
    if init_setup.fig_ax is not None:
        return

    # flatten axes- tuple
    datas = []
    means = []
    errs = []
    Ss = []
    obs = obs_list
    for k, ob in enumerate(obs):
        data_arr, attr_arr = read_calc_data.read_all_observables(init_setup, ob, filename)
        datas.append(data_arr)
        means.append(attr_arr["mean"])
        errs.append(attr_arr["err"])
        Ss.append(attr_arr["S"])

    def common_leading_substring(names):
            """ Finds longest common leading substring from string list 'names'. """
            for i in range(len(names[0])):
                for j in range(1, len(names)):
                    if len(names[j]) <= i or names[0][i] != names[j][i]:
                        return names[0][:i]
            return names[0]

    # Reduce many observables to single value, so rather than plotting many convergence plots
    # Plot only one, the average value. Used in averaging susceptibility x,y and z coordinates
    if average_obs_list:
        assert(datas[0].shape == datas[1].shape)
        datas_mean = np.full(datas[0].shape, fill_value=None)
        means_mean = np.full(datas[0].shape, fill_value=None)
        errs_mean = np.full(datas[0].shape, fill_value=None)
        Ss_max = np.full(datas[0].shape, fill_value=None)
        for i in range(datas[0].shape[0]):
            for j in range(datas[0].shape[1]):
                if datas[0][i,j] is None:
                    continue
                datas_mean[i,j] = np.zeros_like(datas[0][i,j])
                means_mean[i,j] = 0.0
                errs_mean[i,j] = 0.0
                Ss_max[i,j] = 0
                for data, mean, err, S in zip(datas, means, errs, Ss):
                    datas_mean[i,j] += data[i,j] / len(datas)
                    means_mean[i,j] += mean[i,j] / len(datas)
                    errs_mean[i,j] += err[i,j]**2  # Error summation/cumulation is not just average
                    Ss_max[i,j] = max(S[i,j], Ss_max[i,j])
                errs_mean[i,j] = 1/len(datas) * np.sqrt(errs_mean[i,j])
        datas = [datas_mean]
        means = [means_mean]
        errs = [errs_mean]
        Ss = [Ss_max]
        obs = [common_leading_substring(obs) + " mean"]

    m = datas[0].shape[0]
    n = datas[0].shape[1]

    # Print all blockdata figures. Two columns: [Et, Vt]. Three rows, [tau=...]
    # plot as many figures as required for taus.

    num_rows = 3
    num_figs = (n-1)//num_rows + 1
    num_columns = len(obs)

    for fig_itr in range(num_figs):
        s1 = init_setup.latex_symbol1.replace("\\","")
        s2 = init_setup.latex_symbol2.replace("\\","")
        r1 = init_setup.range1
        r2 = init_setup.range2
        u1 = init_setup.latex_unit1
        u2 = init_setup.latex_unit2

        if init_setup.plot_only_range1_idx is not None:
            idx = init_setup.plot_only_range1_idx
            # Append this string to figure name to indicate that only one value is plotted
            conv_str = "_{}={}{}".format(s1, r1[idx], u1)
        else:
            conv_str = ""

        fig_name = "{}_{}={:.2f}-{:.2f}{}{}.pdf".format(
                "-".join(map(lambda s: "∕".join(s.split("/")[-2:]), obs)),  # take part after '/' from datasets
                s2, r2[fig_itr*num_rows], r2[min((fig_itr+1) * num_rows - 1, n - 1)], u2,
                conv_str
        )
        fig1, axes = plt.subplots(num_rows, num_columns,
                                  num=fig_name,
                                  figsize=(9, 12))  # =(4.4,3.2))

        for row_itr in range(num_rows):

            for col_itr in range(num_columns):

                obs_str = obs[col_itr]
                if num_columns != 1:
                    ax = axes[row_itr, col_itr]
                else:
                    ax = axes[row_itr]

                # iteratre var_val 2
                j = fig_itr * num_rows + row_itr
                if j >= n:
                    break

                for line_itr in reversed(range(m)):
                    if datas[col_itr][line_itr, j] is None:
                        continue
                    elif (init_setup.plot_only_range1_idx is not None) \
                            and (init_setup.plot_only_range1_idx != line_itr):
                        continue
                    if len(init_setup.latex_symbol1) > 2 and "\\" not in init_setup.latex_symbol1:
                        # latex_symbol1 is not latex symbol
                        label = "{}: {}".format(init_setup.latex_symbol1,
                                                 init_setup.range1[line_itr])
                    else:
                        label = "${} = {} {}$".format(init_setup.latex_symbol1,
                                                      init_setup.range1[line_itr],
                                                      init_setup.latex_unit1)

                    data = datas[col_itr][line_itr, j]
                    mean = means[col_itr][line_itr, j]
                    err = errs[col_itr][line_itr, j]
                    S = Ss[col_itr][line_itr, j]

                    if init_setup.extrapolate_range1:
                        color_idx = line_itr + 1
                    else:
                        color_idx = line_itr

                    plot_converged_data(ax, data, mean, err, S, color_idx, label=label)

                ax.set_title(obs_str + ', ' + init_setup.dir2_labels[j])
                ax.legend()

        fig1.tight_layout()
        if not os.path.isdir("results/convergence"):
            os.mkdir("results/convergence")
        fig1.savefig("results/convergence/" + fig_name)


def plot_Rave(init_setup):
    pl = init_setup.prt_labels
    pc = init_setup.prt_counts
    pairs = []
    for i in range(len(pl)):
        for j in range(len(pl)):
            if i != j or pc[i] > 1:
                pairs.append("".join(["/PC", "/", pl[i], "_", pl[j], "/Rave"]))
    for pair in pairs:
        try:
            plot_convergence_for_obs(init_setup, [pair], filename="paircorrelation.h5")
        except ValueError:  # pair does not exist -> do nothing
            pass

def plot_converged_data(ax, data, mean, err, S, i, label=""):
    """Plot valid and invalid part, also plot mean level, and moving average.
    Take also data dropping in viral energy into account by cropping top 5%
    largest and smallest values."""

    valid_data = data[S-1:]  # cut convergence trail
    x_valid, y_valid, drp = cut_off_ridiculous_values_v3(valid_data)  # cut off improper values
    if drp is not None:
        print("Dropped out invalid blocks from convergence data. i:", i, "Blocks:", drp,)
    x_valid += (S-1)

    def find_missing_values(full, partial):
        """Compare two arrays and find values that are missing from other one.
        After note: Why on earth I have made such complex function if that
        information can be obtained much more easily directly from cutoff
        function itself?"""
        misses = np.full(len(full)-len(partial), dtype=full.dtype, fill_value=np.nan)
        itr1 = -1   # full itr
        itr2 = 0    # parial itr

        while itr1 < len(full)-1:
            itr1 += 1
            if itr2 < len(partial) and full[itr1] == partial[itr2]:
                itr2 += 1
            else:
                misses[itr1-itr2] = full[itr1]
        assert itr1-itr2 != len(full)-len(partial)
        assert itr2 != len(partial)-1
        assert (not np.isnan(misses).any())
        return misses

    x_miss = find_missing_values(np.arange(S - 1, len(data)), x_valid)
    y_miss = data[x_miss]
    x_invalid = np.hstack((np.arange(0, S-1), x_miss))
    x_invalid += 1  # Block indexing starts from 1
    x_valid += 1
    y_invalid = np.hstack((data[0:S-1], y_miss))
    first_calc = len(ax.get_lines()) == 0  # first time something is drawn to empty axis

    alpha = 0.2
    ax.plot(x_valid, y_valid, color="C" + str(i), label=label, zorder=0, alpha=alpha)

    ax.plot(x_invalid, y_invalid, color="C" + str(i), linestyle="", marker=".", zorder=0, alpha=alpha/2)

    x = np.concatenate((np.arange(x_valid[0]), x_valid), axis=0)
    y = np.concatenate((data[:S], y_valid), axis=0)
    plot_mean_level(ax, x, y, mean, err, S, i, first_calc)




def plot_mean_level(ax, x_data, y_data, mean, err, S, i, first_calc):
    """
    Mean of average curves may not seem to be in exactly same mean
    which is calculated elsewhere. This is property of average windows,
    not a bug. Average windows "sum" center values much more than either
    ends. Therefore center values have more weight.
    windows
    """
    from plot_data.utils import brighten
    N = len(y_data)
    #full_x = np.arange(N)
    m = x_data[-1]
    ax.plot([-0.05*m, m], [mean, mean], color="C{}".format(i), zorder=4)
    # 2-sigma errorbars
    ax.plot([-0.05*m, m], [mean - 2*err, mean - 2*err], color="C{}".format(i), linestyle=":", zorder=4)
    ax.plot([-0.05*m, m], [mean + 2*err, mean + 2*err], color="C{}".format(i), linestyle=":", zorder=4)
    ax.plot([S-1, S-1], [mean + 2*err, mean - 2*err], color="C{}".format(i), linestyle=":", zorder=4)
    ax.plot([N-1, N-1], [mean + 2*err, mean - 2*err], color="C{}".format(i), linestyle=":", zorder=4)


    color = matplotlib.colors.to_rgb("C{}".format(i))
    # moving average
    if len(x_data) <= 10:
        return
    if len(x_data) > 10 + S:
        movmean, idxs = conv_det.moving_average(y_data, 9)
        color_darker = brighten(color, -0.3)
        ax.plot(x_data[idxs][S:], movmean[S:], color=color_darker, zorder=1, linewidth=1, alpha=0.3)
        ax.plot(x_data[idxs][:S], movmean[:S], color=color_darker, zorder=1, linewidth=1, alpha=0.2, ls=":")
        ax.plot(
            [x_data[S + 5]], [movmean[S]], ls="", marker="|", markersize=18,
            color=color_darker, zorder=1
        )

    if len(x_data) > 100 + S:
        movmean, idxs = conv_det.moving_average(y_data, 99)
        color_darker = brighten(color, -0.5)
        ax.plot(x_data[idxs][S:], movmean[S:], color=color_darker, zorder=2, linewidth=2, alpha=0.4)
        ax.plot(x_data[idxs][:S], movmean[:S], color=color_darker, zorder=2, linewidth=2, alpha=0.3, ls=":")
        ax.plot(
            [x_data[S + 50]], [movmean[S]], ls="", marker="|", markersize=18,
            color=color_darker, zorder=2
        )

    if len(x_data) > 1000 + S:
        movmean, idxs = conv_det.moving_average(y_data, 999)
        color_darker = brighten(color, -0.7)
        ax.plot(x_data[idxs][S:], movmean[S:], color=color_darker, zorder=3, linewidth=3, alpha=0.5)
        ax.plot(x_data[idxs][:S], movmean[:S], color=color_darker, zorder=3, linewidth=3, alpha=0.4, ls=":")
        ax.plot(
            [x_data[S + 500]], [movmean[S]], ls="", marker="|", markersize=18,
            color=color_darker, zorder=3
        )

    if len(x_data) > 10000 + S:
        movmean, idxs = conv_det.moving_average(y_data, 9999)
        color_darker = brighten(color, -0.9)
        ax.plot(x_data[idxs][S:], movmean[S:], color=color_darker, zorder=4, linewidth=4, alpha=0.6)
        ax.plot(x_data[idxs][:S], movmean[:S], color=color_darker, zorder=4, linewidth=4, alpha=0.5, ls=":")
        ax.plot(
            [x_data[S + 5000]], [movmean[S]], ls="", marker="|", markersize=18,
            color=color_darker, zorder=4
        )

    ymin = np.min(movmean)
    ymax = np.max(movmean)
    m = np.abs(- ymin + ymax) * 0.5     # margins
    ymin -= m
    ymax += m
    if first_calc and movmean is not None:
        ax.set_ylim((ymin, ymax))
    if not first_calc:
        ylim_min, ylim_max = ax.get_ylim()
        ymin = min(ymin, ylim_min)
        ymax = max(ymax, ylim_max)
        ax.set_ylim((ymin, ymax))


