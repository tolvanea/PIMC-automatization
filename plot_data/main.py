import os
import sys
import pickle
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload

from start_PIMC.misc import add_default_values, check_that_this_is_run_in_correct_directory

import plot_data.plot_convergence
import plot_data.print_data
import plot_data.plot_energy as energy

import plot_data.plot_paircorrelation as pc
import plot_data.plot_magnetic_susceptibility as susc
import plot_data.plot_and_print_field_estimators as plot_and_print_field_estimators

from read_data.utilities.misc import get_obs_dir
from read_data.utilities.read_particles import particle_labels
from read_data.read_calc_data import read_single_HDF5_dataset, get_last_modification_times
import read_data.auto_convergence_detection as conv_det


help_text = """
Plots everything in PIMC-calcuation. (One can see general information about 
system.)

Plots
    - energy
    - pair correlation (if explicitly requested by "pc", see 'Usage')
        - pair correlation
        - mean distance
    - energy components (if explicitly requested by "comp", see 'Usage')

Usage:
        python3 -m plot_data.main [flags]
    
        
    Example: this is the one I use the most
        python3 -m plot_data.main --E_conv --susc_conv
    flags
                    Even without any flags, energy is always plotted 
        --pc        Pair correlation data of particle pairs (<r>, <r^2>, <1/r>)
        --pc_all    Pair correlation + distribution data
        --pc_conv   Convergence of particle distances averages <R> or "Rave"
        --E_conv    Energy convergence (energy per MC block that is)
        --comp      Energy components (interaction energy of nuclei/)
        --susc      Magnetic susceptibility
        --susc_conv Magnetic susceptibility + convergence
        --pol       Static electric polarizability (dipole & first hyper)
        --conv=N    Limit convergence plotting including only line range1 index = N  (0-indexing)
        
        --no_cache  Don't use cached values for quick plot. 
                    More about caching:
                    - By default, data is read from cache created on first run.
                    - If any of observables update, it triggers hdf5 re-read.
                    - Convergence-data is never cached.
                    - Note, if you tweak any of this plotting code,
                      THIS OPTION MUST BE ABSOLUTELY USED, because code 
                      changes do not trigger reload.

        --calc_comb THIS IS A HACK: Combine many individual calculation sets into
                    one figure. This effectively creates a third variating parameters,
                    because this code is hard-coded to variate only two simulation
                    parameters at one go. See source code comment in
                    'plot_data/main.py' for more information.

    ALSO NOTE: There exists a lot of more setting that can be enabled with
    configure_instance.py. The full list of them can be seen from source code in
    'start_PIMC/misc.py : add_default_values()'. I'm sorry from this poor design.
    Now half of all the settings can be set by command line flags above, and other half
    is set by configuration file.
"""
"""
    More about --calc_comb:
        [Behold! The level of "hacky" ugliness is high in this one:] What if you want to combine
        figures of *multiple* calculation sets? That is, combine these plotted curves with other
        curves from other calculations that are from other directories? That is, have a plot
        variating three values at once. Well, next command does exactly that. It goes one directory
        up, and runs this script for all calculations it finds there. Of course, all calculations
        must be variating same values (that is, same range2, latex_symbol2 and latex_unit2).
        Important stupid detail: Run this from the simulation that is alphabetically
        last in you folder. So if you combine two simulation-sets "AQ" and "BO", run
        it from "BO". Also, figures are saved correctly only in "BO".
        Another important detail: You must have 'latex_unit3', 'range3_val'
        and 'latex_symbol3' set up in configure_instance.py. See
        'start_PIMC/misc.py : add_default_values()' for more. Also, range1&2,
        latex_symbol1&2 and latex_unit1&2 must match between the calculations.
        Example:
            python3 -m plot_data.main --susc --calc_comb
"""


def main(args):

    obs_flags = argument_parsing(args)
    if "calculation combination mess" in obs_flags:  # ugly quick additional hack
        calculation_combining(obs_flags)
        return

    check_that_this_is_run_in_correct_directory()

    # Read initial parameters of calculation set (You must be in correct directory for this import)
    from sample import configure_instance # You must be in same folder with this py-file
    # Unlike name says, `init_setup` is not only initial setup information for simulation, but it
    # also contains useful info to read data, plot figures and such. This useful data contains
    # things like S and N information (i.e. valid block range) for each simulation.
    init_setup = configure_instance.InitialSetup()
    # If some options are not explicitly written, then add default values for those
    add_default_values(init_setup, obs_flags)
    # Add other useful infromation into init_setup
    add_calculation_specific_data(init_setup)

    # Print energy and information about failed blocks
    plot_data.print_data.print_energy_and_block_info(init_setup)

    plot_observables(init_setup, obs_flags)
    print("Ok, everything is processed now, plotting images. ctrl + c to close")
    plt.show()

def argument_parsing(args):
    obs_flags = []
    for arg in args[1:]:
        if arg == "--help" or arg == "help" or arg == "-h":
            print(help_text)
            exit(0)
        elif len(arg) > 2 and arg[:2] != "--":
            raise Exception("Use two dashes for flags (i.e. second argument), like: '--susc'")
        # Pair correlation
        elif arg == "--pc":
            obs_flags.append("pair correlation")
        # Pair correlation
        elif arg == "--pc_all":
            obs_flags.append("pair correlation all")
        # Block convergence of <r> for each pair
        elif arg == "--pc_conv":
            obs_flags.append("pair correlation convergence")
        # Energy components: kinetic, potential
        elif arg == "--comp":
            obs_flags.append("components")
        # Mangetic susceptibility
        elif arg == "--susc":
            obs_flags.append("magnetic susceptibility")
        elif arg == "--pol":
            obs_flags.append("electric polarizability")
        # Energy convergence plot
        elif arg == "--E_conv":
            obs_flags.append("energy convergence")
        # Mangetic susceptibility with convergence plot
        elif arg == "--susc_conv":
            obs_flags.append("susceptibility convergence")
        # Select only one convergence curve to be plotted
        elif len(arg) > 7 and arg[:7] == "--conv=":
            obs_flags.append(arg)
        # Hack: variate three different values by combining with calculations from parent directory
        elif arg == "--calc_comb":
            obs_flags.append("calculation combination mess")
        elif arg == "--no_cache":
            obs_flags.append("No cached values")
        else:
            raise Exception("Unknown argument {}, see valid arguments with --help.".format(args[1]))
    return obs_flags

def plot_observables(init_setup, obs_flags):
    # Ugly spaghetti to plot only one wanted convergence curve
    for flag in obs_flags:
        if len(flag) > 7 and flag[:7] == "--conv=":
            init_setup.plot_only_range1_idx = int(flag[7:])
            break

    # Combine with calculations from parent directory

    # Plot always energy
    energy.plot_energy_Vt(init_setup)

    if "pair correlation" in obs_flags:
        pc.plot_and_print_PC(init_setup)
    if "pair correlation all" in obs_flags:
        pc.plot_and_print_PC(init_setup, plot_pc_distribution=True)
    if "components" in obs_flags:
        energy.plot_energy_components(init_setup)
    if "energy convergence" in obs_flags:
        plot_data.plot_convergence.plot_convergence_for_obs(init_setup, ["/E/Et", "/E/Vt"],
                                                    filename="energy.h5")
    if "pair correlation convergence" in obs_flags:
        plot_data.plot_convergence.plot_Rave(init_setup)
    if ("magnetic susceptibility" in obs_flags) or ("susceptibility convergence" in obs_flags):
        susc.plot_and_print_susceptibility(init_setup)
        # The above call is rather ugly legacy, and does the same as
        #   plot_and_print_field_estimators.magnetic_susceptibility(init_setup)
        # but it handles also special extra functions


        # Following block gives you per-block information from susceptibility
        if ("susceptibility convergence" in obs_flags):
            failed = False
            try:
                make_list = lambda s: [s + str(i+1) for i in range(3)]
                plot_data.plot_convergence.plot_convergence_for_obs(
                    init_setup,
                    make_list("/MAGN/dia.susc._(qA)^2_"),
                    filename="statmagn.h5",
                    average_obs_list=True,
                )
            except ValueError as e:  # Old naming
                try:
                    plot_data.plot_convergence.plot_convergence_for_obs(
                        init_setup,
                        ["/MAGN/diamagn_susceptibility_A^2_1",
                         "/MAGN/diamagn_susceptibility_A^2_2",
                         "/MAGN/diamagn_susceptibility_A^2_3"],
                        filename="statmagn.h5",
                        average_obs_list=True,
                    )
                    failed = True
                except ValueError:
                    raise e
            if not failed and init_setup.partial_susceptibilites:
                from plot_data.plot_magnetic_susceptibility import partial_susc_datasetnames
                for dset, label, important in partial_susc_datasetnames(init_setup):
                    if not important:
                        continue
                    plot_data.plot_convergence.plot_convergence_for_obs(
                        init_setup,
                        make_list(dset),
                        filename="statmagn.h5",
                        average_obs_list=True,
                    )
    if ("electric polarizability" in obs_flags):
        plot_and_print_field_estimators.electric_dipole_polarizability(init_setup, overwrite=True)
        plot_and_print_field_estimators.electric_dipole_polarizability_cross_terms(init_setup, overwrite=False)
        plot_and_print_field_estimators.electric_first_hyper_polarizability(init_setup, overwrite=False)


def add_calculation_specific_data(init_setup):
    if not os.path.isdir("results"):
        os.mkdir("results")
    if not os.path.isdir("results/cache"):
        os.mkdir("results/cache")

    add_block_data(init_setup)
    labels, counts = particle_labels()
    init_setup.prt_labels = labels
    init_setup.prt_counts = counts

def add_block_data(init_setup):
    block_data_file = "results/cache/block_data.pickle"
    mod_times_current = get_last_modification_times(init_setup, "energy.h5")
    if init_setup.use_cache and os.path.isfile(block_data_file):
        with open(block_data_file, "rb") as f:
            (S_mat, N_mat, failed_blocks_mat, mod_times_cache) = pickle.load(f)
        # Check if any value changed
        if np.all(mod_times_cache == mod_times_current):
            init_setup.S_mat = S_mat
            # Do take N value from this, because it is valid only for ET&Vt
            init_setup.N_mat = N_mat
            init_setup.failed_blocks_mat = failed_blocks_mat
            return

    S_mat = np.full((len(init_setup.dir1_labels), len(init_setup.dir2_labels)), dtype=np.int_, fill_value=9999)
    N_mat = np.full((len(init_setup.dir1_labels), len(init_setup.dir2_labels)), dtype=np.int_, fill_value=9999)
    failed_blocks_mat = np.full((len(init_setup.dir1_labels), len(init_setup.dir2_labels)), dtype=object, fill_value=9999)
    for i in range(len(init_setup.dir1_labels)):
        for j in range(len(init_setup.dir2_labels)):
            dir1 = init_setup.dir1_labels[i]
            dir2 = init_setup.dir2_labels[j]
            path = os.getcwd() + "/" + dir1 + "/" + dir2
            if not os.path.isdir(path):
                S_mat[i,j] = 9999
                N_mat[i,j] = 9999
                failed_blocks_mat[i,j] = None
                continue
            obs_path = get_obs_dir(path)
            data_Et, attr_dict_Et = read_single_HDF5_dataset(obs_path + "energy.h5", "/E/Et")
            data_Vt, attr_dict_Vt = read_single_HDF5_dataset(obs_path + "energy.h5", "/E/Vt")

            # Get and set N
            N = attr_dict_Et["N"][0]
            assert(N == attr_dict_Vt["N"][0])  # Check invariants
            if N == 9999:
                raise Exception("Sorry! You have 9999 blocks, which is not supported as 9999 is\n"
                                "my code's internal error value. Please calculate one block more.")
            N_mat[i,j] = N

            # Get S (coverged one, not hdf5 one!)
            # Note convergence detection is made from raw Vt data, High values are not cropped out!
            S = conv_det.calculate_converged_point(data_Vt, N)  # automatic detection
            S = S+1  # Indexing starts from 1 in fortran, so it starts from 1 in our hdf5 containers

            if os.path.isfile(path + "/set_S"):
                with open(path + "/set_S", "r") as f:
                    for line in f:
                        try:
                            S = int(line.strip())
                            break
                        except ValueError:
                            raise Exception("Invalid 'set_S' file, it must contain one number only")
            S_mat[i,j] = S

            # If my data reading has failed, it may have left default value 9999
            assert(np.all(np.abs(data_Et[:N]-9999) > 0.000000001))
            assert(np.all(np.abs(data_Vt[:N]-9999) > 0.000000001))

            # Find if there are failed blocks
            Et_zero = np.abs(data_Et[:N]) == 0.0  # 0.0 is hdf5 init-value
            Vt_zero = np.abs(data_Vt[:N]) == 0.0  # 0.0 is hdf5 init-value
            failed_blocks_mat[i,j] = np.nonzero(np.logical_or(Et_zero, Vt_zero))[0]

            # if np.logical_xor(Et_zero, Vt_zero).any():
            #     print("Error: Only other 'Et' or 'Vt' have failed one block, but not both. This is quite weird.")
            #     print("Et:", np.nonzero(Et_zero), ", Vt:", np.nonzero(Vt_zero))


    init_setup.S_mat = S_mat
    init_setup.N_mat = N_mat  # Do take N value from this, because it is valid only for ET&Vt
    init_setup.failed_blocks_mat = failed_blocks_mat

    if not os.path.isdir("results/cache"):
        os.mkdir("results/cache")
    with open(block_data_file, "wb") as f:
        pickle.dump((S_mat, N_mat, failed_blocks_mat, mod_times_current), f)


def calculation_combining(obs_flags):
    if os.path.isdir("sample"):
        os.chdir("..")

    # 'fig_ax': All figures and axes are stored here, and also some other information.
    # key: observable name, value: (fig, ax)
    fig_ax = {}
    # These all must match between calculations
    properties = lambda o: [o.latex_symbol1, o.latex_symbol2, o.latex_unit1, o.latex_unit2,
                            o.latex_symbol3, o.latex_unit3, o.unite_A2_figure_with_r2,
                            o.logarithmic_y_axis, o.logarithmic_x_axis, o.extrapolate_range1,]

    listdir = sorted(list(os.listdir()))
    range3_0 = []       # Unsorted
    dirs_0 = []         # Unsorted
    calculations_0 = [] # Unsorted
    count = 0

    # Find all valid directories for calculations, and collect some initial data about systems
    for dir in listdir:
        if not os.path.isdir(dir):
            continue
        path = os.path.abspath(dir + "/sample")

        os.chdir(dir)
        try:
            check_that_this_is_run_in_correct_directory()
        except ValueError:
            os.chdir("..")
            continue

        count += 1

        sys.path.append(path)
        # Read initial parameters of calculation set
        if count == 1:
            import configure_instance # You must be in same folder with this py-file
        else:
            configure_instance = reload(configure_instance)
        sys.path.remove(path)

        init_setup = configure_instance.InitialSetup()
        add_default_values(init_setup, obs_flags)

        if (init_setup.latex_symbol3 is None
                or init_setup.range3_val is None
                or init_setup.latex_unit3 is None):
            raise ValueError("You need to add fields 'latex_symbol3', 'range3_val' and "
                             + "'latex_unit3' in initial_setup.py")

        # If execution gets here, then directory is valid calculation

        init_setup.plot_only_extrapolation = True
        init_setup.fig_ax = fig_ax

        dirs_0.append(path)
        calculations_0.append(init_setup)
        range3_0.append(init_setup.range3_val)

        os.chdir("..")

    # Sort them. Damn, python sucks at sorting many lists to same order. Readable? No.
    systems = sorted(list(zip(dirs_0, calculations_0, range3_0)), key=lambda o: o[2])
    dirs, calculations, range3 = tuple(zip(*systems))  # Sorted

    if count == 0:
        raise ValueError("No valid directories found!")
    for first, other in zip(properties(calculations[0]), properties(init_setup)):
        if first != other:
            raise ValueError("Your calculations are not compatible!", first, "!=", other)
    print("Found", count, "calculations, with values", calculations[0].latex_symbol3, ":", range3)

    fig_ax["range3"] = range3
    fig_ax["range3_len"] = count

    # now do the plotting part
    for i, dir in enumerate(dirs):
        os.chdir(dir + "/..")
        print(os.getcwd())
        init_setup = calculations[i]
        init_setup.range3_idx = i
        label = "{} = {} {}".format(init_setup.latex_symbol3, init_setup.range3_val,
                                    init_setup.latex_unit3)
        print("##############################################")
        print("Processing:", label)

        add_calculation_specific_data(init_setup)
        plot_data.print_data.print_energy_and_block_info(init_setup)
        plot_observables(init_setup, obs_flags)
        print("\n\nFinished dir:", dir)
        os.chdir("..")

    print("Whew! Everything is done now. Plotting images. ctrl + c to close")
    plt.show()



if __name__ == "__main__":
    main(sys.argv)
