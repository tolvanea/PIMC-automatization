This directory contains files that plots figures from simulations generated with `python3 -m plot_data.main`.

Main functionality plots energy in function of variated value (e.g. tau, T). Also lot of other plots are drawn, like energy by block figures. Also energy and other observables are printed and saved to a table.


Usage

* Go to directory that which has 'sample' subdirectory in it. Then run
    * `python3 -m plot_data.main`
* There are extra flags you can use:
    * Useful: Plot energy convergence (i.e. energy per block) `python3 -m plot_data.main --E_conv    `
    * Plot all paircorrelation distributions (if data available): `python3 -m plot_data.main --pc`
    * Plot diamagnetic suceptibility (if data available): `python3 -m plot_data.main --susc`
    * Plot energy components (if data available): `python3 -m plot_data.main --comp`
    * You can combine multiple flags to plot them all.
    * For many other usage cases, see documentation in `main.py`.
