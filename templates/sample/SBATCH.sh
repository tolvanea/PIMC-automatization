#!/bin/bash -l
# created: May 2020 by tolvanea
# This template is made to be used with PIMC_automatization
# PLEASE FIX HARD CODED PARTS MARKED WITH "FIXME" 
# author: tolvane2

# Usage:
# Subit this sbatch-file with
#   $ sbatch SBATCH.sh

# FIXME: replace project unix name "trantal1" and replace partition "small"
#SBATCH --account=trantal1
#SBATCH --partition=small

# The following fields will be replaced by configure_instance.py
#SBATCH --job-name=XXXX
#SBATCH --mem-per-cpu=999
#SBATCH --time=72:00:00
#SBATCH --ntasks=1
#SBATCH -o out.txt
#SBATCH -e err.txt



echo -e "\n slurm stuff" >> run_info.txt
echo "ID: $SLURM_JOBID" >> run_info.txt
echo "Directory: $SLURM_SUBMIT_DIR" >> run_info.txt
echo "Date: $(date -Iseconds)" >> run_info.txt
echo "Cores: $SLURM_NPROCS" >> run_info.txt
echo "Mem/CPU: $SLURM_MEM_PER_CPU" >> run_info.txt
#( set -o posix ; set ) > variables.txt  # get all local variables
#printenv > variables.txt  # get all local variables

# FIXME: replace log file path (optional, but I recommend it though)
echo -e "$(date -Iseconds), $SLURM_JOBID, $SLURM_JOB_NAME, $SLURM_SUBMIT_DIR\n" >> /scratch/trantal1/tolvanea/slurmi_logi.txt

module load hdf5

# The following line will be modified by configure_instance.py 
# Executable is replaced with correct path (in my case "/users/tolvane2/pimc3/build/pimc3")
srun /users/tolvane2/koodit/pimc3/build/pimc3 minimal

# This script will print some usage statistics to the end of file: out.txt
echo "\n"
seff $SLURM_JOBID >> run_info.txt
echo "\n"
