# PIMC-automatization

This repository contains many different scripts that are useful with PIMC (Path Integral Monte Carlo) code, which is developed in Tampere University. The `pimc3` code is at
https://gitlab.com/compphys/est/pimc3
but it has still restricted access. If you need acces, contact us.

The scripts in this repository automate sensitivity analysis with `pimc3`, that is, they run many parallel `pimc3`-simulations that vary one or two parameters. For example: How energy changes if temperature is varied using three different time steps? Or: How susceptibility changes by variating temperature or nuclear mass? The most common parameters to vary are temperature and the time step.

The `pimc3` is a standalone program, so these scripts generates multiple configuration files and spawn multiple instances of the program.

This is project is made for managing calculations in my master's thesis 
https://gitlab.com/tolvanea/msc-thesis .

## Functionlities
Main scripts are:

* `start_PIMC/run_set`
    * Start multiple simulations, which have vary some parameter. The user provides simulation template and  write a rule for varying parameter. This script copies the simulation template, applies rule, and runs them all. Works both with home PC and CSC-cluster.
    * See documentation in [start_PIMC/README.md](/start_PIMC/README.md) for usage.
* `plot_data/main`
    * Plot sensitivity analysis which is obtained by running `start_PIMC.run_set`. Reads a directory tree of simulations that is generated, and prints and plots a lot of useful information
    * See documentation in [plot_data/README.md](/plot_data/README.md) for usage.

Also there's some helper scripts here and there that do some specific singular task. For example,
* `read_data/hdf5_summary`
    * Print hdf-file content recursively. Instead of printing all block data, it prints only averages over blocks. A general tool for quickly inspecting raw data of single simulation.


## Dependencies and setup
First of, you need our software `pimc3`.

Also you need these python libraries:
```pip3 install numpy matplotlib uncertainties scipy h5py psutil```

All documentation in this repo assumes that this repo is cloned to local directory and it has been added to pythonpath. That is, clone this in some folder with:
```
git clone https://gitlab.com/tolvanea/PIMC-automatization.git
```
Add repo to pythonpath by adding following line to you shell script (e.g. `~/.bashrc`)
```
export PYTHONPATH="${PYTHONPATH}:/...your_path_to.../PIMC-automatization/"
```
By having the repo in pythonpath helps to run it easily from many different simulation directories.

These scripts are developed and tested with linux only, and they may work on mac. I have no idea if they work on windows, because filesystem paths on windows are different.

## Basic usage

### Starting simulation and reading data:

In nutshell, this project is used like so:

1. Copy [sample](/templates/sample)-folder as a base for simulation.
2. Edit the parameters to make the simulation you want
3. Start a set of new simulations with

    ```python3 -m start_PIMC.run_set --start```

    (To get full documentation, add flag `--help`.
    See [start_PIMC/README.md](/start_PIMC/README.md) for more information about starting/continuing simulations.)
4. Let simulation to run some while. Data can be read/plotted while it's running.
5. Plot energy and its convergence with

    ```python3 -m plot_data.main --E_conv```

    (To get full documentation, add flag `--help` to the command. Also, see [plot_data/README.md](/plot_data/README.md) for some brief information about the flags.)

Please run the commands with `--help` flag to read updated documentation of the usage. You can also read that documentation from header comments of corresponding python files. So, please read that documention.

### Reading raw data for singular simulation
Let's suppose you have started pimc3 directly and you do not have directory structure generated by `start_PIMC.run_set --start`. There are some scripts to print data of singular simulation directly from observables.

* Print all averaged data of some hdf5 file (here `energy.h5`):

    ```python3 -m read_data.hdf5_summary energy.h5```

    (The averaging is a bit more basic than in the main script, but well usable in testing.)
* Print magnetic susceptibilities with unit conversions:

    ```read_data.construct_observables.read_diamagnetic_susceptibility```

For reading raw hdf5 files, there is also offical program `h5dump` from (Debian) package `h5utils`. But my script `hdf5_summary.py` is better for that.




## Code quality
Over the years, this code base has grown organically to quite large spaghetti mess. The only purpose it have had, is to quickly produce results and images for me. Because I am likely the only user, documentation is mostly written to explain my future self what the past self has hacked. Generally the code is ugly, weird, and sometimes just stupid. My soul cries when I read it, but I have not fixed it, because it is unnecessary to fix something that _technically_ works. I have always been in a hurry, so technical depth has cumulated over the years. The _code does work_, at least for the tasks I use it for, but the internal refactoring and code is anything but beautiful.

Should you use it?

* If you need to vary some parameter in multiple different simulations: _yes_
* If you need to extrapolate out the time step error: _yes_
* If you want to run single calculation: _maybe_
* If you need to plot automatically figures out of it: _yes_
* If you need to analyze those calculations more deeply: _yes_
* If you want to avoid common pitfalls with PIMC parameter choise: _yes_
* If you want to modify something to this code that it does not yet provide: _uh oh, good luck_

If you consider using this bundle of scripts, please contact me. Of course there's some documentation, but it is quite scarce. I can try to instruct how to use these tools, if even I can remember it. It may be worth to get these tools running, because they automate _a lot_ of manual work. Also reimplementing similar set of tools would take at least two summers of work.


Licence: MIT or CC0 or CRAP licence, so do whatever you wish with it
