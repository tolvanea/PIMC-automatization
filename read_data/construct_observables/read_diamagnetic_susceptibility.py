"""
This script reads magnetic suceptibility from hdf-files. Can be called in 
calculation directory (the one that contains CONF, PARTICLES, obs...), or in obs-directory.

Run with
python3 -m read_data.construct_observables.read_diamagnetic_susceptibility



Explanation of values in statmagn.h5 file, assuming that system is
positronium Ps (p+e)

m = (q_e*A_e + q_p*A_p)
m_x = x coordinate of m
m_p = q_p*A_p
m_e = q_e*A_e
The quoted text is the observable in statmagn.h5. The original observable is multiplied with
- µ * e^2 * a0^4 / (β * hbar**2) * avogadro
to get these results

<m^2>           "dia.susc._(qA)^2_"
    -22.35(4)e-11 m^3/mol
<m_x*m_y>       "dia.susc._cross_terms_xy-yz-zx_"
    0.002(22)e-11 m^3/mol
<m_e^2>         "dia.susc._(qA)^2_e_only_"
    -46.7(8)e-11 m^3/mol
<m_p^2>         "dia.susc._(qA)^2_p_only_"
    -46.7(8)e-11 m^3/mol
<A_e*A_p>       "dia.susc._(A_e*A_p)_"
    -35.5(7)e-11 m^3/(mol q^2)
<(A_e+A_p)^2>   "dia.susc._(sumA)^2_"
    -164(3)e-11 m^3/(mol q^2)
<m_e*m_p>       "dia.susc._(q_e*A_e)*(q_p*A_p)_"
    35.5(7)e-11 m^3/mol


"""
import numpy as np
import h5py
# PIMC-automatization repo is in path
from os import getcwd, path
from uncertainties import ufloat  # pip install [--user] uncertainties

from read_data.utilities.misc import get_obs_dir, get_calcuation_dir
from miscellaneous import conf_modification
from read_data.auto_convergence_detection import calculate_converged_point
from miscellaneous import general_tools
from plot_data.plot_energy import observables_for_each_particle_pair
from read_data.utilities.read_particles import particle_info
from read_data.read_calc_data import extrapolate_range1_at_0
import read_data.utilities.sem2 as sem2
import read_data.read_calc_data as read_calc_data


class au:
    # https://en.wikipedia.org/wiki/Atomic_units
    # SI units are in au class, yes stupid, but kept here for backwards compatibility
    charge_in_SI = 1.602176634e-19
    hbar_in_SI = 1.054571800e-34
    mass_in_SI = 9.1093837015e-31

    length_in_SI = 5.29177210903e-11
    speed_of_light = 137.035999084
    fine_structure_constant = 1/speed_of_light
    energy_in_SI = 4.3597447222071e-18
    time_in_SI = 2.4188843265857e-17
    temperature_in_SI = 3.1577464e5
    electric_current_in_SI = time_in_SI * charge_in_SI
    force_in_SI = energy_in_SI / length_in_SI
    velocity_in_SI = length_in_SI / time_in_SI
    speed_of_light_in_SI = velocity_in_SI * speed_of_light

    mu0 = 4*np.pi / speed_of_light**2
    e = 1.0
    hbar = 1.0
    eps0 = 1.0/(4*np.pi)
    m_e = 1.0
    m_p = 1836.1526734
    c = speed_of_light
    µ_0 = mu0
    a0 = 1.0

    # Unitless:
    alpha = 1.0/c
    N_A = 6.02214076e23  # Avogadro

class SI:
    boltzmann_constant = 1.380649e-23
    vacuum_magnetic_permeability = 4*np.pi * 1e-7
    mu0 = vacuum_magnetic_permeability
    eps0 = 8.8541878128e-12
    e = au.charge_in_SI
    a0 = au.length_in_SI
    k_B = boltzmann_constant
    hbar = au.hbar_in_SI
    speed_of_light = 299792458
    m_e = au.mass_in_SI
    m_p = m_e * au.m_p

    c = au.velocity_in_SI * au.c
    µ_0 = 4*np.pi * 1e-7

    # Unitless:
    alpha = 1/137.035999084
    N_A = 6.02214076e23  # Avogadro

class constants:
    avogadro = 6.02214076e23
    H_mass_in_u = 1.007825
    H_molar_mass = H_mass_in_u * avogadro # this is [mol/kg]

    
    
def get_tau(obs_dir):
    calcdir = get_calcuation_dir(obs_dir)
    if not path.isfile(calcdir + "CONF"):
        raise Exception("CONF file not found")
    parts = conf_modification.read_line(calcdir + "CONF", "Tau")
    tau = float(parts[0])
    return tau

def get_converged_point_and_N(obs_dir):
    """ 
    Gets indices for valid data
    Indexins starts from 1
    """
    filename = obs_dir + 'energy.h5'
    f = h5py.File(filename, 'r')
    virial_energy = f["E"]["Vt"][...]
    N = int(f["E"]["Vt"].attrs["N"])
    start = calculate_converged_point(virial_energy, N)
    return (start, N)

def read_susceptibilities(obs_dir, tau, units="SI", obs="diamagn_susceptibility_A^2_"):
    """
    Old function, mostly deprecated
    """
    filename = obs_dir + 'statmagn.h5'
    f = h5py.File(filename, 'r')
    group = f["MAGN"]
    
    trotter = group["statinfo"].attrs["M"]
    beta = int(trotter) * float(tau)
    
    converged_point, _ = get_converged_point_and_N(obs_dir)
    
    mgn_susceptibility = []
    for i in range(1,4):
        # + str(i)
        name = obs + str(i)
        N = group[name].attrs["N"][0]
        area_squared = group[name][converged_point:N]  # A^2

        # Magnetic susceptibility is - A^2 * e^2 /(B * c^2 * h_bar^2) 
        # Note that in atomic units most of natural constants are 1.0
        chi = convert_A2_to_units_of_susceptibility(area_squared, beta, units, trotter)
        mgn_susceptibility.append(chi)

    return np.array(mgn_susceptibility)

def convert_A2_to_units_of_susceptibility(area_squared, beta, units="SI", trotter=None):
    """
    Convert value <(qA)^2> calculated in statmagn.h5 to units of magnetic susceptibility
    """
    # Equation for magnetic susceptibility is in gaussian units is:
    #   - <A>^2 * e^2 /(beta * c^2 * h_bar^2),
    #   Source: source Pollock and Runge 1992
    # More precisely I derived it to be in SI:
    #   - <(sum_i q_i A_i)^2> * mu_0 /(beta * h_bar^2)
    #   where i iterates over all particles
    # (Note! chi is system susceptibility, so no volume included.)

    if units == "au":
        l = 1.0  # length
        e = 1.0  # charge
        β = beta
        ℏ2 = 1.0**2  # planks constant
        µ = au.mu0  # magnetic permeability of vacuum or something
        m = au.m_e  # mass
    elif (units == "SI") or (units == "cgs"):
        # Convert in SI units
        l = SI.a0  # length
        e = SI.e   # charge
        β = beta / au.energy_in_SI
        ℏ2 = SI.hbar**2
        µ = SI.mu0
        m = SI.m_e  # mass
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")

    if trotter is not None:
        # See pollock and runge 1992
        tau = β / trotter
        tau_correction = tau * ℏ2 / (4 * m**2)
    else:
        tau_correction = 0
    # Why this is disabled ?? - Because it is not needed, and it assumes free particles in high t or something.
    tau_correction = 0

    chi_sys = - µ * (e**2) * (tau_correction + (area_squared * l**4) / (β * ℏ2))
    chi_mol = chi_sys * constants.avogadro

    if units == "au":
        return chi_sys
    elif units == "SI":
        return chi_mol
    elif units == "cgs":
        # convert from SI to cgs.
        # Because this chi is already multiplied by volume, (m^3 -> cm^3) factor 1e-6
        chi_mol_in_stupid_cgs_units = chi_mol / (1e-6 * 4*np.pi)
        return chi_mol_in_stupid_cgs_units
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")

def convert_x2y2_to_units_of_susceptibility(x2y2, units="SI"):
    """
    Convert x2y2 term in statmagn.h5 to units of susceptibility
    :return:
    """
    # I derived extra term in susceptibility to be (in SI):
    #   q^2/(4m)*(x^2+y^2)

    if units == "au":
        e = 1.0  # electric charge
        m = 1.0  # mass
        l = 1.0  # bohr radius
        µ = au.mu0  # magnetic permeability of vacuum or something
    elif (units == "SI") or (units == "cgs"):
        e = SI.e    # electric charge
        m = SI.m_e  # mass
        l = SI.a0  # bohr radius
        µ = SI.mu0
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")

    x2y2_with_units = - (µ * e**2 / m) * (x2y2 * l**2)
    x2y2_mol = x2y2_with_units * constants.avogadro

    if units == "au":
        return x2y2_with_units
    elif units == "SI":
        return x2y2_mol
    elif units == "cgs":
        # convert from SI to cgs.
        # Because this chi is already multiplied by volume, (m^3 -> cm^3) factor 1e-6
        chi_mol_in_stupid_cgs_units = x2y2_mol / (1e-6 * 4*np.pi)
        return chi_mol_in_stupid_cgs_units
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")


def read_susecptibility_from_r2_data(init_setup, unit):
    """
    Read susceptibility from <r^2> particle distance data using Rebrane's formula
    See  article "Nonadiabatic theory of diamagnetic susceptibility of molecules", Rebane 2002
    """
    if unit == "au":
        c = au
    elif (unit == "SI") or (unit == "cgs"):
        c = SI
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")

    dict_R2ave = observables_for_each_particle_pair(init_setup,
                                                   prefix="/PC", postfix="/R2ave",
                                                   filename="paircorrelation.h5")

    callable = lambda init_setup, path, i, j: (None, particle_info(path + "/PARTICLES"))
    prt_info = read_calc_data.read_generic_information(init_setup, callable)
    particle_labels = list(prt_info.keys())
    if len(particle_labels) > 2:
        # Susceptibility calculation is implemented only for max two different kind of particles
        return None, None

    masses = {}
    charges = {}
    amounts = {}
    r2s = {}
    r2_errs = {}
    zeros_f = np.zeros((len(init_setup.range1), len(init_setup.range2)), dtype=np.float_)
    zeros_i = np.zeros((len(init_setup.range1), len(init_setup.range2)), dtype=np.int_)
    for lab in particle_labels:
        amounts[lab] = zeros_i.copy()
        charges[lab] = zeros_f.copy()
        masses[lab] = zeros_f.copy()
        for i in range(len(init_setup.range1)):
            for j in range(len(init_setup.range2)):
                info = prt_info[lab][i,j]
                if info is None:
                    amounts[lab][i,j] = 0
                    charges[lab][i,j] = float("nan")
                    masses[lab][i,j] = float("nan")
                else:
                    amounts[lab][i,j] = info[0]
                    charges[lab][i,j] = info[1]
                    masses[lab][i,j] = info[2]

    ord = lambda x, y: (x,y) if x < y else (y,x)
    total_charge = 0.0
    for lab in charges:
        total_charge += amounts[lab] * charges[lab]
    if np.any(total_charge > 0.00001):
        return None, None  # Must be charge neutral system

    for pair_path in dict_R2ave:
        # pair is string like "/PC/e_p/R2ave"
        pair_str = pair_path.split("/")[2]
        p1, p2 = pair_str.split("_")
        (_, attrs) = dict_R2ave[pair_path]
        r2s[ord(p1, p2)] = attrs["mean"]
        r2_errs[ord(p1, p2)] = attrs["err"]

    for p in particle_labels:
        # only one particle of this kind, (no inter-particle distance therefore)
        if (p, p) not in r2s:
            r2s[(p, p)] = zeros_f.copy() + 1  # Does not matter what is here written, is not used by eq34
            r2_errs[(p, p)] = zeros_f.copy() + 1  # Does not matter what is here written, is not used by eq34


    def eq_34(c, p, N, q, m, M, r2_1N, r2_12, r2_N1N):  # Equation 34, (not rule 34)
        """
        Calculate magnetic susceptibilities for molecules, with and without born oppenheimer.
        Must be only two kinds of particles.
        See  article "Nonadiabatic theory of diamagnetic susceptibility of molecules", Rebane 2002

        For born oppenheimer just use infinite mass.
        (If there is one particle of some kind, then it does not matter what mean value is give for
        interparticle distance in that case)

        (It does not matter if electrons or nucleus are interchanged, equation is analytical
        after all.)

        Args
            c       class of natural constants
            p       number of electrons
            N       number of nucleus + electrons
            q       charge of electron
            m       mass of electron
            M       nucleus mass
            r2_1N   square mean distance <r^2> of nucleus-electron
            r2_12   square mean distance <r^2> of electron-electron
            r2_N1N  square mean distance <r^2> of nucleus-nucleus
            """
        alpha_units = 1/(au.alpha**2)
        # Transform values from paper's au energy units to (SI/au) susceptibility
        E_to_susc = lambda E_dia: 4 * np.pi * c.a0**3 * (-2 * c.alpha**2 * E_dia)
        eq34_in_au_E = alpha_units \
                       * (p*q**2/(24*(N-p)*au.c**2))\
                       * ((1/m)*(2*(N-p)*r2_1N - (N-p-1)*r2_N1N)
                          + (1/M)*(2*p*r2_1N - (p-1)*r2_12))
        return E_to_susc(eq34_in_au_E)

    p1 = particle_labels[0]
    p2 = particle_labels[1]
    susc = np.zeros((len(init_setup.range1), len(init_setup.range2)), dtype=np.float_)
    susc_err = np.zeros((len(init_setup.range1), len(init_setup.range2)), dtype=np.float_)
    for i in range(len(init_setup.range1)):
        for j in range(len(init_setup.range2)):
            s = eq_34(c=c, p=amounts[p1][i,j], N=amounts[p1][i,j]+amounts[p2][i,j],
                         q=charges[p1][i,j], m=masses[p1][i,j], M=masses[p2][i,j],
                         r2_1N=ufloat(r2s[ord(p1,p2)][i,j], r2_errs[ord(p1,p2)][i,j]),
                         r2_12=ufloat(r2s[(p1,p1)][i,j], r2_errs[(p1,p1)][i,j]),
                         r2_N1N=ufloat(r2s[(p2,p2)][i,j], r2_errs[(p2,p2)][i,j])
                         )
            susc[i,j] = s.n
            susc_err[i,j] = s.s

    if init_setup.extrapolate_range1:
        susc, susc_err = extrapolate_range1_at_0(init_setup, susc, susc_err)

    if unit == "au":
        return susc, susc_err
    elif unit == "SI":
        return susc * c.N_A, susc_err * c.N_A
    elif unit == "cgs":
        # convert from SI to cgs.
        # Because this chi is already multiplied by volume, (m^3 -> cm^3) factor 1e-6
        stupid_cgs = c.N_A / (1e-6 * 4*np.pi)
        return susc * stupid_cgs, susc_err * stupid_cgs
    else:
        raise ValueError("Unknown unit (not 'SI', 'cgs', 'au')")



# # Test that unit conversion works as should
# def test_unit_few_conversions():
#     print("wtf?")
#     # eps0 = 1 / (mu0 * c^2)
#     micro_in_SI_1 = 1 / (SI.eps0 * SI.speed_of_light**2)
#     micro_in_SI_2 = SI.mu0
#     print("micro_in_SI_1, micro_in_SI_2", micro_in_SI_1, micro_in_SI_2)
#     assert(np.isclose(micro_in_SI_1, micro_in_SI_2))
#
#     c_1 = au.speed_of_light * au.velocity_in_SI
#     c_2 = SI.speed_of_light
#     assert(np.isclose(c_1, c_2))
#
#     multiplier_2 = 4*np.pi * (1/(4*np.pi*SI.eps0))/SI.speed_of_light**2
#     multiplier_1 = SI.mu0
#     assert(np.isclose(multiplier_1, multiplier_2))
#
#     beta_in_SI = (au.time_in_SI / SI.hbar)
#     beta_in_SI_2 = (1.0 / (SI.boltzmann_constant * au.temperature_in_SI))
#     beta_in_SI_3 = 1.0 / au.energy_in_SI
#     assert(np.isclose(beta_in_SI, beta_in_SI_2))
#     assert(np.isclose(beta_in_SI, beta_in_SI_3))
#
#     asdf = SI.mu0
#     fdsa = 1 / (SI.speed_of_light**2) * 4*np.pi / (4*np.pi*SI.eps0)
#     assert(np.isclose(asdf, fdsa))




def main(obs):
    #test_errors()
    obs_dir = get_obs_dir(getcwd())
    if not path.isfile(obs_dir + "statmagn.h5"):
        raise Exception("No obs/statmagn.h5 found")

    tau = get_tau(obs_dir)
    chi_SI = read_susceptibilities(obs_dir, tau, obs=obs)
    chi_cgs = read_susceptibilities(obs_dir, tau, units="cgs", obs=obs)
    chi_au = read_susceptibilities(obs_dir, tau, units="au", obs=obs)

    chis_SI = []
    chis_cgs = []
    chis_au = []
    for i,s in enumerate(["x", "y", "z"]):
        mean_SI, err_SI = sem2.sem2(chi_SI[i,:])
        mean_cgs, err_cgs = sem2.sem2(chi_cgs[i,:])
        mean_au, err_au = sem2.sem2(chi_au[i,:])
        chis_SI.append(ufloat(mean_SI, err_SI))
        chis_cgs.append(ufloat(mean_cgs, err_cgs))
        chis_au.append(ufloat(mean_au, err_au))
        if False:
            print("   chi_{} - SI: {}e-11 m^3/mol    cgs:{}e-6 cm^3/mol    a.u.: {}e-4 a0^3"
                .format(s,
                    general_tools.pretty_print_errorbars(mean_SI*10**11, err_SI*10**11),
                    general_tools.pretty_print_errorbars(mean_cgs*10**6, err_cgs*10**6),
                    general_tools.pretty_print_errorbars(mean_au*10**4, err_au*10**4),
            ))
    chi_SI_mean = (chis_SI[0] + chis_SI[1] + chis_SI[2]) / 3
    chi_cgs_mean = (chis_cgs[0] + chis_cgs[1] + chis_cgs[2]) / 3
    chi_au_mean = (chis_au[0] + chis_au[1] + chis_au[2]) / 3

    print("    χ - SI: {}e-11 m^3/mol    cgs:{}e-6 cm^3/mol    a.u.: {}e-4 a0^3"
        .format(
            general_tools.pretty_print_errorbars(chi_SI_mean.n*10**11, chi_SI_mean.s*10**11),
            general_tools.pretty_print_errorbars(chi_cgs_mean.n*10**6, chi_cgs_mean.s*10**6),
            general_tools.pretty_print_errorbars(chi_au_mean.n*10**4, chi_au_mean.s*10**4),
        )
    )

if __name__ == "__main__":
    print("Magnetic susceptibility: (assuming uniform material in all directions)")
    obsnames = [
        "diamagn_susceptibility_A^2_",
        "dia.susc._(qA)^2_",
        "dia.susc._cross_terms_xy-yz-zx_",
        "dia.susc._(qA)^2_e_only_",
        "dia.susc._(qA)^2_p_only_",
        "dia.susc._(A_e*A_p)_",
        "dia.susc._(sumA)^2_",
        "dia.susc._(q_e*A_e)*(q_p*A_p)_",
    ]
    for obs in obsnames:
        print(obs, ":")
        try:
            main(obs)
        except KeyError:
            print("    Not found")
        except IndexError:
            print("    Not enough data")

