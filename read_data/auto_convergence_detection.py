import numpy as np
from scipy.stats import norm, normaltest
from scipy.special import factorial


def calculate_converged_point(data_arr, N=None, assume_converged=0.5, threshold=1e-3) -> int:
    """
    Automatically find out value for S (converged point).

    Assume former part of data is converged. Calculate converged point with
    rolling window averaging. Rolling window is 1/6 of data length. `S` is
    the smallest value for which blocks on range [S, S + N/6] are not
    statistically differing from converged part.

    Threshold determines what is statistical signicant. Blocks are considered
    to be statistically different with default threshold 1/1000. The closer
    threshold is to 0, the more permissive this algorithm is. Value threshold=1
    means that upper bound for highest allowed deviation is determined directly from
    converged data. That is, rolling window average of first half can not be higher
    than on converged side. Threshold=0.001 means that deviation with likelyhood of
    0.1% is allowed on data. Threshold is not mathematically exact probability, but
    is some similar tuning factor anyways.


    This algorithm works as follows
        1. Assume data is gaussian.
        2. Assume the end of data is converged. Let `N * (1-assume_converged)` be called "converged point".
        3. Calculate the fluctuation of converged data, that is, calculate the boundaries
           for rolling window average. Calculate maximum and minimum window mean, and
           add `threshold` amount of extra deviation to them.
        4. Start stepping backwards from converged point with rolling window. If mean of
           window is outside boundaries determined from converged part, then place
           converged point S there.

    :param data_arr:            1d numpy array
    :param N:                   end-point, default = len(data_arr)
    :param assume_converged:    If 0.33, the former 1/3 of data is assumed to be converged
    :param threshold:           Allow deviation this improbable
    :return: S                  converged point
    TODO Student's t-test would be better way of doing this, but if this works,
         it's ok. https://en.wikipedia.org/wiki/Student%27s_t-test
    """

    if len(data_arr.shape) != 1:
        print("data_arr.shape", data_arr.shape)
        raise ValueError("1d matrices only!")

    N = N or len(data_arr)
    window_size = max(N//6, 5)
    l = int(N * (1-assume_converged))  # Mid point dividing converged from non-converged
    step = max(1, window_size//10)  # skip some indices in iteration for better efficiency

    def get_fluctuation_range(converged, prob, window_size, step):

        min_mean = np.inf
        max_mean = -np.inf

        for j in range(window_size, len(converged), step):
            win = converged[j-window_size: j]
            mean = win.mean()
            sem = win.std() / np.sqrt(len(win))  # 'sem' = standard error of mean

            top = norm.ppf(1-prob, loc=mean, scale=sem)
            bottom = norm.ppf(prob, loc=mean, scale=sem)

            min_mean = min(bottom, min_mean)
            max_mean = max(top, max_mean)

        return min_mean, max_mean



    min_allowed_mean, max_allowed_mean = get_fluctuation_range(
        data_arr[l:],
        threshold,
        window_size,
        step,
    )

    S = l
    for i in range(l, window_size, -step):
        win = data_arr[i-window_size: i]
        mean = win.mean()
        if not ((min_allowed_mean < mean) and (mean < max_allowed_mean)):
            break
        S = i-(window_size//4)
    return S

def moving_average(data, window=9):
    """
    Moving window average on data. Returns array that has size of
    len(data) - window - 1
    """

    if np.mod(window, 2) != 1:
        raise Exception("Window size must be an odd number!")
    if len(data) < window:
        raise Exception("Data size less than window size! data:{}, window={}".
                        format(len(data), window))

    half = (window-1) // 2  # Half window

    mean_arr = np.full(len(data)-2*half, fill_value=np.float64("nan"))
    idx_arr = np.arange(half, len(data) - half)

    for i in range(half, len(data) - half):
        mean_arr[i-half] = np.mean(data[i-half:i+half+1])

    assert(len(mean_arr) == len(idx_arr))

    return mean_arr, idx_arr
