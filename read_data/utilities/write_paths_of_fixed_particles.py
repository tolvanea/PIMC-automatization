import numpy as np
import h5py
import os

"""
Writes hdf5-files so that for example, fixed nuclues H2-distance can be varied.
I do not remeber now how complicated it was to use this module.

Ok, so here's how it's done:
    0. Set second last column in 'PARTICLES' to 0, which means that this particle is fixed
                                     |
        Like so:    "p  2  1.0  1.0  0  0"
    1. Start pimc, and let it run one second, fixed particle paths are initialized at pos 0,0,0
    2. take the paths-directory to sample
    3. use set_prt_positions() in your configure_instance.py
"""


# CAN NOT REMEMBER HOW THIS FUNCTION IS SUPPOSED TO BE USED
# I haven't written good documentation to myself --> totally useless function
# def create_paths(positions, prt_name="p", file_name="paths/001.h5"):
#     """
#     Creates new file "paths/001.h5" in which is written
#
#     :param file_name:   e.g. "paths/001.h5"
#     :param prt_name:    name of particle, eg. "e" or "p". (Is a group in hdf-file.)
#     :param charges:     eg. 1.0 for "p"
#     :param positions:   list of coordinate-lists
#                         e.g. [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]]
#     :return:
#     """
#
#     if len(positions[0]) != 3:
#         raise Exception("Assert! Positions of particles were not given in double nested list!")
#     if file_name[0:5] != "paths":
#         raise Exception("Assert! writing somewhere else than 'paths/' folder!")
#
#     if not os.path.exists("paths"):
#         os.makedirs("paths")
#     else:
#         raise Exception("Assert, directory 'paths/' already exists! Not overwriting that.")
#
#
#     h5file = h5py.File(file_name, 'w')     # open the file
#
#     species_group = h5file.create_group(prt_name)
#
#     for i, prt_coord in enumerate(positions):
#         prt = "p{}".format(i+1)
#         crd = np.array(prt_coord)
#         # coordinate is in doubly nested format in hdf-file. (Trotter == 1)
#         crd = np.expand_dims(crd, axis=0)
#
#
#         if np.shape(crd) != (1,3):
#             raise Exception("Assert! That's not a coordinate! See documentation above")
#
#         prt_group = species_group.create_group(prt)
#
#         ds = prt_group.create_dataset("r", data=crd)
#         ds.attrs.create("reset", np.array([0]))  # set attribute
#
#     h5file.close()


def overwrite_paths_to_fixed_positions(positions, prt_name="p", file_name="paths/001.h5"):
    """
    Overwirtes staric nucleus positions to existing file "paths/001.h5". So you must run dummy
    run on pimc to create "paths/001.h5" (one second of run time is well enough).

    :param file_name:   e.g. "paths/001.h5"
    :param prt_name:    name of particle, eg. "e" or "p". (Is a group in hdf-file.)
    :param charges:     eg. 1.0 for "p"
    :param positions:   list of coordinate-lists
                        e.g. [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]]
    :return:
    """

    if len(positions[0]) != 3:
        raise Exception("Assert! Positions of particles were not given in double nested list!")
    if file_name[0:5] != "paths":
        raise Exception("Assert! writing somewhere else than 'paths/' folder!")

    if not os.path.exists(file_name):
        raise Exception("Assert! You must have existing paths-file over which " +
                        "static paths are written. Sorry for inconvenience.")

    h5file = h5py.File(file_name, 'r+')     # open the file

    species_group = h5file.get(prt_name)

    for i, prt_coord in enumerate(positions):
        prt = "p{}".format(i+1)
        crd = np.array(prt_coord)
        # coordinate is in doubly nested format in hdf-file. (Trotter == 1)
        crd = np.expand_dims(crd, axis=0)


        if np.shape(crd) != (1,3):
            raise Exception("Assert! That's not a coordinate! See documentation above")

        if prt not in species_group.keys():
            raise Exception("Your previous '00X.h5' file must be exactly similar" +
                            "to one you are trying to manipulate (same num of particles)")

        prt_group = species_group.get(prt)

        ds = prt_group["r"]
        ds[...] = crd

        # OR:
        #del prt_group["r"]
        #ds = prt_group.create_dataset("r", data=crd)

        #ds.attrs.create("reset", np.array([0]))  # set attribute

    h5file.close()
    
def overwrite_electron_paths_to_fixed_positions(positions, prt_name="p", file_name="paths/001.h5"):
    """
    Overwirtes staric nucleus positions to existing file "paths/001.h5". So you must run dummy
    run on pimc to create "paths/001.h5" (one second of run time is well enough).

    :param file_name:   e.g. "paths/001.h5"
    :param prt_name:    name of particle, eg. "e" or "p". (Is a group in hdf-file.)
    :param charges:     eg. 1.0 for "p"
    :param positions:   list of coordinate-lists
                        e.g. [[0.0, 0.0, 0.0], [1.0, 0.0, 0.0]]
    :return:
    """

    if len(positions[0]) != 3:
        raise Exception("Assert! Positions of particles were not given in double nested list!")
    if file_name[0:5] != "paths":
        raise Exception("Assert! writing somewhere else than 'paths/' folder!")

    if not os.path.exists(file_name):
        raise Exception("Assert! You must have existing paths-file over which " +
                        "static paths are written. Sorry for inconvenience.")

    h5file = h5py.File(file_name, 'r+')     # open the file

    species_group = h5file.get(prt_name)

    for i, prt_coord in enumerate(positions):
        prt = "p{}".format(i+1)
        crd = np.array(prt_coord)
        # coordinate is in doubly nested format in hdf-file. (Trotter == 1)
        crd = np.expand_dims(crd, axis=0)
        if np.shape(crd) != (1,3):
            raise Exception("Assert! That's not a coordinate! See documentation above. Yours is {}".format(np.shape(crd)))

        if prt not in species_group.keys():
            raise Exception("Your previous '00X.h5' file must be exactly similar" +
                            "to one you are trying to manipulate (same num of particles)")

        prt_group = species_group.get(prt)

        ds = prt_group["r"]
        trotter = ds[...].shape[0]
        crd = np.repeat(crd, trotter, axis=0)
        ds[...] = crd

        # OR:
        #del prt_group["r"]
        #ds = prt_group.create_dataset("r", data=crd)

        #ds.attrs.create("reset", np.array([0]))  # set attribute

    h5file.close()

