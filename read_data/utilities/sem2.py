import numpy as np
from typing import Tuple
"""
For more information, see Kylänpää's Thesis p.27
"""

"""
This is pythonized version of matlab script sem2.m written by Ilkka Kylämpää and 
Juha Tiihonen in project pimc3. This file may contain bugs, but basic usage is 
tested to agree with original script. However it may be outdated from original 
project. 
"""


def C(dp, i, N, avg, variance) -> float:
    """
    Discrete estimate for autocorrelation time.
    """
    coeff = 0
    for j in range(N-i): # -1
        coeff = coeff + (dp[j]-avg) * (dp[j+i]-avg)
    #print("coeff", coeff)
    #print("N, i, variance", N,i,variance)
    coeff = coeff/((N-i) * variance)
    #print("coeff2", coeff)
    return coeff


def corrtime(dp) -> float:
    """
    Correlation time.
    """
    avg = np.mean(dp)
    N = len(dp)
    variance = np.var(dp, ddof=1)
    #print("variance",variance,"dp",dp)
    kappa = 0.0

    for i in range(N):
        coef = C(dp, i+1, N, avg, variance) # "i+1" because indexing in python starts from 0:
        if (coef < 0):
            break
        else:
            kappa = kappa + coef
        #print("kappa", kappa)

    kappa = 1 + 2*kappa
    return kappa


def sem2(data, st: int=1, en: int=None) -> Tuple[float, float]:
    """
    Calculates the standard error of mean (SEM). Error is increased by the
    correlation of samples. Note: the error that is returned, corresponds sigma-1
    confidence, which is about 67%. For fully random data, error is same as np.std(a)

    NOTICE! If INDEXING OF `st` and `en` INPUTS STARTS FROM 1

    :param data: One dimensional observable-data of variating blocks.
    :param st:  start-index in which data is converged, (indexing starts from 1)
    :param en:  end-index (inclusive, indexing starts from 1)
    :return: mean, error (including autocorrelation)
    """
    
    if len(data.shape) != 1:
        raise Exception("sem2 works only with 1d data!")

    en = (en or len(data))  # or-shortcut-syntax: if en==None then en=len(data)

    dat = data[st-1:en] # array slice

    E = np.mean(dat)
    if not np.all(np.isclose(dat, np.repeat(dat[0], len(dat)), atol=0.0, rtol=1e-10)):
        Err = np.std(dat, ddof=1) * np.sqrt(corrtime(dat) / len(dat))
    else: # All values are the same
        Err = 0.0

    return float(E), float(Err)
