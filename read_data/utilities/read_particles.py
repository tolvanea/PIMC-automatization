from typing import List, Tuple, Dict

def particle_info(file_name="sample/PARTICLES") -> Dict[str, Tuple[int, float, float, int, bool]]:
    """
    Return all information of particles by label
    i.e. For H2: {"e": (2, -1.0, 1.0, 5, 0), "p": (2, -2.0, 1836.152804, 5, 0)}
    if multilevel is 0, then it is fixed particle, and mass is set to infinity.

    :param file_name:       path to file (string)
    :returns    label, charge, mass, initial multilevel, restricted path
    """

    particles = {}

    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if len(parts) != 6:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

            try:
                label = parts[0]
                if int(parts[4]) == 0:  # If adiabatic, then mass is infinite
                    mass = float("inf")
                else:
                    mass = float(parts[3])
                # count, charge, mass, multilevel, restricted paths
                info = (int(parts[1]), float(parts[2]), mass, int(parts[4]), int(parts[5]))
                particles[label] = info
            except ValueError:
                raise ValueError("Malformed PARTICLES-file!")

    if len(particles) == 0:
        raise Exception("Somethings badly wrong in particle counting. Zero moving particles?")

    return particles

def dynamic_particle_count(file_name = "./PARTICLES") -> int:
    """
    Read and count the number of dynamic (moving) particles in system.
    :param file_name:       path to file (string)
    """

    num_movin_prt = 0

    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if len(parts) != 6:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

            try:
                num_prt = int(parts[1])
                is_moving = (int(parts[4]) != 0)
                if is_moving:
                    num_movin_prt += num_prt
            except ValueError:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

    if num_movin_prt == 0:
        raise Exception("Somethings badly wrong in particle counting. Zero moving particles?")

    return num_movin_prt


def particle_labels(file_name="sample/PARTICLES") -> Tuple[List[str], List[str]]:
    """
    Return all particle labels and number counts,
    i.e. ["eu", "ed", "p"] [2, 1, 1]

    :param file_name:       path to file (string)
    """

    labels = []
    counts = []

    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if len(parts) != 6:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

            try:
                num_prt = int(parts[1])
                labels.append(parts[0])
                counts.append(num_prt)
            except ValueError:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

    if len(labels) == 0:
        raise Exception("Somethings badly wrong in particle counting. Zero moving particles?")

    return labels, counts



def detect_node_surfaces(file_name = "./PARTICLES") -> bool:
    """
    Retutrns True if kinetic node surfaces exists in PARTICLES file
    :param file_name:       path to file (string)
    """

    nodes = None

    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if len(parts) != 6:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

            try:
                block = int(parts[5])
                if (block == 0):
                    nodes = False
                else:  # found one!
                    nodes = True
                    break
            except ValueError:
                raise ValueError("Malformed PARTICLES-file!")
                #continue  # something's wrong in file

    if nodes is None:
        raise Exception("Somethings badly wrong in particles file. Zero particles?")

    return nodes
