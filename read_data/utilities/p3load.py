import numpy as np
import matplotlib.pyplot as plt

import util
import misc
import sem2

"""
This is pythonized version of matlab script p3load.m written by Juha Tiihonen in
project 'pimc3'. However this file may contain bugs and the most probably, is
outdated from orginal project. 

Actually, if it seems to be correct, this function is not used anywhere in 
the project. However 'detect_hdf5_file_and_group()' uses some parts of it.
"""



def p3load(obsname="Et", directory="./", numprocs=None, showplot=0, showall=False):
    """
    PIMC 3 suite: load any data
    
    Load pimc3 observables by name. (Observables are stored in hdf5-containers.)
    
    :param obsname:     Name of observable, e.g. "Et"
    :param directory:   Directory to search observable files. Path obs/00x/ is auto detected.
    :param numprocs:    Number of processors in calculation (selects observable-directory)
    :param showplot:    Shows
    :param showall:     Return full vector (instead of starting from S)
    :return: ave,           average from sem2
             err,           error from sem2
             obs,           data vector ((1,N)-vector is converted to 1d vector)
             axl            axis for grid-values
    
    >>> ave, err, obs, axl = p3load("Et")
    """

    ave = None
    err = None
    obs = None          #  data np.ndarray
    axl = float("nan")

    # choose groups of data
    tp = 'd'       # dat-files by default
    ft = False     # no fourier transform by default

    if (obsname in ['Et', 'Ek', 'Ep', 'Vt', 'Vp', 'Vk']):
        filename = 'energy'
        group = '/E'
        names = [obsname]
    elif (obsname =='mu1'):
        filename = 'statpol'
        group = '/POL'
        names = ['mu1_1', 'mu1_2', 'mu1_3']
    elif (obsname =='mu2'):
        filename = 'statpol'
        group = '/POL'
        names = ['mu2_11', 'mu2_22', 'mu2_33']
    elif (obsname =='mu2t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['mu2']
        axlname = ['time']
        tp = 'g'
    elif (obsname =='theta2t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['theta2']
        axlname = ['time']
        tp = 'g'
    elif (obsname =='omega2t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['omega2']
        axlname = ['time']
        tp = 'g'
    elif (obsname =='alpha1t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['mu2']
        axlname = ['time', 'omega']
        ft = 1
        tp = 'g'
    elif (obsname =='alpha2t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['theta2']
        axlname = ['time', 'omega']
        ft = 1
        tp = 'g'
    elif (obsname =='alpha3t'):
        filename = 'dynpol'
        group = '/DP'
        names = ['omega2']
        axlname = ['time', 'omega']
        ft = 1
        tp = 'g'
    else:
        filename = 'statpol'
        group = '/POL'
        names = [obsname]

    # set directory
    if (numprocs == None):
        # Autodetects the correct directory obs/ or obs/00x/
        file = misc.get_obs_dir(directory) + filename + '.h5'
    elif (numprocs == 1):
        file = directory + 'obs/' + filename + '.h5'
    else:
        file = directory + 'obs/' + '{:03}'.format(numprocs) + '/' + filename + '.h5'

    # read data
    if (tp == 'd'):  # dat
        ln = len(names)
        for i in range(ln):
            nm = names [i]

            data, idx, S = util.p3dat(file, group, nm, showall=showall)

            if (i == 0):  # create empty return-arrays, (dimension are now known)
                ave = np.full(ln, float("nan"))
                err = np.full(ln, float("nan"))
                obs = np.full((ln, len(data)), fill_value=float("nan"))
            obs[i,:] = data

            ave[i], err[i] = sem2.sem2(obs[i, S:])

            if (showplot):
                fig, ax1 = plt.subplot(1,1, figsize=(5,4))

                ax1.plot(idx[0:S], obs[i, 0:S], 'r', idx[S:], obs[i, S:], 'b')

                # show mean
                ax1.plot([idx[0], idx[-1]], [ave[i], ave[i]], '--k')
                ax1.plot([idx[0], idx[-1]], [ave[i], ave[i]] - err(i), ':k')
                ax1.plot([idx[0], idx[-1]], [ave[i], ave[i]] + err(i), ':k')
                ax1.title(file + ': ' + nm + ' ' + str(numprocs) + ' cores')
                plt.show()

    else:  # grid
        data, idx, S = util.p3grid(file, group, names[0], showall=showall)

        obs = np.empty((data.shape), data.dtype)
        ave = np.full((obs.shape[1], obs.shape[0]), float("nan"))
        err = np.full((obs.shape[1], obs.shape[0]), float("nan"))

        if (ft):
            axl     = util.p3axis(file, group, axlname[1])
            axl_tau = util.p3axis(file, group, axlname[0])
            tau = np.mean(axl_tau[1:] - axl_tau[0:-2])

            for i in range(np.shape(data)[0]):
                for k in range(np.shape(data)[2]):
                    obs[i,:, k] = tau * np.fft(np.squeeze(data[i,:, k]))

            obs = np.real(obs)

        else:
            axl = util.p3axis(file, group, axlname[0])
            obs = data

        for j in range(np.shape(obs)[1]):
            for i in range(np.shape(obs)[0]):
                ave[i, j], err[i, j] = util.sem2(np.squeeze[obs[i, j, S:]])

        if showplot:
            # get a limit
            for i in range(1,len(ave)):
                if (ave[i] < (ave[0] / 20)):
                    xlimit = axl(i)
                    break

            plt.figure()
            for i in range(np.shape(ave)[0]):
                plt.plot(axl, np.squeeze(ave[i,:]), '-')

            plt.set_xlim([-1, 1] * xlimit)
            plt.set_title(file + ': ' + names[0] + ' ' + str(numprocs) + ' cores')
            plt.show()

    # convert scalar values
    if (ave.shape == (1,)):
        ave = np.asscalar(ave)
        err = np.asscalar(err)
    # convert column vector to row vector
    if obs.shape[0] == 1 and obs.shape[1] > 1:
        obs = np.squeeze(obs.transpose())

    return ave, err, obs, axl