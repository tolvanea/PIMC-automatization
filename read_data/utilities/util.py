import numpy as np
import h5py

"""
This is pythonized version of matlab scripts p3axis.m p3dat.m and p3grid.m 
written by Juha Tiihonen in project pimc3. However this file may contain bugs 
and the most probably, is outdated from orginal project. 

Actually, if it seems to be correct, these functions are not used anywhere in 
my project.
"""


def p3axis(file, group, name):
    """
    PIMC 3 suite: load grid axis, e.g. PC/e_e/r
    (Can also be used to load any other vector.)

    [axis] = p3axis(file,group,name)

    :param file:    relative path to *.h5 file to open
    :param group:   Group is e.g. for energy "/E"
    :param name:    Name of data is e.g. "Et"
    :return: axis   numpy array of of data
    """
    dset = group + '/' + name

    axis = h5py.File(file, 'r').get(dset)
    return np.array(axis)


def p3dat(file, group, name, showall=True):
    """
    PIMC 3 suite: load dat-observable

    Loads hdf5 data.

    [obs,idx,S] = p3dat(file,group,name,showall)

    idx contains block indices for adjusting S
    S is given, if requested

    :param file:    Relative path to *.h5 file to open
    :param group:   Group is e.g. for energy "/E"
    :param name:    Name of data is e.g. "Et"
    :param showall: Return full vector (instead of starting from S)
    :return:  obs,  Observalbe datavector (as numpy array)
              idx,  index-vector from S to N. (INDEXING STARTS FROM 0)
              S     Start position (INDEXING STARTS FROM 0)
    """

    dset = group + '/' + name

    hf = h5py.File(file, 'r')
    data_group = hf.get(dset)
    data = np.array(data_group)

    if (data is None):
        pass

    N = data_group.attrs.get('N')
    S = data_group.attrs.get('S')
    if N is None:
        raise Exception(
              "No attribute {} found in data group: {}".format("N", dset))
    N = int(N)
    S = int(S)
    S = S - 1

    if showall:
        S=0

    idx = np.arange(S, N)
    obs = data[S:N]

    return obs, idx, S


def p3grid(file, group, name, showall=1):
    """
     PIMC 3 suite: load grid data

     # This file is used for example loading e.g pair_correlation 'PC/e_p/PC'.

     >>>obs, idx, S = p3grid(file, group, name, showall)

     idx contains block indices for adjusting S
     S is given, if requested

    :param file:    Relative path to *.h5 file to open
    :param group:   Group is e.g. for energy "/E"
    :param name:    Name of data is e.g. "Et"
    :param showall: Return full vector (instead of starting from S)
    :return:        (NOTICE! INDEXIN STARTS FROM 0)
            obs,    Observalbe datavector (as numpy array)
            idx,    index-vector from S to N. (INDEXING STARTS FROM 0)
            S       Start position (INDEXING STARTS FROM 0)
    """

    dset = group + '/' + name

    hf = h5py.File(file, 'r')
    data_group = hf.get(dset)
    data = np.array(data_group)

    N = data_group.attrs.get('N')
    S = data_group.attrs.get('S')
    if (N is None) or (S is None):
        raise Exception(
            "No attribute {} found in data group: {}".format("N", dset))

    N = int(N)
    S = int(S)
    S = S - 1

    if showall:
        S = 0

    idx = np.arange(S, N)
    obs = np.squeeze(data[:, :, :, S:N])

    return obs, idx, S