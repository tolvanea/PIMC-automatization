
format long

addpath('/home/tolvanea/levy/koodit/pimc3/mfiles/')


% test energy p3dat
if 0
    obsname = 'Vt';  % change this Et, Ep, Ek...
    filename = 'energy';
    group = '/E';
    nm = obsname;


    file = ['./' 'obs/' sprintf('%03d',2) '/' filename '.h5' ];


    disp('p3dat')

    [data,idx,S] = p3dat(file,group,nm,0);

    data_res = [data(1), data(round(length(data)/2)), data(end), sum(data)];
    idx_res = [idx(1), idx(round(length(idx)/2)), idx(end), sum(idx)];

    sprintf('%.4f,' , data_res)
    sprintf('%d,' , idx_res)
    S
end

% p3grid
if 0
    obsname = 'e_e/PC';  % change this e_p/PC
    filename = 'paircorrelation';
    group = '/PC';
    nm = obsname;
    file = ['./' 'obs/' sprintf('%03d',2) '/' filename '.h5' ];
    
    disp('p3grid')
    
    [data,idx,S] = p3grid(file,group,nm,0);
    
    data_res = [data(1,1), data(round(length(data)/2),1), data(end,1), sum(data(1,:))];
    idx_res = [idx(1), idx(round(length(idx)/2)), idx(end), sum(idx)];

    disp(size(data_res))
    sprintf('%.6f, ' , data_res)
    sprintf('%d, ' , idx_res)
end

% p3axis
if 0    
    obsname = 'e_p/r';  % change this e_p/r
    filename = 'paircorrelation';
    group = '/PC';
    nm = obsname;
    file = ['./' 'obs/' sprintf('%03d',2) '/' filename '.h5' ];
    
    disp('p3axis')
    
    data = p3axis(file,group,nm);
    
    data_res = [data(1), data(round(length(data)/2)), data(end), sum(data)];
    idx_res = [idx(1), idx(round(length(idx)/2)), idx(end), sum(idx)];

    sprintf('%.6f, ' , data_res)
    %sprintf('%d, ' , idx_res)
end

if 1        
    disp('p3load')
    
    nm = 'Ek'  % change Ek, Ep...
    
    [ave, err, obs] = p3load(nm, './', 2);
    
    obs = [obs(1), obs(round(length(obs)/2)), obs(end), sum(obs)];
    
    ave
    err
    sprintf('%.6f, ' , obs)
end
