import unittest
import numpy as np

import sys
#sys.path.append("..")
sys.path.append("/home/alpi/kesa_2018/koodit/python/hdf5_luku")
import util
import sem2
import misc
import p3load

"""
This file contains tests that compare python version outputs to results of
original matlab scripts.
"""

# results data from matlab_test.m:

matlab_p3dat_energy = {
    'Et':{
# only few values are tested, because vectors take a lot of copy-paste-text space
        #         first   half-way   last     sum
        'data' : np.array([-2.8238, -2.8834, -3.0170, -293.5403]),
        'idx'  : np.array([1,50,100,5050]),
        'S'    : 1
    },
    'Ep':{
        'data' : np.array([-5.4209,-5.6241,-5.5727,-560.1871]),
        'idx'  : np.array([1,50,100,5050]),
        'S'    : 1
    },
    'Ek':{
        'data' : np.array([2.5972,2.7406,2.5558,266.6469]),
        'idx'  : np.array([1,50,100,5050]),
        'S'    : 1
    },
    'Vt':{
        'data' : np.array([-2.8005,-2.9123,-2.8776,-290.3628,]),
        'idx'  : np.array([1,50,100,5050]),
        'S'    : 1
    }
}
"""
matlab_p3dat_paircorrelation = {
# p3dat /PC/e_e/PC
    'e_e/PC':{
        'data' : [0.001202, 0.000000, 0.000000, 1.000000],
        'idx'  : [1, 50, 100, 5050],
        'S'    : 1
    },
    'e_p/PC':{
        'data' : [0.013709, 0.000000, 0.000000, 1.000000],
        'idx'  : [1, 50, 100, 5050],
        'S'    : 1
    }
}
"""
matlab_p3grid_paircorrelation = {
# p3grid /PC/e_X/PC
    'e_e/PC':{
        'data' : [0.001202, 0.000000, 0.000000, 0.159905],
        'idx'  : [1, 50, 100, 5050],
        'S'    : 1
    },
    'e_p/PC': {
        'data' : [0.013709, 0.000000, 0.000000, 1.610413],
        'idx'  : [1, 50, 100, 5050],
        'S'    : 1
    }
}
matlab_p3axis_paircorrelation = {
# p3axis /PC/e_X/r
    'e_e/r':
        {'data' : [0.074257, 7.500000, 14.925743, 757.500000]},
    'e_p/r':
        {'data' : [0.074257, 7.500000, 14.925743, 757.500000]}
}

matlab_p3load_energy = {
# p3load
    'Et' : {
        'ave' :   -2.935402728297358,
        'err' :    0.028302162315903,
        'obs' :  [-2.823750, -2.883442, -3.016991, -293.540273]
    },
    'Ek' : {
        'ave' : 2.666468553356450,
        'err' : 0.039927409556503,
        'obs' : [2.597195, 2.740620, 2.555751, 266.646855]
    }
}


def is_close(a:float, b:float) -> bool:
    c = abs(min(a,b))
    return np.abs(a - b) < 1e-12*c


def p3dat_is_correct(data, idx, S, obsname, matlab_dict, no_middle_idx=False):
    """
    This function test whether calculated python results match to matlab results.

    :param data,idx, S:   python result
    :param obsname:       dictionary key
    :param matlab_dict:   dictionary of matlab results
    :param no_middle_idx: do not take middle and last index into account
                          (matlabs unintuitive indexing causes sometimes problems)
    :return:
    """

    # python results:  first        half-way                  last       sum
    p_data = np.array([data[0], data[round(data.shape[0] / 2)-1], data[-1], np.sum(data)]);
    # different indexing in python functions (starts from zero)
    p_idx = np.array([idx[0] + 1, idx[round(len(idx) / 2)-1] + 1, idx[-1]+ 1, np.sum(idx+1)]);

    p_idx = p_idx #
    S = S+1

    # matlab resutls
    m_data = np.array(matlab_dict[obsname]['data'])
    m_idx = matlab_dict[obsname]['idx']
    m_S = matlab_dict[obsname]['S']

    if no_middle_idx:
        same_data = np.sum(np.abs(m_data[[0,3]] - p_data[[0,3]])) < 0.001
    else:
        same_data = np.sum(np.abs(m_data-p_data)) < 0.001
    same_idx = not np.any(m_idx != p_idx)
    same_S = (S == m_S)

    if (np.shape(same_data) != () or np.shape(same_idx) != ()):
        raise Exception("This data is not 1-dimensional!")

    B = same_data and same_idx and same_S
    if not B:
        print(("same_data {}, python: {}\n" +
               "                matlab: {}\n").format(same_data, p_data, m_data))
        print(("same_idx {},  python: {}\n" +
               "                matlab: {}\n").format(same_idx, p_idx, m_idx))
        print(("same_S {}     python: {}\n" +
               "                matlab: {}\n").format(same_S, S, m_S))

    return B


def p3axis_is_correct(data, obsname, matlab_dict):
    """
        This function test whether calculated python results match to matlab results.

        :param data:                python result
        :param obsname:             dictionary key
        :param matlab_dict:         dictionary of matlab results
        :return:
        """
    # python results:  first        half-way                  last       sum
    p_data = np.array([data[0], data[round(len(data) / 2)-0], data[-1], np.sum(data)]);

    # matlab resutls
    m_data = matlab_dict[obsname]['data']

    same_data = np.sum(np.abs(m_data-p_data)) < 0.001

    B = same_data
    if not B:
        print("same_data {}".format(same_data))
        print("    python: {}\n    matlab: {}".format(p_data, m_data))

    return B


class TestUtils(unittest.TestCase):

    def test_p3dat_energy(self):
        filename = "energy"
        group = "/E"
        file = './' + 'obs/' + '{:03}'.format(2) + '/' + filename + '.h5';

        for name in ['Et', 'Ek', 'Ep', 'Vt']:
            data, idx, S = util.p3dat(file, group, name, showall=False);

            B = p3dat_is_correct(data, idx, S, name, matlab_p3dat_energy)
            if not B:
                print("Name:", name, "\n")

            self.assertTrue(B)

    """ 
    # Script was mot designed for this.
    def p3dat_test_paircorrelation(self):
        filename = "paircorrelation"
        group = "/PC"
        file = './' + 'obs/' + '{:03}'.format(2) + '/' + filename + '.h5';

        for name in ['e_e/PC', 'e_p/PC']:
            data, idx, S = util.p3dat(file, group, name, showall=False);

            B = p3dat_is_correct(data, idx, S, name, matlab_p3dat_paircorrelation)
            if not B:
                print("Name:", name, "\n")

            self.assertTrue(B)
    """

    def test_p3grid(self):
        filename = "paircorrelation"
        group = "/PC"
        file = './' + 'obs/' + '{:03}'.format(2) + '/' + filename + '.h5';

        for name in ['e_e/PC', 'e_p/PC']:
            data, idx, S = util.p3grid(file, group, name, showall=False);

            # Function 'p3dat_is_correct()' works for p3grid data too, (because
            # the data is modified to the dictionary matlab_p3grid_paircorrelation)
            B = p3dat_is_correct(data[:,0], idx, S, name, matlab_p3grid_paircorrelation, True)
            if not B:
                print("data.shape", data.shape)
                print("idx.shape", idx.shape)
                print("Name:", name, "\n")

            self.assertTrue(B)

    def test_p3axis(self):
        filename = "paircorrelation"
        group = "/PC"
        file = './' + 'obs/' + '{:03}'.format(2) + '/' + filename + '.h5';

        for name in ['e_e/r', 'e_p/r']:
            data = util.p3axis(file, group, name);

            B = p3axis_is_correct(data, name, matlab_p3axis_paircorrelation)
            if not B:
                print("Name:", name, "\n")

            self.assertTrue(B)


class TestSem2(unittest.TestCase):
    def test_sem2_simple(self):
        A = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        mean, err = sem2.sem2(A)
        # correct results:
        m = 5.50000000000000
        e = 3.68781778291716
        B = is_close(m, mean) and is_close(e, err)
        if not B:
            print("Correct, {},{}   my{},{}".format(m, e, mean, err))
        self.assertTrue(B)

    def test_sem2_no_autocorrelation(self):
        A = np.sin(np.linspace(0,100000,1000))
        mean, err = sem2.sem2(A)
        # correct results:
        m = -0.00455045829612308
        e = 0.0966809805226778
        B = is_close(m, mean) and is_close(e, err)
        if not B:
            print(("Correct, {},{}\n" +
                   "my       {},{}\n" +
                   "diff     {},{}\n" +
                   "is_close {},{}").format(m, e,
                                            mean, err,
                                            m-mean, e-err,
                                            is_close(m, mean), is_close(e, err)))
        self.assertTrue(B)


def p3load_is_correct(ave, err, obs, obsname, matlab_dict):
    """
        This function test whether calculated python results match to matlab results.

        :param data:                python result
        :param obsname:             dictionary key
        :param matlab_dict:         dictionary of matlab results
        :return:
        """
    # python results:  first        half-way                  last       sum
    p_obs = np.array([obs[0], obs[round(len(obs) / 2)-1], obs[-1], np.sum(obs)]);

    # matlab resutls
    m_ave = matlab_dict[obsname]['ave']
    m_err = matlab_dict[obsname]['err']
    m_obs = matlab_dict[obsname]['obs']

    same_ave = is_close(m_ave, ave)
    same_err = is_close(m_err, err)
    same_obs = np.sum(np.abs(m_obs-p_obs)) < 0.001

    if (np.shape(same_ave) != () or np.shape(same_obs) != ()):
        print("shape obs", obs.shape)
        print("ave, err, obs", ave, err, obs)
        raise Exception("This data is not 1-dimensional!")

    B = same_obs and same_ave and same_err
    if not B:
        print("same_obs {}    python: {}\n    matlab: {}".format(same_obs, p_obs, m_obs))
        print("same_ave {}    python: {}\n    matlab: {}".format(same_ave, ave, m_ave))
        print("same_err {}    python: {}\n    matlab: {}".format(same_err, err, m_err))

    return B


class Testp3load(unittest.TestCase):

    def TODO(self):
        group = "/E"
        self.assertTrue(True)  # TODO

    def energies(self):
        for name in ['Et', 'Ek']:
            ave, err, obs, axl = p3load.p3load(name);

            #print("ave, err, obs, axl", ave, err, obs, axl)

            B = p3load_is_correct(ave, err, obs, name, matlab_p3load_energy)
            if not B:
                print("Name:", name)
            self.assertTrue(True)

    def data(self):
        self.assertTrue(True)  # TODO

    def others(self):
        self.assertTrue(True)  # TODO


def run_tests_manually():
    tst_utils = TestUtils()
    tst_utils.test_p3dat_energy()
    tst_utils.test_p3grid()
    tst_utils.test_p3axis()

    tst_sem2 = TestSem2()
    tst_sem2.test_sem2_simple()
    tst_sem2.test_sem2_no_autocorrelation()

    tst_p3load = Testp3load()
    tst_p3load.energies()


if __name__ == '__main__':
    unittest.main()
    #run_tests_manually()