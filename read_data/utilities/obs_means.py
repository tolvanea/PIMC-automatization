##!/usr/bin/env python3
#
# This script is heavily based on
#   pimc3/p3compress.py
#   and
#   https://stackoverflow.com/questions/28170623/how-to-read-hdf5-files-in-python
#
#   proper documentation: http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html

help_text = """
Prints contet of all hdf5-files in some directory. For every array prints sizes,
mean, variaces. If data contain attributes 'S', 'N' prints sem2-autocorrelations
within that range.

Used to inspect results of pimc-calulation with quickly.


Usage:
    1. Change directory to
            my_calulation/          (prints all files in obs/)
        or
            my_calulation/obs/
        or
            my_calulation/paths/    (prints all files in pats/)

    2. Print all values of all observable-files
            $ python3 -m read_data.utilities.obs_means
       Print all values of selected observable-files
            $ python3 -m read_data.utilities.obs_means energy.h5 statmagn.h5


"""

import h5py
import numpy as np
import sys
import os
import read_data.utilities.misc as misc
import read_data.utilities.sem2 as sem2


def print_attrs(name, obj):
    ident = name.count("/") + 1

    # dataset
    if isinstance(obj, h5py.Dataset):

        print("    " * (ident) + "Is dataset: {}".format(name))

        # main data
        print("    " * (ident + 1) + "Main data: {}".format(""))
        dataset = obj
        arr = np.array(dataset)
        print("    " * (ident + 2), "Shape: {}".format(arr.shape))
        keys = obj.attrs.keys()
        if (len(arr) == 0):
            print("    " * (ident + 2), "Empty array")
        elif (len(arr) == 1):
            print("    " * (ident + 2), "Value : {}".format(arr))
        elif (len(arr.shape) == 1):
            if (('S' in keys) and ('N' in keys)):
                S = int(obj.attrs.get("S"))
                N = int(obj.attrs.get("N"))
                #S_N_mean = arr[S:N].mean()
                S_N_var = arr[S:N].std()
                S_N_mean, S_N_err = sem2.sem2(arr, S, N)
                last_half_mean, last_half_err = sem2.sem2(arr, N//2, N)
                last_half_var = arr[N//2: -1].std()
                print("    " * (ident + 2), "Mean S–N:{: .5f},  N/2–N:{: .5f}".
                      format(S_N_mean, last_half_mean))
                print("    " * (ident + 2), "Err  S–N:{: .5f},  N/2–N:{: .5f}".
                      format(S_N_err, last_half_err))
                print("    " * (ident + 2), "Var  S–N:{: .5f},  N/2–N:{: .5f}".
                      format(S_N_var, last_half_var))
            else:
                latter_half_mean = arr[len(arr)//2: -1].mean()
                latter_half_var = arr[len(arr)//2: -1].std()
                print("    " * (ident + 2), "Mean: {: .4f}    last 10: {: .4f}".
                      format(arr.mean(), latter_half_mean))
                print("    " * (ident + 2), "Var:  {: .4f}    last 10: {: .4f}".
                      format(arr.std(), latter_half_var))

            if len(arr) < 10:
                print("    " * (ident + 2), "Array:")
                print(arr)

        # attributes
        print("    " * (ident + 1) + "Attributes:")
        for item, val in obj.attrs.items():
            print("    " * (ident + 2) + "{:8} {}".format(item, val))

        #print(arr[0:min(len(arr), 6)])
    else:
        print("    " * (ident), "Is group:   {}".format(name))

    print("")

def print_means(args):

    try:
        directory = misc.get_obs_dir("./")
    except FileNotFoundError:  # directory 'obs/' not found, use current directory instead
        directory = "./"

    if len(args) == 1:
        files = [filename for filename in os.listdir(directory) if filename.endswith(".h5")]
    else:
        files = args[1:]

    for filename in files:
        print("Visiting: " + filename)
        # open in h5py
        fin = h5py.File(directory + filename, "r")
        grpin = fin['/']
        grpin.visititems(print_attrs)

        fin.close()


def main():
    if (len(sys.argv) == 2) and sys.argv[1] == "--help" :
        print(help_text)
        return

    print_means(sys.argv)

if __name__ == "__main__":
    main()
