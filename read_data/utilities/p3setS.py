#!/usr/bin/env python3


"""
This file is based on the one from git@gitlab.com:qcad.fi/pimc3.git written
by Juha Tiihonen. This file may be outdated
"""

import h5py
import sys
import os


def main(S: int = 1, directory: str = './') -> None:

    def setS(grpin):
        dset = fin[grpin]
        if not dset.attrs:
            return None
        elif not 'N' in dset.attrs.keys():  # not an observable
            return None

        N = int(dset.attrs.get('N'))
        if S <= N:
            dset.attrs.modify('S', S)
        else:
            print("S cannot be greater than N ({}), S:{}, N:{}"
                  .format(dset.name, S, N))

    found = False
    for filename in os.listdir(directory):
        if filename.endswith(".h5") and not filename.startswith("compressed"):
            found = True
            # open in h5py
            fin = h5py.File(directory + filename, "a")
            grpin = fin['/']
            grpin.visit(setS)

            fin.close()
    if not found:
        raise Exception(
            "No hdf5 files found in'" + os.path.abspath(directory) + ".")

if __name__ == "__main__":
    main(*sys.argv)