from pathlib import Path
import os

# change directory to observables
def get_obs_dir(path0):
    from pathlib import Path
    """
    Changes the directory to observables and auto detects if 'obs/00x' exists.
    
    Call this function for for `path0` being 'my_calculation' or 'my_calculation/obs' or 
    'my_calculation/obs/00x' and it returns 'my_calculation/obs' (or if multithreaded, then it 
    returns my_calculation/obs/00x ). Here 'my_calcualtion' is the calculation directory, which 
    contains e.g. files CONF and PARTICLES.
    
    (I think this function is unnecessarily complex for the task it does.)

    :param path0:             path string, that can be 
    :return: '.../obs/00x/' or '.../obs/' depending if folder 00x exists.
    """
    path = Path(os.path.abspath(path0))

    if os.path.basename(path) == "obs" or os.path.basename(os.path.dirname(path)) == "obs":
        #print("Oh, you are already in obs-directory")
        obs_path = Path(path)
    else:
        dir_content = os.listdir(path)
        obs_path = Path(path.joinpath("obs"))
        # Error checking
        if not (obs_path.is_dir() and ("CONF" in dir_content) and ("PARTICLES" in dir_content)
                and (os.path.basename(path)[0] != "0")):
            print("DEBUGGIN: Your path:", path)
            print("DEBUGGIN: Direcotry does not seem to contain observables 'obs/' 'CONF' or 'PARTICLES'. ")
            print("DEBUGGIN: Your directory contains only:", dir_content)
            raise Exception(
                "This directory does not seem to contain 'obs/' or 'CONF' or 'PARTICLES'.\n" +
                "Your path: " + str(path) + "\n." +
                "Your directory contains: " + str(dir_content) + ".\n" +
                "In other words:\n" +
                "You must be in calculation directory, or in 'obs/', or in 'obs/00x' -directory to run this \n" +
                "command. (BTW, 'calculation directory' is the one that contains 'obs/', 'CONF' and 'PARTICLES'.)\n"
            )

    for file in obs_path.iterdir():
        # Oh cool, found (MPI) multicore observables
        if (file.is_dir() and file.name.startswith("0") and file.name.isnumeric()):
            num_proc_path = file
            break
    else:  # Single core observables
        for file in obs_path.iterdir():
            if (file.is_file() and file.name.endswith(".h5")):
                num_proc_path = obs_path
                break
        else:
            print("read_calc_data.py: No observables found:    ..." + (str(path) + "/obs")[-37:-1] )
            num_proc_path = ""

    return str(num_proc_path) + "/"

def get_calcuation_dir(path0):
    """
    This function resolves calcualtion directory from observable directory. Calculation directory
    is the on that contains "CONF" or "obs/"
    
    This function may a pretty weird, but I don't want to rewrite this path resolving logic again.
    
    :param obs_dir:             Observable directory returned by get_obs_dir()
    """
    obs_dir = get_obs_dir(path0)
    if os.path.basename(obs_dir) == "obs":
        return os.path.dirname(obs_dir) + "/"
    elif os.path.basename(os.path.dirname(obs_dir)) == "obs":
        return os.path.dirname(os.path.dirname(obs_dir)) + "/"
    elif os.path.basename(os.path.dirname(os.path.dirname(obs_dir))) == "obs":
        return os.path.dirname(os.path.dirname(os.path.dirname(obs_dir))) + "/"
    else:
        print("DBG:", path0)
        print("DBG:", obs_dir)
        print("DBG:", os.path.basename(obs_dir))
        print("DBG:", os.path.basename(os.path.dirname(obs_dir)))
        raise Exception("Crap. This error should not be thrown.")
