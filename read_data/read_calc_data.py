import os
import sys
import numpy as np
#import scipy.io as sio
from typing import Tuple, Dict
import json
import pickle
import read_data.utilities.sem2 as sem2
from read_data.utilities.misc import get_obs_dir
from miscellaneous import conf_modification

#import miscellaneous.general_tools as general_tools


"""
This module read pimc3-generated data (obs/XXX.h5) passes it out as a data structure.
"""


def main(argv):
    raise Exception("This is a module!, It is called from plotting functions")
    
    
def initial_error_checking():
    parent_dir_name = os.path.basename(os.getcwd())
    list_dir = os.listdir()
    
    if not ("sample" in list_dir):
        raise Exception("You must run this in same directory with 'sample'. Yours", parent_dir_name)
    if parent_dir_name == "sample":
        raise Exception("'sample' is very misleading name for a parent folder.")


class InitialSetupFromJSON:
     def __init__(self, dictionary):
         # Now just create a class from fly.
         for k, v in dictionary.items():
             setattr(self, k, v)


def read_initial_setup_from_JSON(path='results/metadata/initial_configuration.json',):
    """
    Deprecated, not used anywhere.

    Read file that was created when calculation was initialized. Now includes latex labels

    [Who designed this thing? Oh, it was me? It's stupid anyways. Why not read data directly from
    configure_instance.py ?]
    """
    with open(path, 'r') as file:
        # Tuple[str, str, List[float], List[float], List[str]]
        dump = json.load(file)

        init_setup = InitialSetupFromJSON(dump)

    return init_setup



def read_all_observables(init_setup, observable_name, filename):
    """
    Iterates all directories, reads the given obervable in each of them, and
    then returns matrix of that info. Note: Obserable must be number array, but
    I think hdf5 enforces to that. Arrays can be whatever dimensional, and
    whatever number type. (Examples arrays are: [1], [1.0, 2.0], [[[1, 2]], [[3, 4]]])

    However if the array has attributes `S` and `N`, then it is block-data array, and
    extra information is calculated, most notably `sem2` values "mean" and "err", i.e.
    mean and standard deviation in mean. Some invalid-looking blocks may be cropped out.

    This function only reads hdf5 containers and does not edit them. (Hopefully
    creates less race conditions)

    :param observable_name: - e.g "/E/Et"
    :param filename:        - e.g. "energy.h5"
    :return: data_arr       - matrix of vectors of block observables
                              (size = var1 * var2, item: np.array). Note that if it
                              array of blocks (contains S an N) then garpage values
                              in the end after N are cropped out from returned arrays.
                              (My code elsewhere in this project may do unnecessary cropping
                              with data_arr[:N], but they are just remainders of the past.)
                              Note that values before S are still not cropped out.
             attr_dict      - dictionary of matrices containg data of that simulation
                              keys:
                                    "mean",     mean over all converged blocks, dropping ill values
                                    "err",      sigma-1 error corresponding mean
                                    "S",     <- This is corrected with auto convergence detection!
                                                It is not hdf5 container value!
                                    "N",        Array size, you should no need it as zeros are
                                                cropped out of array ends automatically
                                    "mean_extrapolation_at_range1=0",
                                                Mean extrapolation at tau=0
                                    "err_extrapolation_at_range1=0",
                                                Error extrapolation at tau=0
                              Also it contains:
                                  succeed - bool matrix containing information
                                            whether observable files were found.
                                            TODO currently it's just blank (all positive)
                                                If not needed, then remove it.
                                  name - same as input `observable_name`
                                  tau  - time step
                                  S -
    """
    f = lambda stp, path, i, j: read_single_directory(stp, path, i, j, filename, observable_name)
    data_matrix, attr_matrix = iterate_directories(init_setup, f)

    # will be re_orient_attr_matrix
    data_arr = re_orient_blockdata_matrix(data_matrix, init_setup)
    attr_dict = re_orient_attr_matrix(attr_matrix, init_setup, data_arr, observable_name)

    exception = None
    try:
        if "mean" in attr_dict:
            # Extrapolation creates additional dict items for extrapolated values
            mean_mat = attr_dict["mean"]
            err_mat = attr_dict["err"]
            data0, err0 = extrapolate_range1_at_0(init_setup, mean_mat, err_mat)
            attr_dict["mean_extrapolation_at_range1=0"] = data0
            attr_dict["err_extrapolation_at_range1=0"] = err0
    except Exception as e:
        print("DEBUG: Silently failed extrapolation that was not asked.")
        exception = e
    save_obs_to_cache(init_setup, attr_dict, filename, observable_name)

    return data_arr, attr_dict

def read_generic_information(init_setup, callable):
    """
    Same as 'read_all_observables', but works on general data instead of hdf5 observable.
    Inteded to gather data from CONF or PARTICLES files.
    :param init_setup:
    :param callable:    callable(init_setup, path) -> None, attr_dict
    :return:
    """
    _, attr_matrix = iterate_directories(init_setup, callable)
    attr_dict = re_orient_attr_matrix(attr_matrix, init_setup)

    return attr_dict


def iterate_directories(init_setup, callable):
    """
    Reads hdf5 files from all different calculations, and combines them into one
    matrix. Is designed to work with observables and pair correlation.

    Do minor fixes to array data
        - Crop array values after N
        - If some block is failed (Et/Vt is zero), then replace that observable
          with values of some neighbouring blocks.
    Do minor fixes to attr dict data
        - Indices S and N are integers, (not 1-length arrays)
        - S is replaced S that is convergence detected by using Vt-data.
          So S is not from hdf5 file



    :param init_setup:
    :param callable:    callable(init_setup, path) -> data_arr, attr_dict
    :return:
        data_matrix     hdf5 data array
        attr_matrix     hdf5 attributes in dict
    """
    dir1_labels = init_setup.dir1_labels
    dir2_labels = init_setup.dir2_labels

    it1 = range(len(dir1_labels))
    it2 = range(len(dir2_labels))
    data_matrix = [[None for j in it2] for i in it1]
    attr_matrix = [[None for j in it2] for i in it1]

    for i, dir1 in enumerate(dir1_labels):
        for j, dir2 in enumerate(dir2_labels):
            path = os.getcwd() + "/" + dir1 + "/" + dir2
            if not os.path.isdir(path):
                data_matrix[i][j] = None
                attr_matrix[i][j] = None
                continue

            data, attr_dict = callable(init_setup, path, i, j)

            data_matrix[i][j] = data        # notice: scalars are 1-size vectors
            attr_matrix[i][j] = attr_dict

    return data_matrix, attr_matrix

def read_single_directory(init_setup, path, i, j, filename, group_and_name):
    obs_path = get_obs_dir(path)

    data, attr_dict = read_single_HDF5_dataset(obs_path + filename, group_and_name)
    # Modify data and attr_dict
    new_data = modify_values_from_original(init_setup, data, attr_dict, i, j, group_and_name, path)

    for block in range(len(new_data)):
        if not np.any(new_data[block,...]): # np.any checks if data is `!= 0.0` or `!= [0.0, 0.0, ...]`
            print("CRAP, INTERNAL ERROR, block data contains zeros", path, group_and_name, block)
            assert(False)
    return new_data, attr_dict


def read_single_HDF5_dataset(filepath, group_and_name) -> Tuple[np.ndarray, Dict]:
    """
    Reads any datavector and attributes from hdf5.

    :param filepath:        e.g. obs/001/energy.h5
    :param group_and_name:  e.g. /E/Et
    :return:
    """
    import h5py

    if not os.path.isfile(filepath):
        raise ValueError("File not found: {}".format(filepath))
        #return None, None

    dset = group_and_name
    hf = h5py.File(filepath, 'r')
    check_erros_hdf5(dset, hf, filepath)

    data_set = hf.get(dset)

    attr_dict = {}
    for (name, value) in data_set.attrs.items():
        attr_dict[name] = value

    if "N" not in attr_dict:
        data = np.array(data_set)
    else:
        N = attr_dict["N"][0]
        data = np.array(data_set)[:N]
    attr_dict["dataset_name"] = group_and_name
    hf.close()

    return (data, attr_dict)


def modify_values_from_original(init_setup, data, attr_dict, i, j, group_and_name, path):
    """Modify data and attr_dict:

    data:       crop padding zeros from end, delete erroneus values (returns new array)
    attr_dict:  set S to converged point, set new N if values are removed in between

    Erroneus block
    """
    # Add information about conf file
    parts = conf_modification.read_line(path + "/CONF", "Tau")
    tau = float(parts[0])
    attr_dict["tau"] = tau

    if not (("S" in attr_dict) or ("N" in attr_dict)):
        return data
    dir1, dir2 = init_setup.dir1_labels[i], init_setup.dir2_labels[j]
    S = init_setup.S_mat[i,j]
    attr_dict["S"] = S
    N = attr_dict["N"][0]  # Why this does not raise exception here?
    attr_dict["N"] = N

    # Crop off crap values from end of hdf5 array. (They are usually zeros.
    # The were used as buffer for future writes without needless allocation.)
    data = data[:N,...]

    failed_E_blocks = init_setup.failed_blocks_mat[i,j]
    failed_data_blocks = []
    for block in range(len(data)):
        # np.any checks if data is `!= 0.0` or `!= [0.0, 0.0, ...]`
        # Erroneous block
        if not np.any(data[block,...]):
            failed_data_blocks.append(block)

    if len(failed_E_blocks) + len(failed_data_blocks) == 0:
        return data
    failed_blocks = []
    for idx in failed_E_blocks:
        if idx not in failed_data_blocks:
            print("Weirdness in '{}/{}' '{}' block '{}': It is not failed even though"
                  .format(dir1, dir2, group_and_name, idx, ))
            print("    Et and/or Vt is failed. (By fail is meant '== 0.0'.) Anyways, this value")
            print("    {} is removed as it may be corrupted.\n"
                  .format(data[idx] if np.size(data[idx,...]) < 10 else "[too long array]"))
        failed_blocks.append(idx)

    for idx in failed_data_blocks:
        if idx not in failed_E_blocks:
            print("Weirdness in '{}/{}' '{}' block '{}': Even though it is failed"
                  .format(dir1, dir2, group_and_name, idx, ))
            print("    Et / Vt is not failed. (By fail is meant '== 0.0'.) Anyways, this block ")
            print("    going to be removed.\n")
            failed_blocks.append(idx)

    new_data = np.delete(data, failed_blocks, axis=0)
    for block in range(len(new_data)):
        assert(np.any(new_data[block,...])) # np.any checks if data is `!= 0.0` or `!= [0.0, 0.0, ...]`
    attr_dict["N"] = N - len(failed_blocks)

    return new_data




def re_orient_blockdata_matrix(dm, stp):
    """
    If data_matrix has vectors as its items, this function puts them
    into one single multidimensional array. Also crops off failed simulations
    :param dm: d
    :return:
    """

    if np.isscalar(dm[0][0]):
        return np.array(dm)
    else:
        valid = find_one_valid(dm)
        vt = valid.dtype
        if (vt == np.float64) or (vt == np.float32) or (vt == np.int32) or (vt == np.int64):
            data_arr = np.zeros((len(stp.range1), len(stp.range2)), dtype=object)
        else:
            # This error check is not very useful. If you need, this constraint may be lifted
            raise Exception("What type it is then? Data is type '{}', which is not hard coded"
                            .format(valid.dtype))

        for i in range(data_arr.shape[0]):
            for j in range(data_arr.shape[1]):
                item = dm[i][j]
                #assert(item is not None)  # This should not happen
                if item is None:  # None means no calculation available
                    data_arr[i, j] = None
                else:
                    data_arr[i, j] = item

        return data_arr

def find_one_valid(mat):
    """
    Returns the element in matrix that is not None.
    """
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            item = mat[i][j]
            if item is None:
                continue
            return item
    else:
        raise Exception("Not a single valid calculation found. This may mean that there is no\n "
                        "calculation data available or your directories are not named how they\n"
                        "were when they were generated.")

def re_orient_attr_matrix(attr_matrix, stp, data_arr=None, observable_name=None):
    """
    Instead of having matrix of small dictionaries, this function converts it to
    dict of matrices.

    If observable is scalar value for single block, this also it adds sem2 mean and
    error in same dict. (It also crops some oddly inconsistent values from data
    before meaning.)

    :param attr_matrix:
    :param stp:           initial_setup
    :return:
    """

    dict = {}
    # Keep book about calculations that did an did not produce output
    succeed = np.empty((len(stp.range1), len(stp.range2)), dtype=np.bool_)
    valid_attr = find_one_valid(attr_matrix)

    for key, val in valid_attr.items():
        # Find one valid item "it"
        if isinstance(val, np.ndarray) and np.shape(val) == (1,):
            valid_val = val[0]
        else:
            valid_val = val

        if np.issubdtype(type(valid_val), np.integer):
            init_val = np.int_(9999)  # Numeric value signaling error or missing data
        elif isinstance(valid_val, float):
            init_val = np.float_("nan")
        else:
            init_val = None

        arr = np.full((len(stp.range1), len(stp.range2)), fill_value=init_val)

        for i in range(len(stp.range1)):
            for j in range(len(stp.range2)):
                item = attr_matrix[i][j]
                # Invalid values:
                if item is None:
                    succeed[i, j] = False
                    continue
                else:
                    succeed[i, j] = True
                    it = item[key]
                    if isinstance(it, np.ndarray) and np.shape(it) == (1,):
                        arr[i, j] = it[0]
                    else:
                        arr[i, j] = it

        dict[key] = arr
    if observable_name is not None:
        dict["name"] = observable_name
        dict["succeed"] = succeed

    # Some other array
    if ("S" not in dict) or ("N" not in dict):
        return dict
    # multidimensional array
    elif len(find_one_valid(data_arr).shape) != 1:
        return dict
    else:
        # Add 'mean' and 'err' values
        tweak_attr_matrix_some_more(dict, attr_matrix, data_arr)

    return dict


def tweak_attr_matrix_some_more(dict, attr_matrix, data_arr):
    """Add 'mean' and 'err'."""

    mean_mat = np.zeros((len(attr_matrix), len(attr_matrix[0])),
                        dtype=data_arr.dtype) + 9999

    err_mat = np.zeros((len(attr_matrix), len(attr_matrix[0])),
                        dtype=data_arr.dtype) + 9999

    for i in range(mean_mat.shape[0]):
        for j in range(mean_mat.shape[1]):
            #assert(dict["S"][i, j] is not None)  # This should not happen or hdf5 container is somehow broken
            if dict["S"][i, j] == 9999 or dict["S"][i, j] is None:
                mean_mat[i, j] = float("nan")
                err_mat[i, j] = float("nan")
                continue
            S = dict["S"][i, j]
            N = dict["N"][i, j]

            if N <= 1:  # not enough blocks
                raise Exception(
                    "Umm, N={} with simulation idx: ({},{}) in observable {},\nget more blocks please."
                    .format(N, i, j, dict["name"] if "name" in dict else ""))

            #cut_off_ridiculous_values(dat)
            raw_data = data_arr[i, j][S-1: N]
            # Crop measurement mistakes off
            ids, dat, drp = cut_off_ridiculous_values_v3(raw_data)
            if drp is not None:
                dset = attr_matrix[i][j]["dataset_name"]
                print("Dropped out invalid blocks in", dset, ". Blocks: ", drp)

            if S > N:
                raise Exception(
                    "Error in hdf5-attributes S > N !, S:{}, N:{}, name:{}, idx1: {}, idx2: {}"
                        .format(S, N, dict["name"], i, j))

            mean, err = sem2.sem2(dat)

            mean_mat[i, j] = mean
            err_mat[i, j] = err

    dict["mean"] = mean_mat
    dict["err"] = err_mat



def cut_off_ridiculous_values_v2(dat):
    """Cut off 0.1% of data with highest and lowest values. So return subset that
    contains '99.8% of data. If there is less than 1000 blocks, then does no cropping.
    This function is to
    :return
        ids     indices that contain 99.8% of data
        dat     99.8% of original data
    """

    assert len(dat.shape) == 1

    srt = np.argsort(dat)
    beg = len(dat)//1000
    end = len(dat) - beg
    ids = np.sort(srt[beg:end])
    return ids, dat[ids]

def cut_off_ridiculous_values_v3(dat):
    """
    Crop measurement mistakes off.

    :param dat:
    :return:
        ids     indices of valid data
        dat     valid data
        drp     Dropped out indices
    """
    mean_raw = dat.mean()
    err_raw = dat.std()
    # margin 7*std removes gaussially distributed data point with probability 1 in 390 682 215 445
    valid = np.abs(dat - mean_raw) <= (err_raw * 7)
    ids = np.arange(len(dat))
    if valid.all():
        return ids, dat, None # No, need to drop anything, return original data
    else:
        drp = np.nonzero(~valid)
        return ids[valid], dat[valid], drp


def extrapolate_range1_at_0(init_setup, mean_mat, err_mat):
    from miscellaneous.general_tools import linear_errorbardata_fit
    assert(mean_mat.shape == err_mat.shape)

    mean_ext = np.zeros(mean_mat.shape[1])
    err_ext = np.zeros(mean_mat.shape[1])

    for j in range(mean_mat.shape[1]):
        x = []
        y = []
        err = []
        for i in range(mean_mat.shape[0]):
            if not np.isnan(mean_mat[i,j]):
                x.append(init_setup.range1[i])
                y.append(mean_mat[i,j])
                err.append(err_mat[i,j])

        if len(x) >= 2:
            mean, err = linear_errorbardata_fit(x, y, err, 0.0, err_sigma=1)
        else:
            mean, err = np.nan, np.nan
        mean_ext[j] = mean
        err_ext[j] = err


    data0 = np.concatenate((np.expand_dims(mean_ext,0), mean_mat), axis=0)
    err0 = np.concatenate((np.expand_dims(err_ext,0), err_mat), axis=0)
    assert(data0.shape[0] == mean_mat.shape[0] + 1)
    return data0, err0



def check_erros_hdf5(dset, hf, filepath):
    if dset[0] != "/":
        dset = "/" + dset

    if dset not in hf:
        error_mess = []
        error_mess.append("Dataset {} not found! See stdout below.".format(dset))
        error_mess.append("This is the content of file {} :".format(filepath))
        for name in hf:
            error_mess.append("    {}".format(name))
            for name2 in hf["/" + name]:
                error_mess.append("        {}/{}".format(name, name2))
                for i, name3 in enumerate(hf["/" + name + "/" + name2]):
                    error_mess.append("            {}.  {}".format(i+1, name3))
                    if i >= 4:
                        error_mess.append("            ... and other {} items"
                                          .format(len(hf["/" + name + "/" + name2])-5))
                        break
        raise ValueError("\n".join(error_mess))


def read_cached_observables(init_setup, observable_name, filename, fallback=True):
    observable_name_replaced = observable_name.replace("/", "∕")
    if not os.path.isfile("results/cache/" + observable_name_replaced + ".pickle"):
        if fallback:
            return read_all_observables(init_setup, observable_name, filename)[1]
        else:
            raise ValueError("Cached value not calculated yet! Read from hdf5 instead")

    with open("results/cache/" + observable_name_replaced + ".pickle", "rb") as f:
        attr_dict = pickle.load(f)

    # Check if any value changed
    mod_times_current = get_last_modification_times(init_setup, filename)
    mod_times_cache = attr_dict["last_modificiation_epoch_times"]
    if not np.all(mod_times_cache == mod_times_current):
        #print("Updating cache to match hdf5 data in", filename, observable_name)
        if fallback:
            _, attr_dict = read_all_observables(init_setup, observable_name, filename)
            return attr_dict
        else:
            raise ValueError("There is newer observables available than cached ones")

    if init_setup.extrapolate_range1 and "mean" in attr_dict\
            and not ("mean_extrapolation_at_range1=0" in attr_dict):
        _, attr_dict = read_all_observables(init_setup, observable_name, filename)
        return attr_dict

    return attr_dict


def get_last_modification_times(init_setup, filename):
    mod_times = np.zeros((len(init_setup.dir1_labels), len(init_setup.dir2_labels)))
    for i, dir1 in enumerate(init_setup.dir1_labels):
        for j, dir2 in enumerate(init_setup.dir2_labels):
            path = os.getcwd() + "/" + dir1 + "/" + dir2
            if not os.path.isdir(path):
                continue
            obs_path = get_obs_dir(path)
            file = obs_path + filename
            mod_times[i][j] = os.path.getmtime(file)
    return mod_times


def save_obs_to_cache(init_setup, attr_dict, filename, observable_name):
    mod_times = get_last_modification_times(init_setup, filename)
    attr_dict["last_modificiation_epoch_times"] = mod_times

    if not os.path.isdir("results/cache"):
        os.mkdir("results/cache")

    observable_name_replaced = observable_name.replace("/", "∕")
    with open("results/cache/" + observable_name_replaced + ".pickle", "wb") as f:
        pickle.dump(attr_dict, f)


if __name__ == "__main__":
    main(sys.argv)
