#!/usr/bin/python3

"""
This script lowers the priority of all matching programs
Usage
python3 -m change_priority pimc3
"""

import os
import sys
import time


def main(argv):
    import psutil
    from os.path import basename

    if "/" not in argv[1]:
        process_name = argv[1]
    else:
        process_name = basename(argv[1])


    if (len(argv) >= 2):
        priority = argv[2]
    else:
        priority = "low"

    if not (priority in "high,medium,low"):
        raise Exception("Unkown priority '" + argv[2] + "'")

    changed_something = False

    for i in range(5):  # 5 tries to do that
        for proc in psutil.process_iter():
            if proc.name() == process_name:
                nice_old = proc.nice()

                # Linux denies access if you try to modify other
                # processes priority, therefore run only for processes
                # less than 30 second old
                if proc.cpu_times()[0] < 30:
                    if (priority == "high"):
                        proc.nice(-10)
                    elif (priority in "medium"):
                        proc.nice(0)
                    elif (priority == "low"):
                        proc.nice(15)

                # Enable confirmation by uncommenting line below
                #print(proc.name(), proc.pid, " nice:", nice_old, "->", proc.nice())
                changed_something = True

        if changed_something:
            break

        # Sleep 50-250ms to wait that the process has spawned to system
        # It may still throw warning that the process does not exist yet.
        time.sleep(0.05*i)
    if not changed_something:
        print("change_priority.py: No process found with name '" + process_name + "'. (This is not imporant warning)")


if __name__ == "__main__":
    main(sys.argv)
