"""
This file renames new susceptibility observable name back to old one
"""

import numpy as np
import h5py

def rename(name_old, name_new, filepath):
    hf = h5py.File(filepath, 'r+')
    dset = hf.get(name_old)
    if (dset is None):
        raise ValueError("Did not find dataset with name " + name_old)
    hf[name_new] = dset
    del hf[name_old]
    hf.close()

def main():
    for i in range(1,4):
        rename("/MAGN/diamagn_susceptibility_A^2_{}".format(i),
               "/MAGN/dia.susc._(qA)^2_{}".format(i),
               "statmagn.h5")

main()
