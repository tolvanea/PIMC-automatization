"""
This file is stand-alone tool to stitch together observables from two different calculation.
The two calculations can be calculated with different amount of cores and NumMoves, thus name
of this "combine_serial_parallel.py" file. To use this file, replace hard-coded parts in functions
"combine_magn()" and "combine_E()".
"""

import numpy as np
import h5py
from os import makedirs
from os.path import dirname, isdir
from shutil import copyfile


def combine_serial_and_parallel_hdf5_files(
    filepath_serial,
    filepath_parall,
    filepath_output,
    dataset_names,
    serial_S=None,
    just_work_please=None
):
    """
    Congatenates M-core ("serial") and N-core ("parall") calculations into one hdf5 dataset. One
    use case is for example M=1 and N=8. As block datas have different variance, they are
    compressed to match each other. It really does not matter wheter parameters are really
    serial or parallel calculations at all, but the thing that matters is that 'filepath_serial'
    calculation is appended after 'filepath_parall'. That's why there's possibility to set
    starting point for serial data with 'serial_S'. The new congatenated dataset built using
    parallel dataset as a template, so 'filepath_output' contains also all other datasets of
    'filepath_parall'.

    Core-count or NumMoves need not to match, as block data is packed so that both
    calculations have same variance before congatenating. However, if exact packing
    is used, following should hold:
        - Greatest common denominator of
            M * NumMoves_serial and N * NumMoves_parall
          should be reasonably small.
        - Sampling interval is the same on both calculations.

    But there is also inexact packing ratio, which compares variances of calculations directly,
    and packs data determined by that. The same comression ratio is used for all datasets, and it is
    taken from median of compression ratios of dataset_names. Inexact compressing ratio is more
    dirty, but in most cases it is totally sufficient.


    See example in main() function.

    Arguments
        filepath_serial     input file name that is appended after 'filepath_parall'
                            (This file is not modified.)
        filepath_parall     input file name that comes first in combined dataset,
                            (This file is not modified.)
        filepath_output     output file of combined (and compressed) data.
                            (This file is overwritten if it exists.)
                            This file is built on top of copy of "parall" file, and appended with
                            compressed serial data. Only those datasets that are in `dataset_names`
                            are modified.
        dataset_names       List of all dataset names. These should be found from both calculations
        serial_S            Start index for serial data (items <S are dropped as non-convergent)
                            None means that 1/6 data is dropped automatically
        just_work_please    Use exact/inexact packing ratio insted of exact packing ratio.
                                True - Use always inexact value
                                None - Use exact value if available, otherwise use inexact
                                False - Use exact value, throw error otherwise
                            Exact packing ratio is read from 'cores' and 'moves' parameters.
                            Inexact packing ratio is read from variance.
    """

    hf_serial = h5py.File(filepath_serial, 'r')
    hf_parall = h5py.File(filepath_parall, 'r')
    copyfile(filepath_parall, filepath_output)  # Overwrite if existing
    hf_output = h5py.File(filepath_output, 'r+')

    # `cores_serial` can be 1
    cores_serial = hf_serial.get(dirname(dataset_names[0]) + "/statinfo").attrs["cores"][0]
    cores_parall = hf_parall.get(dirname(dataset_names[0]) + "/statinfo").attrs["cores"][0]
    moves_serial = hf_serial.get(dirname(dataset_names[0]) + "/statinfo").attrs["moves"][0]
    moves_parall = hf_parall.get(dirname(dataset_names[0]) + "/statinfo").attrs["moves"][0]
    inter_serial = hf_serial.get(dirname(dataset_names[0]) + "/statinfo").attrs["interval"][0]
    inter_parall = hf_parall.get(dirname(dataset_names[0]) + "/statinfo").attrs["interval"][0]
    # Note: `moves*cores` of serial should be exact divider to that of parallel

    div = np.gcd(cores_parall*moves_parall, cores_serial*moves_serial) # greatest common denominator
    frac = (cores_parall*moves_parall // div, cores_serial*moves_serial // (div))
    exact1 = frac[0] <= 60 and frac[1] <= 60 # common denominator is practically too small
    exact2 = inter_parall == inter_serial
    exact = exact1 and exact2
    if just_work_please is None:
        mode = "exact/inexact"
    elif just_work_please:
        mode = "inexact"
    else:
        mode = "exact"

    if mode == "exact":
        if not exact1:
            raise Exception("Not large enough common denominator for cores * NumMoves")
        if not exact2:
            raise Exception("Interval mimatch: intervals of calculations must match")
    if exact and mode != "inexact":
        compress_fraction = frac
        print("Exact compression")
        print("compress_ratio = (cores_parall * moves_parall) / (cores_serial * moves_serial)")
        print("{:>5} / {:<5} = ({:>12} * {:<12}) / ({:>12} * {:<12})"
            .format(compress_fraction[0], compress_fraction[1], cores_parall, moves_parall, cores_serial, moves_serial))
    else:
        compress_fraction = None  # Will be set later
    assert(dirname(dataset_names[0]) == dirname(dataset_names[-1])) #  Datasets are same calculation

    ratios = []
    serial_datas = []
    parall_datas = []
    for idx, dset_name in enumerate(dataset_names):
        dset_serial = hf_serial.get(dset_name)
        if (dset_serial is None):
            raise ValueError("Invalid dataset name, {} not in {}".format(dset_name, filepath_serial))
        dset_parall = hf_parall.get(dset_name)
        if (dset_parall is None):
            raise ValueError("Invalid dataset name, {} not in {}".format(dset_name, filepath_parall))
        data_serial = np.array(dset_serial[:dset_serial.attrs["N"][0]])
        data_parall = np.array(dset_parall[:dset_parall.attrs["N"][0]])
        assert(len(data_serial.shape) == 1)
        assert(len(data_parall.shape) == 1)
        assert(data_serial.dtype == data_parall.dtype)

        serial_datas.append(data_serial)
        parall_datas.append(data_parall)

        variance_serial = data_serial[data_serial.shape[0]//2:].std()
        variance_parall = data_parall[data_parall.shape[0]//2:].std()
        ratios.append((variance_serial / variance_parall, dset_name))

    ratios.sort(key=lambda t: t[0])
    median_ratio, median_dset = ratios[len(ratios)//2]
    if median_ratio < 1:
        if median_ratio < 1/12:
            m = 60
        else:
            m = 24
        div = np.gcd(int(m*median_ratio), m)
        compress_fraction_inexact = (int(m*median_ratio)//div, m//div)
    else:
        if median_ratio < 12:
            m = 24
        else:
            m = 60
        div = np.gcd(m, int(m/median_ratio))
        compress_fraction_inexact = (m//div, int(m/median_ratio)//div)
    if compress_fraction is None:
        compress_fraction = compress_fraction_inexact
        print("Non-exact compression ratio taken from the median which is {}".format(median_dset))
    else:
        print("Totally unrelated note, if your calculation were inexact, it would have used ratio:")
    print("    variance_serial / variance_parall = compress_ratio ")
    print("    {:>.13f} / {:<.13f} = {:>5} / {:<6}"
        .format(
            variance_serial,
            variance_parall,
            compress_fraction_inexact[0],
            compress_fraction_inexact[1]
        )
    )

    for idx, dset_name in enumerate(dataset_names):
        dset_output = hf_output.get(dset_name)
        if serial_S is not None:
            S = min(serial_S-1, 0)
        else:
            S = serial_datas[idx].shape[0]//6
        data_serial = serial_datas[idx][S:]
        data_parall = parall_datas[idx]

        compressed_serial = compress(data_serial, compress_fraction[0])
        compressed_parall = compress(data_parall, compress_fraction[1])
        combined_data = np.hstack((compressed_parall, compressed_serial))

        if idx==0:
            print("Lengths:")
            print("parall:", data_parall.shape)
            print("serial:", data_serial.shape)
            print("compressed_serial:", compressed_serial.shape)
            print("compressed_parall:", compressed_parall.shape)
            print("combined_data:", combined_data.shape)

        dset_output.resize(len(combined_data), axis=0)
        dset_output[:] = combined_data
        dset_output.attrs["N"] = np.array([len(combined_data)], dtype=dset_output.attrs["N"].dtype)


    print("Done. All listed datasets in {} and {} concatenated to overwritten file {}"
        .format(filepath_parall, filepath_serial, filepath_output))
    hf_serial.close()
    hf_parall.close()
    hf_output.close()

def compress(data, compress_ratio):
    compressed = np.zeros(len(data)//compress_ratio, dtype=data.dtype)
    for i in range(len(data)):
        if i >= len(compressed)*compress_ratio:
            break
        compressed[i//compress_ratio] += data[i]
    compressed /= compress_ratio
    return compressed

def combine_files(
    dir_serial,
    dir_parall,
    dir_output,
    filename,
    dataset_names,
    serial_S,
):
    # Configure these:
    filepath_serial = dir_serial + "/" + filename
    filepath_parall = dir_parall + "/" + filename
    # The datasets in following file will be overwritten!
    filepath_output = dir_output + "/" + filename # This file is based on parallel file.

    if not isdir(dir_output):
        makedirs(dir_output)

    combine_serial_and_parallel_hdf5_files(
        filepath_serial,
        filepath_parall,
        filepath_output,
        dataset_names,
        serial_S,
        just_work_please=None,
    )

def main():
    # Ok, I have currently directory structure
    #   obs/
    #       obs_csc/                        (parallel data)
    #           energy.h5
    #           statmagn.h5
    #       obs_kotikone/                   (serial data)
    #           energy.h5
    #           statmagn.h5
    #       002/                            (output directory)
    #       combine_serial_parallel.py      (this file)

    dir_serial = "old002"
    dir_parall = "new004"
    dir_output = "combined"
    serial_S = None  # You don't want nonconvergent serial data in the middle of array


    combine_files(
        dir_serial,
        dir_parall,
        dir_output,
        filename="energy.h5",
        dataset_names = [
            "/E/Ek",
            "/E/Ep",
            "/E/Et",
            "/E/Vk",
            "/E/Vp",
            "/E/Vt",
        ],
        serial_S=serial_S,
    )
    combine_files(
        dir_serial,
        dir_parall,
        dir_output,
        filename="statmagn.h5",
        dataset_names=[
            "/MAGN/dia.susc._(qA)^2_1",
            "/MAGN/dia.susc._(qA)^2_2",
            "/MAGN/dia.susc._(qA)^2_3",
        ],
        serial_S=serial_S,
    )

main()
