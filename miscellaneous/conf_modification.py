#!/usr/bin/python3
from typing import List
from sys import argv
from sys import exit
import os.path


help_text = """
This script reads (or writes) to one field in line in file.


required arguments:
    -f file
    -k key word
    -i index of words on line (starts from 0)
    -r replace value (only in replace mode)

example_file.txt:
    key1 val11 val12 val13
    key2 val21 val22 val23 val24
    key2 val31 val32

Example read:
$ python3 conf_modification read -f "CONF_FILE" -k "key2" -i "0"
        --> val21
        
Example write
$ python3 conf_modification write -f "CONF_FILE" -k "key2" -i "0" -r "mod21"

Example replace whole line
$ python3 conf_modification replace_line -f "CONF_FILE" -k "line" -r "new_line"

"""

"""
in python:
read_full_line(key2):
    --> [val21, val22, val23, val24]

usage (running directly)
    read_conf.py -f CONF_FILE -r KEY_VALUE_TO

"""




def read_line(file_name: str, key: str, range: tuple=(0, 9999999)) -> List[str]:
    """
    This function reads given config file, and returns the line (list(str)) for
    the matching key (key not included in list). Keys and value are separated
    with space(s) in file. Raises error if multiple matches are found.

    :param file_name:       path to file (string)
    :param key:             key (string)
    :param range:           limit range of lines to be searced


    """
    assert(len(range) == 2) # You did give something else as range as should
    found_line = None
    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if (len(parts)>0 and parts[0]==key):

                if (len(parts) < 2):    # skips newlines and unknown formatting
                    raise RuntimeError("Key must be separated with spaces")

                if (range[0] <= line_indx) and (line_indx <= range[1]):
                    if found_line is None:
                        found_line = parts[1:]
                    else:
                        raise RuntimeError("Multiple matches!")

    if found_line is None:
        raise RuntimeError("Nothing found with " + str(key))

    return found_line

def modify_line(file_name: str, key: str, word_idx: int, replacement, range: tuple=(0, 9999999)):
    """
    This function replaces one field in config line.
    
    Indexing starts from 0 obviously.

    The when the line matching to key is:
        key 1.2 3.4 asdf stuff-with-space
    corresponding word_idx is:
        0: 1.2
        1: 3.4
        2: asdf
        3: stuff-with-space
    ... and so on.

    So this replaces word corresponding the word-idx
    

    Keys and value are separated with space(s) in file.
    Args
        file_name:       path to file (string)
        key:             key (string), first field on the line
        word_idx         see above
        replacement      replacement value for word
        range            line index range to be searched on


    """
    found_line = None
    assert(len(key.split()) == 1)  # key should be one word, otherwise idk what happens
    assert(len(replacement.split()) == 1)  # replacement should be one word, or you screw your file
    with open(file_name, "r+") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if (1 <= len(parts)) and (parts[0]==key):

                if (len(parts) < 2):    # skips newlines and unknown formatting
                    raise Exception("Key must be separated with spaces")

                if (range[0] <= line_indx) and (line_indx <= range[1]):
                    if found_line is None:
                        found_line = parts[1:]
                        parts[word_idx+1] = replacement
                        #file_lines[line_indx] = line.replace(found_line[word_idx], replacement)
                        ws = get_whitespace_in_identation(line)
                        file_lines[line_indx] = ws + "\t".join(parts) + "\n"
                    else:
                        raise Exception("Multiple matches!")

        if found_line is None:
            print("    Error in `modify_line`!  Nothing found with", key)
            print("    Should this crash? Yes? Let's chrash.")
            raise ValueError("Nothing found with " + key)

        conf_file.seek(0)
        conf_file.truncate() # is this required in any way?
        conf_file.writelines(file_lines)


def get_whitespace_in_identation(line: str):
    """
    Preserves identation in modify_line
    """
    for i,char in enumerate(line):
        if char not in " \n":
            whitespace = line[0:i]
            break
    return whitespace

def replace_line(file_name: str,
                 line: str,
                 replacement,
                 match_begining=True,
                 range: tuple=(0, 9999999)):
    """
    Replaces the whole line with given value. Notice that param:line can be
    stripped from excess spaces. Also line matches if it has same beginning.

    file_name:      path to file (string)
    line:           whole line (string) (does not need to contain identing whitespaces)
    replacement     replacement of whole line (no need for identing whitespaces)
    match_begining  the compare beginning of line, (instead of matching full line)
    range           line index range to be searched on


    """
    found_line = None
    with open(file_name, "r+") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, file_line in enumerate(file_lines):

            if (range[0] <= line_indx) and (line_indx <= range[1]):

                if (file_line == line) or \
                   (file_line.strip() == line.strip()) or \
                   (match_begining and file_line.strip().startswith(line.strip())):

                    if found_line is None:
                        found_line = file_line.strip()

                        #file_lines[line_indx] = file_line.replace(found_line[word_idx], replacement)
                        ws = get_whitespace_in_identation(file_line)
                        file_lines[line_indx] = ws + replacement + "\n"
                    else:
                        raise Exception("Multiple matches!")

        if found_line is None:
            print("    Nothing found with", line)
            print("    Continuing")

        conf_file.seek(0)
        conf_file.truncate() # is this required in any way?
        conf_file.writelines(file_lines)


def old_bash_style_config_modifier(argv):
    """
    This function is the first thing I did with python in years so, the structure is crap.
    It works though well (from bash), so in a hurry it is not re-written. Desiged
    specifically to edit CONF file in pimc3-calculations. The good thing is that it preserves
    all white space, unlike 'modify_line', which works for editing as well.

    example line to be edited:
        Temperature                   300.0
        Tau                           1.0
        ...

    conf_mod.old_bash_style_config_modifier(["<trash-argument>", "-f", "CONF", "--Tau=0.5", "--Temperature=200"])

    Args
        argv    the list of arguments that will be given in bash
    """

    given_key = ""
    new_value = ""

    # Get file name
    if (("-f" in argv) and (argv.index("-f")+1 < len(argv))):
        file = argv[argv.index("-f")+1]
    else:
        print("    No file given")
        print("    Usage: conf_mod.py -f path/file.txt --key1=value1 --xyz=123")
        print("    Nothing is written")
        raise Exception("No file given")

    # check that file exist
    if (not os.path.isfile(file)):
        print("    No file", file, "exist in ", os.getcwd())
        print("    Nothing is written")
        raise Exception("No file " + file + " exist")

    # open file, iterate parameters and iterate file lines
    with open(file, "r+") as conf_file:

        file_lines = conf_file.readlines()
        file_lines_copy = file_lines

        # Iterate through parameters
        itr = 1
        while(itr < len(argv)):

            # Parse key and value from parameters
            given_key, new_value = "", ""
            if ((2 <= len(argv[itr])) and (argv[itr][0:2] == "--")):
                [given_key, new_value] = argv[itr][2:].split(sep="=")
            elif(argv[itr]=="-f"):
                itr = itr + 2	# skip file name
                continue
            else:
                print("    Unknown parameter,",argv[itr])
                print("    Usage: ./conf_mod.py -f path/file.txt --key1=value1 --xyz=123")
                print("    Nothing is written to file")
                raise Exception("Unknown parameter, " +argv[itr])

            # Following will be prosecced if '--' parameter pair was found

            itr = itr + 1

            replaced = False
            for line_indx, line in enumerate(file_lines_copy):
                value_pair = line.split()
                if (len(value_pair) != 2):	# skips newlines and unknown '='-formatting
                    continue
                if (value_pair[0]==given_key):
                    file_lines[line_indx] = line.replace(value_pair[1],new_value)
                    replaced = True

            if (not replaced and given_key!=""):
                print("    Nothing found with", given_key)
                print("    Continuing")


            conf_file.seek(0)
            conf_file.truncate()
            conf_file.writelines(file_lines)
		#print("    Key-values replaced succesfully")


def read_parameters(argv):
    """Parameter reading"""

    mode = argv[1]
    file_name = ""
    key = ""
    index = ""
    replace = ""


    if mode == "--help":
        print(help_text)

    if mode not in ["write", "read", "replace_line"]:
        print("must be read or write mode")
        print(help_text)
        raise Exception("read stdout above")

    if (("-f" in argv) and (argv.index("-f")+1 < len(argv))):
        file_name = argv[argv.index("-f")+1]
    else:
        print("No file given")
        print(help_text)
        raise Exception("read stdout above")

    if (("-k" in argv) and (argv.index("-k")+1 < len(argv))):
        key = argv[argv.index("-k")+1]
    else:
        print("No key given")
        print(help_text)
        raise Exception("read stdout above")

    if mode == "replace_line":
        pass
    else:
        if (("-i" in argv) and (argv.index("-i")+1 < len(argv))):
            index = int(argv[argv.index("-i")+1])
        else:
            print("No index given")
            print(help_text)
            raise Exception("read stdout above")

    # check that file exist
    if (not os.path.isfile(file_name)):
        print("    No file", file_name, "exist")
        print("    Nothing is read")
        raise Exception("read stdout above")

    if mode in ["write", "replace_line"]:
        if (("-r" in argv) and (argv.index("-r")+1 < len(argv))):
            replace = argv[argv.index("-r")+1]
        else:
            print("No replace value given")
            print(help_text)
            raise Exception("read stdout above")

    return mode, file_name, key, index, replace


def main_function_if_executed_directly(argv):
    """
    This is main function, and is executed if this "module" is called directly.
    Therefore this can be called from bash.
    Indexing starts from 0 obviously.
    """

    mode, file_name, key, index, replace = read_parameters(argv)


    # The conf reading
    if mode=="read":
        print("read IS NOT TESTED")
        line = read_line(file_name, key, index)
        print(line[index])

    elif mode=="write":
        modify_line(file_name, key, index, replace)

    elif mode=="replace_line":
        replace_line(file_name, key, replace)
    else:
        print("Why this not work?")


if __name__ == "__main__":
    main_function_if_executed_directly(argv)

