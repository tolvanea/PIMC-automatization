import sys
import math
import numpy as np


# https://goshippo.com/blog/measure-real-size-any-python-object/
def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size


# Print float x with p significant digits
# Taken from: http://randlet.com/blog/python-significant-figures-format/
def to_precision(x,p):
    """
    returns a string representation of x formatted with a precision of p

    Based on the webkit javascript implementation taken from here:
    https://code.google.com/p/webkit-mirror/source/browse/JavaScriptCore/kjs/number_object.cpp
    """
    x = float(x)

    if x == 0.:
        return "0." + "0"*(p-1)

    out = []

    if x < 0:
        out.append("-")
        x = -x

    e = int(math.log10(x))
    tens = math.pow(10, e - p + 1)
    n = math.floor(x/tens)

    if n < math.pow(10, p - 1):
        e = e -1
        tens = math.pow(10, e - p+1)
        n = math.floor(x / tens)

    if abs((n + 1.) * tens - x) <= abs(n * tens -x):
        n = n + 1

    if n >= math.pow(10,p):
        n = n / 10.
        e = e + 1

    m = "%.*g" % (p, n)

    # Exponential form disabled for now
    # if e < -2 or e >= p:
    #     out.append(m[0])
    #     if p > 1:
    #         out.append(".")
    #         out.extend(m[1:p])
    #     out.append('e')
    #     if e > 0:
    #         out.append("+")
    #     out.append(str(e))
    if e == (p -1):
        out.append(m)
    elif e >= 0:
        out.append(m[:e+1])
        if e+1 < len(m):
            out.append(".")
            out.extend(m[e+1:])
    else:
        out.append("0.")
        out.extend(["0"]*-(e+1))
        out.append(m)

    return "".join(out)

def to_precision_fixed(num, pres):
    """
    Return number with given significant digits.

    >>> to_precision_fixed(1234.567, 4)
    '1235'
    >>> to_precision_fixed(1234.567, 5)
    '1234.6'
    >>> to_precision_fixed(1234.567, 6)
    '1234.57'
    >>> to_precision_fixed(-1234.567, 6)
    '-1234.57'
    >>> to_precision_fixed(0.1234567, 3)
    '0.123'
    >>> to_precision_fixed(0.1234567, 4)
    '0.1235'
    >>> to_precision_fixed(-0.1234567, 4)
    '-0.1235'
    >>> to_precision_fixed(0.000001234567, 4)
    '0.000001235'
    >>> to_precision_fixed(1234567.01, 3)
    '1230000'
    >>> to_precision_fixed(1234.567, 4)
    '1235'
    >>> to_precision_fixed(-1234.567, 4)
    '-1235'
    >>> to_precision_fixed(1234.567, 2)
    '1200'
    """
    x = float(num)
    if np.isnan(num) or np.isnan(pres):
        return "NaN"
    digits_of_x = int(math.log10(math.fabs(x)))+1
    if digits_of_x > pres:
        return str(round(int(x+0.5), -(digits_of_x-pres)))
    return to_precision(num, pres)


def pretty_print_errorbars(num: float, err0: float) -> str:
    """
    Print float with error bars like:

    >>> pretty_print_errorbars(12.3121212, 0.0290) # error part is less than 30
    '12.312(29)'
    >>> pretty_print_errorbars(12.3121212, 0.0301) # error part is over 30
    '12.31(3)'
    >>> pretty_print_errorbars(12.3121212, 0.111)
    '12.31(11)'
    >>> pretty_print_errorbars(12.3121212, 0.311)
    '12.3(3)'
    >>> pretty_print_errorbars(12.3121212, 1.111)  # decimal separator in between, exception to rule
    '12(1)'
    >>> pretty_print_errorbars(12.3121212, 3.111)  # decimal separator in between, exception to rule
    '12(3)'
    >>> pretty_print_errorbars(12.3121212, 0.01567)
    '12.312(16)'
    >>> pretty_print_errorbars(12.3121212, 0.001567)
    '12.3121(16)'
    >>> pretty_print_errorbars(12.3121212, 0.0001567)
    '12.31212(16)'
    >>> pretty_print_errorbars(12.3121212, 0.00001567)
    '12.312121(16)'
    >>> pretty_print_errorbars(44.950123, 0.3243818)
    '45.0(3)'
    >>> pretty_print_errorbars(4.4950123, 0.03243818)
    '4.50(3)'
    >>> pretty_print_errorbars(0.44950123, 0.003243818)
    '0.450(3)'
    >>> pretty_print_errorbars(-0.44950123, 0.003243818)
    '-0.450(3)'
    >>> pretty_print_errorbars(-0.0000044950123, 0.00000003243818)
    '-0.00000450(3)'
    >>> pretty_print_errorbars(-0.0000044950123, 0.00000002843818)
    '-0.000004495(28)'
    >>> pretty_print_errorbars(123.456, 0.290)
    '123.46(29)'
    >>> pretty_print_errorbars(1234.567, 0.290)
    '1234.57(29)'
    >>> pretty_print_errorbars(1234.567, 2.90)  # Exception to rule
    '1235(3)'
    >>> pretty_print_errorbars(1234.567, 3.10)
    '1235(3)'
    >>> pretty_print_errorbars(1234.567, 29.1)
    '1235(29)'
    >>> pretty_print_errorbars(12345.67, 291.1)
    '12350(290)'
    >>> pretty_print_errorbars(12345.67, 311.1)
    '12300(300)'
    >>> pretty_print_errorbars(12345.67, 2911.1)
    '12300(2900)'
    >>> pretty_print_errorbars(12345.67, 3111.1)
    '12000(3000)'
    >>> pretty_print_errorbars(4.160100999104829, 0.095)
    '4.16(10)'
    >>> pretty_print_errorbars(-4.160100999104829, 0.095)
    '-4.16(10)'
    >>> pretty_print_errorbars(4.03895648975964, 0.0058658)
    '4.039(6)'

    TODO now errors 1.11|0.29 and 1.11|0.30 rounds like 1.11(29) and 1.1(3).
        I think the rounding should be at 1.11(14) and 1.1(2)

    TODO This code is shit. Endless source of grief. Always broken, and lot of
        fixes on top of each other. So better would be just rewrite it entirely.
        Use with caution if your input differs from examples above.
    """
    #print("This is shit", num, err0)
    if err0 < 0:
        raise Exception("Negative error?")
    if err0 == 0.0:
        return "{}({})".format(num, 0)

    val_significant_digits_base = int(np.floor(np.log10(np.abs(num)))) - int(np.floor(np.log10(np.abs(err0)))) + 2
    #print(val_significant_digits_base, np.log10(np.abs(num)), np.log10(np.abs(err)))
    number_of_zeros_in_err = int(np.floor(np.log10(err0)))
    first_digit_of_err = int(err0 / 10**number_of_zeros_in_err)
    err_first_digit_removed = err0 - (first_digit_of_err * 10**number_of_zeros_in_err)
    second_digit_of_err = int(err_first_digit_removed / 10**(number_of_zeros_in_err-1))
    err_rounded_up_to_10 = (first_digit_of_err == 9) and (second_digit_of_err >= 5)
    err = err0
    if first_digit_of_err < 3:
        err_len = 2
    else:
        if err_rounded_up_to_10:
            err_len = 2
            err = round(err0, -number_of_zeros_in_err)
            val_significant_digits_base -= 1
        else:
            err_len = 1

    val_significant_digits = val_significant_digits_base - (2 - err_len)
    #print("first_digit_of_err,err_first_digit_removed,second_digit_of_err")
    #print(first_digit_of_err,err_first_digit_removed,second_digit_of_err)
    #print("err_len, val_significant_digits, err_rounded_up_to_10")
    #print(err_len, val_significant_digits, err_rounded_up_to_10)

    # if error has digits in it
    if err >= 10:
        value_part = to_precision_fixed(num, val_significant_digits)
        err_part = to_precision_fixed(err, err_len)
        return "{}({})".format(value_part, err_part)
    else:
        # If comma is in 2-digit error
        if (1.0 <= err) and (err < 10.0) and (err_len == 2):
            err_len = 1
            val_significant_digits -= 1
        value_part = to_precision_fixed(num, val_significant_digits)
        err_part = to_precision_fixed(err, err_len)[-err_len:]

        return "{}({})".format(value_part,err_part)


def linear_errorbardata_fit(x_arr, mean_arr, err_arr, x, err_sigma=1, mc_lines=400):
    """
    Evaluate linear fit with errorbars.

    Error estimates are implemented with Monte Carlo sampling and jack-knife unbiasing. See
        https://young.physics.ucsc.edu/jackboot.pdf
    for more about jack-knive method

    :param x_arr:       x-values                                (Iterable[Float])
    :param mean_arr:    y-means, also referred below as "data"  (Iterable[Float])
    :param err_arr:     y-errorbars                             (Iterable[Float])
    :param x:           x-value in which to interpolate or extrapolate  (Float)
    :param err_sigma:   Standard deviation sigma in errorbars: 1 -> 68%,  2 -> 95%,  3 ->99.7% ...
                        This sigma must correspond sigma of `err_arr`. Result errors are also
                        deviated by this same sigma.
    :param mc_points:   Errorbar distribution is scattered this many individual Monte Carlo points
    :param mc_lines:    This many linear fits are (Monte Carlo) sampled from random set of points
    :return:            (y_mean: Float, y_err: Float)
    """
    assert((len(x_arr) == len(mean_arr)) and (len(err_arr) == len(mean_arr)))

    # Numpy internal bug prevents me from writing:
    # coeff_mean = np.polyfit(x_arr, mean_arr, 1)
    # But this hack goes around it:
    coeff_mean = np.polyfit(x_arr, list(mean_arr), 1)

    fit_mean = coeff_mean[0] * x + coeff_mean[1]

    # Unbiased error estimate of fit with jack-knife method
    # So, let's notate line fit "f(data)", where 'data' is input vector. We are interested of
    # error on <f(data_k)> where data_k is jack-knife average sample k. The jack-knife mean is taken
    # from randomly generated gaussian data, that is generated from errobar data.
    jackknife_data_averages = np.zeros((len(mean_arr), mc_lines))
    for i in range(len(mean_arr)):
        data_sample = np.random.randn(mc_lines) * (err_arr[i]/err_sigma) * np.sqrt(mc_lines) + mean_arr[i]
        jackknife_data_averages[i,:] = (data_sample.sum() - data_sample) / (mc_lines-1)
    jackknife_fit_estimates = np.zeros(mc_lines)
    for k in range(mc_lines):
        # Numpy internal bug forces to write this with lists
        coeff = np.polyfit(x_arr, list(jackknife_data_averages[:,k]), 1)  # Fit line to one average-sample
        jackknife_fit_estimates[k] = coeff[0] * x + coeff[1]
    error_estimate = jackknife_fit_estimates.std() * np.sqrt(mc_lines - 1) * err_sigma

    return fit_mean, error_estimate


def test_linear_errorbardata_fit():
    import matplotlib.pyplot as plt
    from matplotlib.colors import to_rgb
    # data_1 = (
    #     [1],
    #     [2]
    # )
    # err_1 = (
    #     [0.3],
    #     [0.6],
    # )

    data_2 = (
        [1, 2],
        [1, 3],
    )
    err_2 = (
        [0.3, 0.3],
        [0.9, 0.9],
    )

    data_3 = (
        [1, 2, 3],
        [1, 2, 4],
    )
    err_3 = (
        [0.3, 0.3, 0.3],
        [0.3, 0.9, 1.2],
        [0.3, 0.9, 0.3],
        [0.9, 0.3, 0.9],
    )

    data_10 = (
        [i+1 for i in range(10)],
        [i**2 for i in range(10)],
    )
    err_10 = (
        [0.9 for _ in range(10)],
        [0.9*(i%3) + 1.0 for i in range(10)],
        [np.abs(5-i) * 0.9 + 1.0 for i in range(10)],
        [(5-np.abs(5-i)) * 0.9 + 1.0 for i in range(10)],
    )

    for data, errbar in ((data_2, err_2), (data_3, err_3), (data_10, err_10)):
        fig, axs = plt.subplots(len(data),len(errbar), figsize=(len(errbar)*4, len(data)*5))
        for i in range(len(data)):
            for j in range(len(errbar)):
                #idx = len(errbar)*i + j
                err_data = 3*np.array(errbar[j])
                x_data = np.array(data[i])
                y_data = 2*x_data + np.random.randn(len(x_data)) *err_data
                axs[i][j].errorbar(x_data, y_data, err_data, c="C0", capsize=4)

                plot_res = 30
                x_fit = np.linspace(-5, max(8, data[i][-1]*2), plot_res)
                y_fit_mean = np.zeros(plot_res)
                y_fit_err = np.zeros(plot_res)
                for k in range(plot_res):
                    #mean, err = linear_errorbardata_fit_v1(x_data, y_data, err_data, x_fit[k], mc_lines=100)
                    #mean, err = linear_errorbardata_fit(x_data, y_data, err_data, x_fit[k], mc_lines=100)
                    y_fit_mean[k] = mean
                    y_fit_err[k] = err
                (line, caps, barlines) = axs[i][j].errorbar(x_fit, y_fit_mean, y_fit_err,
                                                            c="C1", capsize=4, alpha=0.5)
                if min(y_fit_mean) > -40 and max(y_fit_mean) < 40:
                    axs[i][j].set_ylim(-20, 20)

                # Set errorbar color lighter
                def brighten(color, amount):
                    """amount == 0.0: no change, amount == 1.0: white """
                    return tuple(map(lambda c: c + (1-c)*amount, color))
                color = to_rgb(line.get_color())
                for cap in caps:
                    cap.set_color(brighten(color, 0.8))
                for barline in barlines:
                    barline.set_color(brighten(color, 0.2))

    plt.show()

#test_linear_errorbardata_fit()


def proper_mean_with_errorbars(values, errors):
    """
    Given multiple values (or arrays) with uncertainty, this calculates
    the mean for them. Note, the mean is NOT sum/N, because the sum is
    weighted by variances.
    https://en.wikipedia.org/wiki/Weighted_arithmetic_mean
    https://physics.stackexchange.com/a/329412 + insert w_i = 1/sigma_i^2

    values  List of values (or arrays) that will be averaged
    errors  List of errorbars (sigma-1) of 'values'.
            With N samples, this error is std_dev/sqrt(N)
    """
    weighted_sum = values[0]*0
    total_weight = values[0]*0
    for v, e in zip(values, errors):
        weight = 1/e**2
        weighted_sum += v * weight
        total_weight += weight
    weighted_average = weighted_sum / total_weight
    error_of_weighted_average = 1 / np.sqrt(total_weight)
    return weighted_average, error_of_weighted_average
